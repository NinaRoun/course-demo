import React from 'react';
import Settings from '@material-ui/icons/Settings';
import ListOutlined from '@material-ui/icons/ListOutlined';
import ExitToAppOutlined from '@material-ui/icons/ExitToAppOutlined';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Computer from '@material-ui/icons/Computer';
import HourglassFull from '@material-ui/icons/HourglassFull';

export const NAVBAR_OPTIONS = [
    {
        name: 'Profile',
        icon: <AccountCircle />,
        path: '/profile',
    },
    {
        name: 'Instructions',
        icon: <LibraryBooks />,
        path: '/instructions',
    },
    {
        name: 'Learning',
        icon: <Computer />,
        path: '/learning',
    },
    {
        name: 'Statistics',
        icon: <ListOutlined />,
        path: '/statistics',
    },
    {
        name: 'Testing',
        icon: <HourglassFull />,
        path: '/testing',
    },
    {
        name: 'Settings',
        icon: <Settings />,
        path: '/settings',
    },
    {
        name: 'Log out',
        icon: <ExitToAppOutlined />,
        path: '/',
    }
];
