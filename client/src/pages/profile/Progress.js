import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import UserCard from '../../components/UserCard.js';
import Grid from '@material-ui/core/Grid';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { fetchBlocks } from '../../actions/blocksActions.js';
import styles from '../../styles.js';
import { getCurrentBlock } from "../../actions/userActions";

class Progress extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            activeStep: 0,
        };

        this.setActiveStep = this.setActiveStep.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    setActiveStep() {
        const { currentBlock } = this.props;
        //console.log('currentBlock ', currentBlock);

        this.setState({ activeStep: currentBlock.id })
    };

    handleReset() {
        this.setState({ activeStep: 0 });
    };

    async componentDidMount() {
        const { userId } = this.props;

        await this.props.fetchBlocks();
        await this.props.getCurrentBlock(userId);
        this.setActiveStep();
    }

    render() {

        const { classes, blocks } = this.props;
        const { activeStep } = this.state;

        return (
            <div>
                <Stepper activeStep={activeStep} orientation="vertical">
                    {blocks.map(block => {
                        const { name, summary } = block;
                        return (
                            <Step key={name}>
                                <StepLabel>{name}</StepLabel>
                                <StepContent>
                                    <Typography>{summary}</Typography>
                                </StepContent>
                            </Step>
                        )
                    })}
                </Stepper>
                {activeStep === blocks.length && (
                    <Paper square elevation={0}>
                        <Typography>All steps completed - you&apos;re finished</Typography>
                    </Paper>
                )}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userId: state.validation.userProfile.userId,
    currentBlock: state.validation.userProfile.currentBlock,
    blocks: state.blockData.blocks,
    isLogged: state.validation.isLogged,
    error: state.validation.error,
    isLoading: state.validation.isLoading,
    userName: state.validation.userProfile.userName,
    currentBlocks: state.validation.userProfile.currentBlocks,
    finishedBlocks: state.validation.userProfile.finishedBlocks,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchBlocks: () => dispatch(fetchBlocks()),
        getCurrentBlock: (userId) => dispatch(getCurrentBlock(userId)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps,
)(injectStyles(styles)(Progress)))
