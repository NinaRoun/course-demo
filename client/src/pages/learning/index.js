import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import UserCard from '../../components/UserCard.js';
import BlocksList from './BlocksList.js';
import styles from '../../styles.js';
import CollapseButton from "../../components/CollapseButton";

const minPageSize = 700;

class LearningPage extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleClick(page) {
        console.log('!!!path ', page);
    };

    handleClose() {
        console.log('handleClose');
    };

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {

        const { classes } = this.props;
        const { pageIsLarge } = this.state;

        return (
            <div className={classes.pageContent}>

                { !pageIsLarge && <CollapseButton></CollapseButton> }

                <div className={classes.profileContent}>

                    { pageIsLarge && <div className={classes.userCardGrid}><UserCard /></div> }

                    <div className={classes.contentGrid}>
                        <h2 className={classes.pageTitle}>Learning</h2>
                        <div className={classes.pageText}>
                            <BlocksList className={classes.pageText}></BlocksList>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    isLogged: state.validation.isLogged,
    error: state.validation.error,
    isLoading: state.validation.isLoading,
    userName: state.validation.userProfile.userName,
    currentBlocks: state.validation.userProfile.currentBlocks,
});

// const mapDispatchToProps = dispatch => {
//     return {
//         setWindowSize: () => dispatch(setWindowSize()),
//     }
// };

export default withRouter(connect(
    mapStateToProps,
    //mapDispatchToProps
)(injectStyles(styles)(LearningPage)))
