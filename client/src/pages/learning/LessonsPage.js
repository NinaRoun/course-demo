import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import UserCard from '../../components/UserCard.js';
import styles from '../../styles.js';
import CollapseButton from "../../components/CollapseButton";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Lock from '@material-ui/icons/Lock';
import CheckCircle from '@material-ui/icons/CheckCircle';
import { getCurrentStep, setCurrentStep, getFinishedSteps } from '../../actions/userActions.js';

const minPageSize = 700;

class LessonsPage extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
            items: [],
        };

        this.openLesson = this.openLesson.bind(this);
        this.renderLessons = this.renderLessons.bind(this);
    }

    async renderLessons() {
        const { chosenSteps, currentStep, finishedSteps, userId, classes } = this.props;
        this.setState({ items: chosenSteps });
        const items = [];

        await this.props.getCurrentStep(userId);
        await this.props.getFinishedSteps(userId);

        // console.log('!!!!! ', finishedSteps);
        // console.log('!!!!! ', currentStep, userId);
        //
        // console.log('current step ', currentStep);
        // console.log('first step ', chosenSteps[0]);

        chosenSteps.forEach((step, id) => { //when a page loads for the 1st time, check icon doesn't appear for the 1st lesson
            const { _id, name } = step;

            if (currentStep && currentStep._id === _id) items.push({ _id, name, icon: <CheckCircle className={classes.checkCurrentLesson}></CheckCircle> });
            else if (finishedSteps.length && finishedSteps.includes(_id)) items.push({ _id, name, icon: <CheckCircle className={classes.checkFinishedLesson}></CheckCircle> });
            else items.push({ _id, name, icon: <Lock/> });
        });

        this.setState({ items });
    }

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    async componentDidMount() {
        const { userId } = this.props;

        await this.props.getCurrentStep(userId);
        await this.props.getFinishedSteps(userId);

        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));

        console.log(this.props.userProfile);
        await this.renderLessons();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    openLesson(_id) {
        const { finishedSteps } = this.props;

        if (_id === this.props.currentStep._id
            || finishedSteps.length && finishedSteps.includes(_id)) this.props.history.push('/lesson');
    }

    render() {

        const { classes, chosenSteps } = this.props;
        const { pageIsLarge, items } = this.state;

        return (
            <div className={classes.pageContent}>

                { !pageIsLarge && <CollapseButton></CollapseButton> }

                <div className={classes.profileContent}>

                    { pageIsLarge && <div className={classes.userCardGrid}><UserCard /></div> }

                    <div className={classes.contentGrid}>
                        <h2 className={classes.pageTitle}>Lessons</h2>
                        <div className={classes.pageText}>
                            <TableContainer component={Paper}>
                                <Table aria-label="customized table">
                                    <TableBody className={classes.table}>
                                        { items.map(step => {
                                            const { _id, name, icon } = step;

                                            return (
                                                <TableRow
                                                    className={classes.tableItem}
                                                    key={ name }
                                                    onClick={() => this.openLesson(_id)}
                                                >
                                                    <TableCell component="th" scope="row">
                                                        { name }</TableCell>
                                                    <TableCell component="th" scope="row">
                                                        { icon }</TableCell>
                                                </TableRow>
                                            )
                                        })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    isLogged: state.validation.isLogged,
    error: state.validation.error,
    isLoading: state.validation.isLoading,
    finishedSteps: state.validation.userProfile.finishedSteps,
    currentStep: state.validation.userProfile.currentStep,
    chosenSteps: state.stepData.chosenSteps,
    userId: state.validation.userProfile.userId,
    userProfile: state.validation.userProfile,
});

const mapDispatchToProps = dispatch => {
    return {
        getCurrentStep: (userId) => dispatch(getCurrentStep(userId)),
        setCurrentStep: (step, userId) => dispatch(setCurrentStep(step, userId)),
        getFinishedSteps: (userId) => dispatch(getFinishedSteps(userId)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(LessonsPage)))

// LOGIC

// on first load of learning page first block sets as current - done
// first lesson of the first block sets as current lesson - done
// lessons should be finished step by step, the next lesson can't be opened until current one is not finished - done
// that is, until the video of the lesson is not finished, now we have a button instead of the video
// you can't start a new block until current is not finished, only see its content - done
// blocks are learnt one by one - done
// user is able to open any block to see what lesson it contains, but can open only lessons of current and finished blocks - done

// FUNCTIONALITY TO IMPLEMENT

// all items (block and steps) should be sorted by id in the order they should be outputed on the page before rendering - done
// profile page - mark blocks blue when they are passed and open summary of the current block - done
// find out how to implement video on lessons page and track how much is watched
// possibly there should be no possibility to skip on video
// find out where to put text files (lectures) for downloading
// as a way, one more cell next to icon with download icon with a tooltip 'download lecture'
// implement tests after lessons to finish block
// implement admin functionality

