import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { fetchBlocks } from '../../actions/blocksActions.js';
import { fetchSteps, setChosenSteps } from '../../actions/stepsActions.js';
import { setCurrentBlock, getFinishedBlocks, getCurrentBlock } from '../../actions/userActions.js';
import styles from '../../styles.js';
import { setCurrentStep } from "../../actions/userActions";
import { sortNumber } from '../../helpers/index.js';

class BlocksList extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            stepsAreOpen: false,
            filteredSteps: [],
            items: [],
        };

        this.createData = this.createData.bind(this);
        this.renderBlocks = this.renderBlocks.bind(this);
    }

    renderBlocks() {
        const { userId, blocks, finishedBlocks, currentBlock } = this.props;
        this.setState({ items: this.props.blocks });

        const items = [];

        // await this.props.getCurrentBlock(userId);
        // await this.props.getFinishedBlocks(userId);

        blocks.forEach(block => {
            const { _id, name, steps } = block;

            if (finishedBlocks.includes(block._id)) items.push({ _id, name, steps, status: <span>Done</span> });
            else if (currentBlock._id === block._id) items.push({ _id, name, steps, status: <span>In progress</span> });
            else items.push({ _id, name, steps, status: <span>Not started</span> })
        });

        this.setState({ items });
    }

    filterSteps(chosenSteps, clickedBlock) {
        const { steps, currentStep, currentBlock } = this.props;

        const filteredSteps = this.props.steps.filter(step => chosenSteps.includes(step._id));
        const sortedSteps = filteredSteps.sort((a, b) => sortNumber(a.id, b.id));
        //console.log('!!!!!!! sorted steps ', sortedSteps);
        this.props.setChosenSteps(sortedSteps);
        //this.props.setCurrentBlock(clickedBlock, this.props.userId);
    };

    createData() {
        this.props.blocks.map(block => {
            const { name } = block;
            return name;
        })
    };

    async componentDidMount() {
        const { userId, blocks, steps, currentBlock } = this.props;
        this.props.fetchBlocks();
        this.props.fetchSteps();

        await this.props.getCurrentBlock(userId);
        await this.props.getFinishedBlocks(userId);

        const currentSteps = steps.filter(step => blocks[0].steps.includes(step._id));
        const sortedCurrentSteps = currentSteps.sort((a, b) => sortNumber(a.id, b.id));

        if (!currentBlock._id) {
            await this.props.setCurrentBlock(blocks[0], userId);
            await this.props.setCurrentStep(sortedCurrentSteps[0], userId);
        }

        console.log(this.props.userProfile);
        this.renderBlocks();
    }

    componentWillReceiveProps(newProps) {
        if (this.props.chosenSteps !== newProps.chosenSteps) this.props.history.push('/lessons');
    }

    render() {

        const { classes, blocks, steps } = this.props;
        const { items } = this.state;

        return (
            <TableContainer component={Paper}>
                <Table aria-label="customized table">
                    <TableBody className={classes.table}>
                        { items.map(block => {
                            const { _id, name, steps, status } = block;

                            return (
                                <TableRow
                                    onLoad={this.renderBlocks}
                                    className={classes.tableItem}
                                    onClick={() => this.filterSteps(steps, block)}
                                    key={ name }
                                >
                                    <TableCell component="th" scope="row">
                                        { name }
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        { status }
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }
}

const mapStateToProps = state => ({
    blocks: state.blockData.blocks,
    steps: state.stepData.steps,
    isLogged: state.validation.isLogged,
    error: state.validation.error,
    isLoading: state.validation.isLoading,
    userName: state.validation.userProfile.userName,
    currentBlocks: state.validation.userProfile.currentBlocks,
    currentBlock: state.validation.userProfile.currentBlock,
    chosenSteps: state.stepData.chosenSteps,
    userId: state.validation.userProfile.userId,
    currentStep: state.validation.userProfile.currentStep,
    userProfile: state.validation.userProfile,
    finishedBlocks: state.validation.userProfile.finishedBlocks,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchBlocks: () => dispatch(fetchBlocks()),
        fetchSteps: () => dispatch(fetchSteps()),
        setChosenSteps: (steps) => dispatch(setChosenSteps(steps)),
        setCurrentBlock: (block, userId) => dispatch(setCurrentBlock(block, userId)),
        setCurrentStep: (step, userId) => dispatch(setCurrentStep(step, userId)),
        getFinishedBlocks: (userId) => dispatch(getFinishedBlocks(userId)),
        getCurrentBlock: (userId) => dispatch(getCurrentBlock(userId)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps,
)(injectStyles(styles)(BlocksList)))
