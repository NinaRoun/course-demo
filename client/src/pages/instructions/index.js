import React, { Component } from 'react';
import injectStyles from 'react-jss';
import connect from "react-redux/es/connect/connect";
import UserCard from '../../components/UserCard.js';
import styles from '../../styles.js';
import CollapseButton from "../../components/CollapseButton";

const minPageSize = 700;

class InstructionsPage extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
        };
    }

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {

        const { pageIsLarge } = this.state;
        const { classes } = this.props;

        return (
            <div className={classes.pageContent}>

                { !pageIsLarge && <CollapseButton></CollapseButton> }

                <div className={classes.profileContent}>

                    { pageIsLarge && <div className={classes.userCardGrid}><UserCard /></div> }

                    <div className={classes.contentGrid}>
                        <h2 className={classes.pageTitle}>Instructions</h2>
                        <div className={classes.pageText}>
                            <p>CSS-in-JS avoids these problems entirely by generating unique class names when styles are converted to CSS. This allows you to think about styles on a component level with out worrying about styles defined elsewhere.</p>
                            <p>In this course, you will learn how to express popular SCSS (Sass) language features using latest JavaScript features. We will convert simple examples from SCSS to CSS-in-JS. As a designer or (S)CSS developer, you should be able to follow without extensive JavaScript knowledge, understanding SCSS is required though. We will not be using any particular CSSinJS libraries. Instead, we will focus on a base knowledge you need later if you use any CSSinJS library.</p>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    isLogged: state.validation.isLogged,
});

// const mapDispatchToProps = dispatch => {
//     return {
//         setWindowSize: () => dispatch(setWindowSize()),
//     }
// };

export default connect(
    mapStateToProps,
    //mapDispatchToProps
)(injectStyles(styles)(InstructionsPage));
