import React, { Component } from 'react';
import injectStyles from "react-jss";
import styles from '../../styles.js';

class AuthError extends Component {

    render() {

        const { error, classes } = this.props;
        console.log(error);

        return (
            <div className={classes.registrationError}>

                - {error.msg}

            </div>
        );
    }
}

export default injectStyles(styles)(AuthError)
