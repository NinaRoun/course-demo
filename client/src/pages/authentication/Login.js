import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { loginUser } from '../../actions/loginActions.js';
import styles from '../../styles.js';

class Login extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            email: "",
            password: "",
        };

        this.submitLoginForm = this.submitLoginForm.bind(this);
    }

    changeUserEmail(event) {
        this.setState({email: event.target.value});
    }

    changePassword(event) {
        this.setState({password: event.target.value});
    }

    submitLoginForm(event) {
        event.preventDefault();
        const { email, password } = this.state;
        this.props.loginUser({ email, password });
    }

    componentWillReceiveProps(newProps) {
        const { isLogged, isAdmin, userName } = this.props;
        console.log('is admin ', isAdmin);
        console.log('username ', userName);
        if(isLogged !== newProps.isLogged) {
            if (isAdmin) this.props.history.push('/admin');
            else this.props.history.push('/profile');
        } else {
            return null;
        }
    }

    render() {

        const { classes, changeForm } = this.props;
        const { email, password } = this.state;

        return (
            <div className={classes.authForm} id="container">

                <div className={[classes.formContainer, classes.signInUpContainer].join(' ')}>
                    <form action="#">
                        <h1>Sign in</h1>
                        <span>or use your account</span>
                        <input
                            type="email"
                            placeholder="Email"
                            value={email}
                            onChange={event => this.changeUserEmail(event)}
                        />
                        <input
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={event => this.changePassword(event)}
                        />
                        <a href="#">Forgot your password?</a>
                        <button onClick={this.submitLoginForm}>Sign In</button>
                    </form>
                </div>

                <div className={classes.overlayContainer}>
                    <div className={classes.overlay}>
                        <div className={[classes.overlayPanel, classes.overlayPanelRight].join(' ')}>
                            <h1>Hello, Friend!</h1>
                            <p>Enter your personal details and start journey with us</p>
                            <button className={classes.ghostButton} id="signUp" onClick={changeForm}>Sign Up</button>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    isLogged: state.validation.isLogged,
    error: state.validation.error,
    isLoading: state.validation.isLoading,
    userName: state.validation.userProfile.userName,
    isAdmin: state.validation.userProfile.isAdmin,
});

const mapDispatchToProps = dispatch => {
    return {
        loginUser: (credentials) => dispatch(loginUser(credentials)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(Login)))
