import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import Login from './Login.js';
import Register from './Register.js';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import styles from '../../styles.js';

class ProfilePage extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            showSignIn: true,
        };

        this.changeForm = this.changeForm.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    changeForm() {
        //console.log('changeForm');
        const { showSignIn } = this.state;
        this.setState({ showSignIn: !showSignIn });
    };

    handleClose() {
        //console.log('handleClose');
        this.setState({ anchorEl: null })
    };

    render() {

        const { classes, userName } = this.props;
        console.log('!!!!!!!!!! username ', userName);
        const { showSignIn } = this.state;

        return (
            <div className={classes.authBody}>

                { showSignIn ?
                    <Register changeForm={this.changeForm}></Register> :
                    <Login changeForm={this.changeForm}></Login>
                }

            </div>
        );
    }
}

const mapStateToProps = state => ({
    //blocks: state.blockData.blocks,
    isLogged: state.validation.isLogged,
    error: state.validation.error,
    isLoading: state.validation.isLoading,
    userName: state.validation.userProfile.userName,
    //currentCourse: state.userProfile.currentCourse,
    currentBlocks: state.validation.userProfile.currentBlocks,
});

// const mapDispatchToProps = dispatch => {
//     return {
//     }
// };

export default withRouter(connect(
    mapStateToProps,
    // mapDispatchToProps
)(injectStyles(styles)(ProfilePage)))
