import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { registerNewUser } from '../../actions/registerActions.js';
import AuthError from './AuthError.js';
import styles from '../../styles.js';

class Register extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            userName: '',
            userEmail: '',
            firstPassword: '',
            secondPassword: '',
        };

        this.submitRegisterForm = this.submitRegisterForm.bind(this);
    }

    changeUserName(event) {
        this.setState({userName: event.target.value});
    }

    changeUserEmail(event) {
        this.setState({userEmail: event.target.value});
    }

    changeFirstPassword(event) {
        this.setState({firstPassword: event.target.value});
    }

    changeSecondPassword(event) {
        this.setState({secondPassword: event.target.value});
    }

    submitRegisterForm(event) {
        event.preventDefault();
        const { userName, userEmail, firstPassword, secondPassword } = this.state;
        console.log('username ' + userName);
        console.log('user email ' + userEmail);
        console.log('1st pas ' + firstPassword);
        console.log('2nd pas ' + secondPassword);
        this.props.registerNewUser({ userName, userEmail, firstPassword, secondPassword })
    }

    componentWillReceiveProps(newProps) {
        console.log('inReceiveProps');
        const previousIsRegistered = this.props.isRegistered;
        if(previousIsRegistered !== newProps.isRegistered) {
            console.log('coincidence, ', newProps.isRegistered);
            this.props.changeForm();
        } else {
            console.log(newProps.isRegistered);
            return null;
        }
    }

    render() {

        const { classes, changeForm, errors } = this.props;
        const { userName, userEmail, firstPassword, secondPassword } = this.state;
        const renderErrors = errors && errors.map((error, index) => <AuthError error={error} key={index}/>);

        return (
            <div className={classes.authForm} id="container">
                <div className={[classes.formContainer, classes.signInUpContainer].join(' ')}>
                    <form action="#">
                        <h1>Create Account</h1>
                        {errors && renderErrors}
                        { !errors && <span>or use your email for registration</span>}
                        <input
                            type="text"
                            placeholder="Name"
                            value={userName}
                            onChange={event => this.changeUserName(event)}
                        />
                        <input
                            type="email"
                            placeholder="Email"
                            value={userEmail}
                            onChange={event => this.changeUserEmail(event)}
                        />
                        <input
                            type="password"
                            placeholder="Password"
                            value={firstPassword}
                            onChange={event => this.changeFirstPassword(event)}
                        />
                        <input
                            type="password"
                            placeholder="Confirm password"
                            value={secondPassword}
                            onChange={event => this.changeSecondPassword(event)}
                        />
                        <button
                            onClick={this.submitRegisterForm}
                        >Sign Up</button>
                    </form>
                </div>

                <div className={classes.overlayContainer}>
                    <div className={classes.overlay}>
                        <div className={[classes.overlayPanel, classes.overlayPanelRight].join(' ')}>
                            <h1>Welcome Back!</h1>
                            <p>To keep connected with us please login with your personal info</p>
                            <button className={classes.ghostButton} onClick={changeForm} id="signIn">Sign In</button>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    isRegistered: state.registration.isRegistered,
    errors: state.registration.errors,
    isLoading: state.registration.isLoading,
});

const mapDispatchToProps = dispatch => {
    return {
        registerNewUser: (credentials) => dispatch(registerNewUser(credentials)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(Register)))
