import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { fetchUsers } from "../../actions/userActions";
import { fetchSteps } from "../../actions/stepsActions";
import { formatDate } from "../../helpers";
import styles from '../../styles.js';
import Breadcrumbs from "@material-ui/core/Breadcrumbs/Breadcrumbs";
import Typography from "@material-ui/core/Typography/Typography";
import AdminNavLinks from "../../components/AdminNavLinks";
import Link from "@material-ui/core/Link/Link";

const minPageSize = 700;

class SingleUser extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
        };

        this.handleNavClick = this.handleNavClick.bind(this);
    }

    async componentDidMount() {
        await this.props.fetchSteps();
    }

    handleNavClick(event, link) {
        event.preventDefault();
        this.props.history.push(link);
    }

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    async componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        await this.props.fetchUsers();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {

        const { classes, currentUser } = this.props;

        const renderBreadcrumbs = () => {
            return <div className={classes.adNav}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/users" onClick={event => this.handleNavClick(event, '/users')}>
                        Users
                    </Link>
                    <Typography color="textPrimary">Users</Typography>
                </Breadcrumbs>

                <AdminNavLinks />

            </div>
        };

        return (
            <div className={classes.adminContent}>

                { renderBreadcrumbs() }

                <div className={classes.adUserInfoItem}>
                    <div className={classes.adHeader}>Name: {currentUser.name}</div>
                    <div className={classes.adHeader}>email: {currentUser.email}</div>
                </div>

                <div className={classes.adUserHeader}>Register date: {formatDate(currentUser.createdAt)}</div>

                <div className={classes.adUserHeader}>Last update: {formatDate(currentUser.updatedAt)}</div>

                <div className={classes.adUserHeader}>Current block: {currentUser.currentBlock.name}</div>

                <div className={classes.adUserHeader}>Current step: {currentUser.currentStep.name}</div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    steps: state.stepData.steps,
    users: state.admin.users,
    currentUser: state.admin.currentUser,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchSteps: () => dispatch(fetchSteps()),
        fetchUsers: () => dispatch(fetchUsers()),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(SingleUser)))
