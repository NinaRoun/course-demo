import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { fetchBlocks } from "../../actions/blocksActions";
import {
    fetchTests,
    putTest,
    deleteTest,
} from "../../actions/testsActions";
import {
    setCurrentTest,
    orderTestsNameAsc,
    orderTestsNameDesc,
    orderTestsIdAsc,
    orderTestsIdDesc,
} from "../../actions/adminActions";
import { fetchQuestions, setCurrentQuestions } from "../../actions/questionsActions";
import { formatDate } from "../../helpers";
import styles from '../../styles.js';
import Breadcrumbs from "@material-ui/core/Breadcrumbs/Breadcrumbs";
import Typography from "@material-ui/core/Typography/Typography";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import TableContainer from "@material-ui/core/TableContainer/TableContainer";
import AdminNavLinks from "../../components/AdminNavLinks";
import IconButton from "@material-ui/core/IconButton/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button/Button";
import ConfirmDeleteModal from "../../components/ConfirmDeleteModal";

const minPageSize = 700;

class TestsTable extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            isNewTestAdded: false,
            isAlertOpen: false,
            pageIsLarge: null,
            setBlock: '',
            addNewName: '',
            addNewId: '',
            addNewSummary: '',
            deleteModalIsOpen: false,
            testToDelete: '',
        };

        this.saveTest = this.saveTest.bind(this);
        this.addTest = this.addTest.bind(this);
        this.handleBlockChange = this.handleBlockChange.bind(this);
        this.changeNewName = this.changeNewName.bind(this);
        this.changeNewId = this.changeNewId.bind(this);
        this.changeNewSummary = this.changeNewSummary.bind(this);
        this.handleNavClick = this.handleNavClick.bind(this);
        this.setIdOrder = this.setIdOrder.bind(this);
        this.setNameOrder = this.setNameOrder.bind(this);
        this.handlePageClick = this.handlePageClick.bind(this);
        this.openSingleTest = this.openSingleTest.bind(this);
        this.deleteChosenItem = this.deleteChosenItem.bind(this);
        this.cancelDeletion = this.cancelDeletion.bind(this);
        this.cancelAdding = this.cancelAdding.bind(this);
    }

    async componentDidMount() {
        await this.props.fetchSteps();
    }

    handleOpenCloseModal(test) {
        const { deleteModalIsOpen } = this.state;

        if (test._id) this.setState({ testToDelete: test._id });
        this.setState({ deleteModalIsOpen: !deleteModalIsOpen });
    }

    async deleteChosenItem() {
        const { testToDelete } = this.state;

        const block = this.props.blocks.find(block => block.tests.includes(testToDelete));
        await this.props.deleteTest(testToDelete, block._id);
        await this.props.fetchTests();
        this.setState({ deleteModalIsOpen: false, testToDelete: '' });
    }

    cancelDeletion() {
        this.setState({ deleteModalIsOpen: false, testToDelete: '' });
    }

    changeNewName(event) {
        event.preventDefault();
        this.setState({ addNewName: event.target.value });
    }

    changeNewId(event) {
        event.preventDefault();
        this.setState({ addNewId: event.target.value });
    }

    changeNewSummary(event) {
        event.preventDefault();
        this.setState({ addNewSummary: event.target.value });
    }

    handleNavClick(event, link) {
        event.preventDefault();
        this.props.history.push(link);
    }

    async setNameOrder() {
        const { isAscTestNameOrder } = this.props;

        isAscTestNameOrder ? await this.props.orderTestsNameDesc() : await this.props.orderTestsNameAsc();
    }

    async setIdOrder() {
        const { isAscTestIdOrder } = this.props;

        isAscTestIdOrder ? await this.props.orderTestsIdDesc() : await this.props.orderTestsIdAsc();
    }

    handlePageClick(page) {
        this.setState({ currentPage: page });
    }

    openSingleTest(test) {
        const { questions, history, setCurrentTest, setCurrentQuestions } = this.props;
        setCurrentTest(test);
        const filteredQuestions = questions.length && questions.filter(question => test.questions.includes(question._id));
        setCurrentQuestions(filteredQuestions);
        history.push('/test');
    }

    handleBlockChange(event) {
        event.preventDefault();
        this.setState({ setBlock: event.target.value });
    }

    addTest() {
        const { isNewTestAdded } = this.state;

        !isNewTestAdded && this.setState({ isNewTestAdded: !isNewTestAdded });
    }

    async saveTest() {
        const {
            setBlock,
            isNewTestAdded,
            addNewName,
            addNewId,
            addNewSummary,
        } = this.state;

        if(isNaN(Number(addNewId))) this.setState({ notNumberAlertOpen: true });
        else if (addNewName && addNewId){
            await this.props.putTest(addNewId, addNewName, addNewSummary, setBlock);
            this.setState({
                addNewName: '',
                addNewId: '',
                addNewSummary: '',
                isAlertOpen: false,
                isNewTestAdded: !isNewTestAdded,
                notNumberAlertOpen: false,
            });
        } else {
            this.setState({ isAlertOpen: true });
        }
    }

    cancelAdding() {
        this.setState({
            isNewTestAdded: false,
            isAlertOpen: false,
            notNumberAlertOpen: false,
        });
    }

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    async componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        await this.props.fetchTests();
        await this.props.fetchBlocks();
        await this.props.fetchQuestions();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {

        const {
            isNewTestAdded,
            setBlock,
            deleteModalIsOpen,
            isAlertOpen,
            notNumberAlertOpen,
        } = this.state;

        const { classes,
            tests,
            blocks,
            isAscTestNameOrder,
            isAscTestIdOrder,
        } = this.props;

        const renderBreadcrumbs = () => {
            return <div className={classes.adNav}>

                <div>
                    <Breadcrumbs aria-label="breadcrumb">
                        <Typography color="textPrimary">Tests</Typography>
                    </Breadcrumbs>
                </div>

                <AdminNavLinks />

            </div>
        };

        const renderSelectBlock = () => {
            return <div className={classes.adSelect}>
                <FormControl variant="outlined">
                    <InputLabel id="demo-simple-select-outlined-label">Block</InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        value={setBlock}
                        onChange={(event) => this.handleBlockChange(event)}
                        label="Block"
                    >
                        {
                            blocks.map(block => <MenuItem
                                className={classes.adSelectItem}
                                value={block._id}
                                key={block._id}
                            >
                                {block.name}
                                </MenuItem>)
                        }
                    </Select>
                </FormControl>
            </div>
        };

        const renderAddTests = () => {
            return isNewTestAdded ?
                <div className={classes.adCreateItemContainer}>
                    <h4>Creating a new test</h4>

                    <div className={isAlertOpen ? classes.warningIsOpen : classes.warningIsClosed}>
                        Name and id should be filled
                    </div>

                    <div className={notNumberAlertOpen ? classes.warningIsOpen : classes.warningIsClosed}>
                        Id should be a plain number
                    </div>

                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        className={classes.newTextareaName}
                        rows="1"
                        defaultValue='Name'
                        variant="outlined"
                        onChange={event => this.changeNewName(event)}
                    />
                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        className={classes.newTextarea}
                        rows="1"
                        defaultValue='Id'
                        variant="outlined"
                        onChange={event => this.changeNewId(event)}
                    />

                    {renderSelectBlock()}

                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        className={classes.newTextarea}
                        rows="8"
                        defaultValue='Summary'
                        variant="outlined"
                        onChange={event => this.changeNewSummary(event)}
                    />

                    <div className={classes.adAddItemContainer}>
                        <div className={classes.adModalButtonContainer}>
                            <Button
                                variant="contained"
                                color="secondary"
                                onClick={this.cancelAdding}
                            >
                                Cancel
                            </Button>
                            <Button
                                onClick={this.saveTest}
                                variant="contained"
                                color="primary"
                            >
                                Save new test
                            </Button>
                        </div>
                    </div>

                </div> :
                <div>
                    <div className={classes.adAddItemContainer}>
                        <Button
                            onClick={this.addTest}
                            variant="contained"
                        >
                            Add a test
                        </Button>
                    </div>
                </div>
        };

        return (
            <div className={classes.adminContent}>

                <ConfirmDeleteModal
                    deleteModalIsOpen={deleteModalIsOpen}
                    handleOpenCloseModal={this.handleOpenCloseModal}
                    cancelDeletion={this.cancelDeletion}
                    deleteChosenItem={this.deleteChosenItem}
                >
                </ConfirmDeleteModal>

                { renderBreadcrumbs() }

                <div className={classes.adFilterContainer}>
                    <div className={classes.adNavItem}>Order by:</div>

                    <div className={classes.adNavItem}>name</div>
                    {
                        isAscTestNameOrder ?
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setNameOrder}
                            >
                                <ArrowDownward/>
                            </div> :
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setNameOrder}
                            >
                                <ArrowUpward/>
                            </div>
                    }

                    <div className={classes.adNavItem}>id</div>
                    {
                        isAscTestIdOrder ?
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setIdOrder}
                            >
                                <ArrowDownward/>
                            </div> :
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setIdOrder}
                            >
                                <ArrowUpward/>
                            </div>
                    }
                </div>

                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell className={classes.adTableHeader}>Name</TableCell>
                                <TableCell align="center" className={classes.adTableHeader}>Order number</TableCell>
                                <TableCell align="right" className={classes.adTableHeader}>Delete</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tests ? tests.map(test => {
                                const { name, id } = test;
                                return (
                                    <TableRow
                                        key={name}
                                        className={classes.adTableRow}
                                    >
                                        <TableCell
                                            component="th"
                                            scope="row"
                                            onClick={() => this.openSingleTest(test)}
                                        >
                                            {name}
                                        </TableCell>
                                        <TableCell
                                            align="center"
                                            className={classes.adTableOrderNumber}
                                            onClick={() => this.openSingleTest(test)}
                                        >
                                            {id}
                                        </TableCell>
                                        <TableCell
                                            align="right"
                                            className={classes.adDeleteButton}
                                            onClick={() => this.handleOpenCloseModal(test)}
                                        >
                                            <IconButton
                                                aria-label="delete"
                                                color="secondary"
                                            >
                                                <DeleteIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                )
                            }) : null }
                        </TableBody>
                    </Table>
                </TableContainer>

                {renderAddTests()}

            </div>
        );
    }
}

const mapStateToProps = state => ({
    tests: state.testData.tests,
    blocks: state.blockData.blocks,
    questions: state.questionsData.questions,
    isAscTestNameOrder: state.testData.isAscTestNameOrder,
    isAscTestIdOrder: state.testData.isAscTestIdOrder,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchTests: () => dispatch(fetchTests()),
        fetchBlocks: () => dispatch(fetchBlocks()),
        putTest: (id, name, summary, _id) => dispatch(putTest(id, name, summary, _id)),
        deleteTest: (testId, blockId) => dispatch(deleteTest(testId, blockId)),
        setCurrentTest: (test) => dispatch(setCurrentTest(test)),
        orderTestsNameAsc: () => dispatch(orderTestsNameAsc()),
        orderTestsNameDesc: () => dispatch(orderTestsNameDesc()),
        orderTestsIdAsc: () => dispatch(orderTestsIdAsc()),
        orderTestsIdDesc: () => dispatch(orderTestsIdDesc()),
        fetchQuestions: () => dispatch(fetchQuestions()),
        setCurrentQuestions: (questions) => dispatch(setCurrentQuestions(questions)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(TestsTable)))
