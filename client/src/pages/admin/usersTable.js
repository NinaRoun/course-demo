import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { fetchUsers,
    fetchAscUsers,
    orderUsersAsc,
    orderUsersDesc
} from "../../actions/userActions";
import { fetchSteps } from "../../actions/stepsActions";
import { setCurrentUser } from "../../actions/adminActions";
import { formatDate } from "../../helpers";
import styles from '../../styles.js';
import Breadcrumbs from "@material-ui/core/Breadcrumbs/Breadcrumbs";
import Typography from "@material-ui/core/Typography/Typography";
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import TableContainer from "@material-ui/core/TableContainer/TableContainer";
import AdminNavLinks from "../../components/AdminNavLinks";
import Pagination from "../../components/Pagination";

const minPageSize = 700;

class UsersTable extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
            currentPage: 1,
            usersPerPage: 4,
        };

        this.handleNavClick = this.handleNavClick.bind(this);
        this.setRegisterOrder = this.setRegisterOrder.bind(this);
        this.setNameOrder = this.setNameOrder.bind(this);
        this.handlePageClick = this.handlePageClick.bind(this);
        this.openSingleUser = this.openSingleUser.bind(this);
    }

    async componentDidMount() {
        await this.props.fetchSteps();
    }

    handleNavClick(event, link) {
        event.preventDefault();
        this.props.history.push(link);
    }

    async setNameOrder() {
        const { isAscUserNameOrder } = this.props;

        isAscUserNameOrder ? await this.props.orderUsersDesc() : await this.props.orderUsersAsc();
    }

    async setRegisterOrder() {
        const { isAscUserRegistrationOrder } = this.props;

        isAscUserRegistrationOrder ? await this.props.fetchUsers() : await this.props.fetchAscUsers();
    }

    handlePageClick(page) {
        this.setState({ currentPage: page });
    }

    openSingleUser(user) {
        this.props.setCurrentUser(user);
        this.props.history.push('/user');
    }

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    async componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        await this.props.fetchUsers();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {

        const { currentPage, usersPerPage } = this.state;
        const { classes, users, isAscUserNameOrder, isAscUserRegistrationOrder } = this.props;

        const indexOfLastItem = currentPage * usersPerPage;
        const indexOfFirstItem = indexOfLastItem - usersPerPage;
        const currentUsers = users.slice(indexOfFirstItem, indexOfLastItem);

        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(users.length / usersPerPage); i++) {
            pageNumbers.push(i);
        }

        const renderPageNumbers = () => pageNumbers.map(number => {
            return (
                <Pagination
                    key={number}
                    id={number}
                    number={number}
                    onClick={() => this.handlePageClick(number)}
                    className={number === currentPage ? classes.activePage : ""}
                />)});

        const renderBreadcrumbs = () => {
            return <div className={classes.adNav}>

                <div>
                    <Breadcrumbs aria-label="breadcrumb">
                        <Typography color="textPrimary">Users</Typography>
                    </Breadcrumbs>
                </div>

                <AdminNavLinks />

            </div>
        };

        return (
            <div className={classes.adminContent}>

                { renderBreadcrumbs() }

                <div className={classes.adFilterContainer}>
                    <div className={classes.adNavItem}>Order by:</div>

                    <div className={classes.adNavItem}>name</div>
                    {
                        isAscUserNameOrder ?
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setNameOrder}
                            >
                                <ArrowDownward/>
                            </div> :
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setNameOrder}
                            >
                                <ArrowUpward/>
                            </div>
                    }

                    <div className={classes.adNavItem}>registration</div>
                    {
                        isAscUserRegistrationOrder ?
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setRegisterOrder}
                            >
                                <ArrowDownward/>
                            </div> :
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setRegisterOrder}
                            >
                                <ArrowUpward/>
                            </div>
                    }
                </div>

                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell className={classes.adTableHeader}>Name</TableCell>
                                <TableCell className={classes.adTableHeader}>Email</TableCell>
                                <TableCell className={classes.adTableHeader}>Registration date</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {currentUsers ? currentUsers.map(user => {
                                const { name, email, createdAt } = user;
                                return (
                                    <TableRow
                                        key={name}
                                        className={classes.adTableRow}
                                        onClick={() => this.openSingleUser(user)}
                                    >
                                        <TableCell
                                            component="th"
                                            scope="row"
                                        >
                                            {name}
                                        </TableCell>
                                        <TableCell
                                            component="th"
                                            scope="row"
                                        >
                                            {email}
                                        </TableCell>
                                        <TableCell
                                            component="th"
                                            scope="row"
                                        >
                                            {formatDate(createdAt)}
                                        </TableCell>
                                    </TableRow>
                                )
                            }) : null }
                        </TableBody>
                    </Table>
                </TableContainer>

                <div className={classes.paginationContainer}>
                    {renderPageNumbers()}
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    steps: state.stepData.steps,
    users: state.admin.users,
    isAscUserNameOrder: state.admin.isAscUserNameOrder,
    isAscUserRegistrationOrder: state.admin.isAscUserRegistrationOrder,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchSteps: () => dispatch(fetchSteps()),
        fetchUsers: () => dispatch(fetchUsers()),
        setCurrentUser: (user) => dispatch(setCurrentUser(user)),
        fetchAscUsers: () => dispatch(fetchAscUsers()),
        orderUsersAsc: () => dispatch(orderUsersAsc()),
        orderUsersDesc: () => dispatch(orderUsersDesc()),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(UsersTable)))
