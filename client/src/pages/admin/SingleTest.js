import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { fetchTests } from "../../actions/testsActions";
import { updateTest } from "../../actions/adminActions";
import {
    fetchQuestions,
    putQuestion,
    deleteQuestion,
    setCurrentQuestion,
}  from "../../actions/questionsActions";
import styles from '../../styles.js';
import Breadcrumbs from "@material-ui/core/Breadcrumbs/Breadcrumbs";
import Typography from "@material-ui/core/Typography/Typography";
import AdminNavLinks from "../../components/AdminNavLinks";
import Link from "@material-ui/core/Link/Link";
import IconButton from "@material-ui/core/IconButton/IconButton";
import DoneIcon from '@material-ui/icons/Done';
import EditIcon from "@material-ui/icons/Edit";
import TextField from "@material-ui/core/TextField/TextField";
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import DeleteIcon from "@material-ui/icons/Delete";
import TableContainer from "@material-ui/core/TableContainer/TableContainer";
import Button from "@material-ui/core/Button/Button";
import ConfirmDeleteModal from "../../components/ConfirmDeleteModal";

const minPageSize = 700;

class SingleTest extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
            isEdited: false,
            newName: '',
            newId: 0,
            newSummary: '',
            prevName: '',
            prevId: 0,
            prevSummary: '',
            addNewText: '',
            addNewId: 0,
            isAlertOpen: false,
            notNumberAlertOpen: false,
            isNewQuestionAdded: false,
            deleteModalIsOpen: false,
            questionToDelete: '',
        };

        this.handleNavClick = this.handleNavClick.bind(this);
        this.changeName = this.changeName.bind(this);
        this.changeId = this.changeId.bind(this);
        this.changeSummary = this.changeSummary.bind(this);
        this.addQuestion = this.addQuestion.bind(this);
        this.changeNewText = this.changeNewText.bind(this);
        this.changeNewId = this.changeNewId.bind(this);
        this.saveQuestion = this.saveQuestion.bind(this);
        this.cancelAdding = this.cancelAdding.bind(this);
        this.deleteChosenItem = this.deleteChosenItem.bind(this);
        this.cancelDeletion = this.cancelDeletion.bind(this);
        this.openSingleQuestion = this.openSingleQuestion.bind(this);
        this.editTest = this.editTest.bind(this);
        this.saveTest = this.saveTest.bind(this);
    }

    async componentDidMount() {
        await this.props.fetchSteps();
    }

    handleNavClick(event, link) {
        event.preventDefault();
        this.props.history.push(link);
    }

    changeName(event) {
        event.preventDefault();
        this.setState({ newName: event.target.value });
    }

    changeId(event) {
        event.preventDefault();
        this.setState({ newId: event.target.value });
    }

    changeSummary(event) {
        event.preventDefault();
        this.setState({ newSummary: event.target.value });
    }

    editTest() {
        const { isEdited } = this.state;

        !isEdited && this.setState({ isEdited: !isEdited });
    }

    async saveTest() {
        const { currentTest } = this.props;
        const { isEdited, newName, newId, newSummary, prevName, prevId, prevSummary } = this.state;

        if (newName !== prevName || newSummary !== prevSummary || newId !== prevId){
            this.setState({ prevName: newName, prevSummary: newSummary, prevId: newId });
            await this.props.updateTest(currentTest._id, { name: newName, id: newId, summary: newSummary });
        }

        isEdited && this.setState({ isEdited: !isEdited });

    }

    openSingleQuestion(question) {
        //console.log('single question ', question);
        const { history, setCurrentQuestion } = this.props;
        setCurrentQuestion(question);
        history.push('/question');
    }

    handleOpenCloseModal(question) {
        const { deleteModalIsOpen } = this.state;

        if (question._id) this.setState({ questionToDelete: question._id });
        this.setState({ deleteModalIsOpen: !deleteModalIsOpen });
    }

    async deleteChosenItem() {
        const { questionToDelete } = this.state;
        const { currentTest, deleteQuestion } = this.props;

        await deleteQuestion(questionToDelete, currentTest._id);
        this.setState({ deleteModalIsOpen: false, questionToDelete: '' });
    }

    cancelDeletion() {
        this.setState({ deleteModalIsOpen: false, questionToDelete: '' });
    }

    addQuestion() {
        const { isNewQuestionAdded } = this.state;

        !isNewQuestionAdded && this.setState({ isNewQuestionAdded: !isNewQuestionAdded });
    }

    changeNewText(event) {
        event.preventDefault();
        this.setState({ addNewText: event.target.value });
    }

    changeNewId(event) {
        event.preventDefault();
        this.setState({ addNewId: event.target.value });
    }

    async saveQuestion() {
        const { isNewQuestionAdded, addNewText, addNewId } = this.state;
        const { currentTest } = this.props;

        if(isNaN(Number(addNewId))) this.setState({ notNumberAlertOpen: true });
        else if (addNewText && addNewId){
            await this.props.putQuestion(addNewId, addNewText, currentTest._id);
            this.setState({
                addNewText: '',
                addNewId: '',
                isAlertOpen: false,
                isNewQuestionAdded: !isNewQuestionAdded,
                notNumberAlertOpen: false,
            });

            await this.props.fetchQuestions();
            await this.props.fetchTests();
        }
        else {
            this.setState({ isAlertOpen: true });
        }
    }

    cancelAdding() {
        this.setState({
            isNewQuestionAdded: false,
            isAlertOpen: false,
            notNumberAlertOpen: false,
        });
    }

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    async componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        await this.props.fetchTests();
        await this.props.fetchQuestions();

        const { currentTest } = this.props;
        const { id, name, summary } = currentTest;
        this.setState({
            newName: name,
            newId: id,
            newSummary: summary,
            prevName: name,
            prevId: id,
            prevSummary: summary,
        });
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {

        const { classes, currentQuestions } = this.props;
        const {
            isEdited,
            newName,
            newId,
            newSummary,
            deleteModalIsOpen
        } = this.state;

        const renderBreadcrumbs = () => {
            return <div className={classes.adNav}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/tests" onClick={event => this.handleNavClick(event, '/tests')}>
                        Tests
                    </Link>
                    <Typography color="textPrimary">Test</Typography>
                </Breadcrumbs>

                <AdminNavLinks />

            </div>
        };

        const renderQuestions = () => {
            return <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell className={classes.adTableHeader}>Question</TableCell>
                            <TableCell align="center" className={classes.adTableHeader}>Order number</TableCell>
                            <TableCell align="right" className={classes.adTableHeader}>Delete</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        { currentQuestions.length ?
                            currentQuestions.map(question => {
                            const { text, id, _id } = question;
                            return (
                                <TableRow
                                    key={_id}
                                    className={classes.adTableRow}
                                >
                                    <TableCell
                                        component="th"
                                        scope="row"
                                        onClick={() => this.openSingleQuestion(question)}
                                    >
                                        {text}
                                    </TableCell>
                                    <TableCell
                                        align="center"
                                        className={classes.adTableOrderNumber}
                                        onClick={() => this.openSingleQuestion(question)}
                                    >
                                        {id}
                                    </TableCell>
                                    <TableCell
                                        align="right"
                                        className={classes.adDeleteButton}
                                        onClick={() => this.handleOpenCloseModal(question)}
                                    >
                                        <IconButton
                                            aria-label="delete"
                                            color="secondary"
                                        >
                                            <DeleteIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            )
                        }) : null }
                    </TableBody>
                </Table>
            </TableContainer>
        };

        const renderAddQuestions = () => {
            const { isNewQuestionAdded, isAlertOpen, notNumberAlertOpen } = this.state;

            return isNewQuestionAdded ?
                <div className={classes.adCreateItemContainer}>
                    <h4>Creating a new question</h4>

                    <div className={isAlertOpen ? classes.warningIsOpen : classes.warningIsClosed}>
                        Name and id should be filled
                    </div>

                    <div className={notNumberAlertOpen ? classes.warningIsOpen : classes.warningIsClosed}>
                        Id should be a plain number
                    </div>

                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        className={classes.newTextareaName}
                        rows="1"
                        defaultValue='Text'
                        variant="outlined"
                        onChange={event => this.changeNewText(event)}
                    />
                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        className={classes.newTextarea}
                        rows="1"
                        defaultValue='Id'
                        variant="outlined"
                        onChange={event => this.changeNewId(event)}
                    />
                    <div className={classes.adAddItemContainer}>
                        <div className={classes.adModalButtonContainer}>
                            <Button
                                variant="contained"
                                color="secondary"
                                onClick={this.cancelAdding}
                            >
                                Cancel
                            </Button>
                            <Button
                                onClick={this.saveQuestion}
                                variant="contained"
                                color="primary"
                            >
                                Save new question
                            </Button>
                        </div>
                    </div>
                </div> :
                <div>
                    <div className={classes.adAddItemContainer}>
                        <Button
                            onClick={this.addQuestion}
                            variant="contained"
                        >
                            Add a question
                        </Button>
                    </div>
                </div>
        };

        return (
            <div className={classes.adminContent}>

                <ConfirmDeleteModal
                    deleteModalIsOpen={deleteModalIsOpen}
                    handleOpenCloseModal={this.handleOpenCloseModal}
                    cancelDeletion={this.cancelDeletion}
                    deleteChosenItem={this.deleteChosenItem}
                >
                </ConfirmDeleteModal>

                { renderBreadcrumbs() }

                <div className={classes.adHeaderName}>Name:</div>
                {
                    !isEdited ?
                        <IconButton
                            className={classes.editButton}
                            aria-label="edit"
                            color="primary"
                            onClick={this.editTest}
                        >
                            <EditIcon />
                        </IconButton> :
                        <IconButton
                            className={classes.editButton}
                            aria-label="edit"
                            color="primary"
                            onClick={this.saveTest}
                        >
                            <DoneIcon />
                        </IconButton>
                }

                {
                    !isEdited ?
                        <div className={classes.adTextfieldName}>{ newName }</div> :
                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            rows="1"
                            defaultValue={newName}
                            variant="outlined"
                            onChange={event => this.changeName(event)}
                        />
                }

                <div className={classes.adHeader}>Order number:</div>
                {
                    !isEdited ?
                        <div className={classes.adTextfield}>{ newId ? newId : 'No id' }</div> :
                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            rows="1"
                            defaultValue={newId}
                            variant="outlined"
                            onChange={event => this.changeId(event)}
                        />
                }

                <div className={classes.adHeader}>Summary:</div>
                {
                    !isEdited ?
                        <div className={classes.adTextfield}>{ newSummary || 'No summary' }</div> :
                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            rows="8"
                            defaultValue={newSummary}
                            variant="outlined"
                            onChange={event => this.changeSummary(event)}
                        />
                }

                { renderQuestions() }

                { renderAddQuestions() }

            </div>
        );
    }
}

const mapStateToProps = state => ({
    tests: state.testData.tests,
    currentTest: state.testData.currentTest,
    questions: state.questionsData.questions,
    currentQuestions: state.questionsData.currentQuestions,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchTests: () => dispatch(fetchTests()),
        fetchQuestions: () => dispatch(fetchQuestions()),
        updateTest: (_id, { name, id, summary } ) => dispatch(updateTest(_id, { name, id, summary })),
        putQuestion: (id, text, testId) => dispatch(putQuestion(id, text, testId)),
        deleteQuestion: (id, testId) => dispatch(deleteQuestion(id, testId)),
        setCurrentQuestion: (question) => dispatch(setCurrentQuestion(question)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(SingleTest)))
