import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { fetchBlocks } from "../../actions/blocksActions";
import { fetchSteps } from "../../actions/stepsActions";
import {
    setBlockToEdit,
    setStepsToEdit,
    putBlock,
    deleteBlock,
    orderBlocksNameAsc,
    orderBlocksNameDesc,
    orderBlocksIdAsc,
    orderBlocksIdDesc,
} from "../../actions/adminActions";
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Paper from '@material-ui/core/Paper';
import styles from '../../styles.js';
import TextField from "@material-ui/core/TextField/TextField";
import AdminNavLinks from '../../components/AdminNavLinks.js';
import ConfirmDeleteModal from '../../components/ConfirmDeleteModal.js';

class BlocksList extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
            isEdited: false,
            deleteModalIsOpen: false,
            isNewBlockAdded: false,
            addNewName: '',
            addNewId: '',
            addNewSummary: '',
            blockToDelete: '',
            isAlertOpen: false,
            notNumberAlertOpen: false,
        };

        this.addBlock = this.addBlock.bind(this);
        this.handleOpenCloseModal = this.handleOpenCloseModal.bind(this);
        this.changeNewName = this.changeNewName.bind(this);
        this.changeNewId = this.changeNewId.bind(this);
        this.changeNewSummary = this.changeNewSummary.bind(this);
        this.saveBlock = this.saveBlock.bind(this);
        this.cancelAdding = this.cancelAdding.bind(this);
        this.cancelDeletion = this.cancelDeletion.bind(this);
        this.deleteChosenItem = this.deleteChosenItem.bind(this);
        this.handleNavClick = this.handleNavClick.bind(this);
        this.setIdOrder = this.setIdOrder.bind(this);
        this.setNameOrder = this.setNameOrder.bind(this);
    }

    async componentDidMount() {
        await this.props.fetchBlocks();
        await this.props.fetchSteps();
    }

    async setNameOrder() {
        const { isAscBlockNameOrder } = this.props;

        isAscBlockNameOrder ? await this.props.orderBlocksNameDesc() : await this.props.orderBlocksNameAsc();
    }

    async setIdOrder() {
        const { isAscBlockIdOrder } = this.props;

        isAscBlockIdOrder ? await this.props.orderBlocksIdDesc() : await this.props.orderBlocksIdAsc();
    }

    handleNavClick(event, link) {
        event.preventDefault();
        this.props.history.push(link);
    }

    addBlock() {
        const { isNewBlockAdded } = this.state;

        !isNewBlockAdded && this.setState({ isNewBlockAdded: !isNewBlockAdded });
    }

    changeNewName(event) {
        event.preventDefault();
        this.setState({ addNewName: event.target.value });
    }

    changeNewId(event) {
        event.preventDefault();
        this.setState({ addNewId: event.target.value });
    }

    changeNewSummary(event) {
        event.preventDefault();
        this.setState({ addNewSummary: event.target.value });
    }

    async saveBlock() {
        const { isNewBlockAdded, addNewName, addNewId, addNewSummary } = this.state;

        if(isNaN(Number(addNewId))) this.setState({ notNumberAlertOpen: true });
        else if (addNewName && addNewId){
            await this.props.putBlock(addNewId, addNewName, addNewSummary);
            this.setState({
                addNewName: '',
                addNewId: '',
                addNewSummary: '',
                isAlertOpen: false,
                isNewBlockAdded: !isNewBlockAdded,
                notNumberAlertOpen: false,
            });
            await this.props.fetchBlocks();
        }
        else {
            this.setState({ isAlertOpen: true });
        }
    }

    handleOpenCloseModal(block) {
        const { deleteModalIsOpen } = this.state;

        if (block._id) this.setState({ blockToDelete: block._id });
        this.setState({ deleteModalIsOpen: !deleteModalIsOpen });
    }

    async deleteChosenItem() {
        const { blockToDelete } = this.state;

        await this.props.deleteBlock(blockToDelete);
        await this.props.fetchBlocks();
        this.setState({ deleteModalIsOpen: false, blockToDelete: '' });
    }

    cancelDeletion() {
        this.setState({ deleteModalIsOpen: false, blockToDelete: '' });
    }

    cancelAdding() {
        this.setState({
            isNewBlockAdded: false,
            isAlertOpen: false,
            notNumberAlertOpen: false,
        });
    }

    openSingleBlock(block) {
        const { steps } = this.props;

        const filteredSteps = steps.filter(step => block.steps.includes(step._id));
        this.props.setBlockToEdit(block);
        this.props.setStepsToEdit(filteredSteps);
        this.props.history.push('/block');
    }

    render() {

        const { classes, blocks, isAscBlockNameOrder, isAscBlockIdOrder } = this.props;
        const { deleteModalIsOpen } = this.state;

        const renderAddBlocks = () => {
            const { isNewBlockAdded, isAlertOpen, notNumberAlertOpen } = this.state;

            return isNewBlockAdded ?
                <div className={classes.adCreateItemContainer}>
                    <h4>Creating a new block</h4>

                    <div className={isAlertOpen ? classes.warningIsOpen : classes.warningIsClosed}>
                        Name and id should be filled
                    </div>

                    <div className={notNumberAlertOpen ? classes.warningIsOpen : classes.warningIsClosed}>
                        Id should be a plain number
                    </div>

                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        className={classes.newTextareaName}
                        rows="1"
                        defaultValue='Name'
                        variant="outlined"
                        onChange={event => this.changeNewName(event)}
                    />
                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        className={classes.newTextarea}
                        rows="1"
                        defaultValue='Id'
                        variant="outlined"
                        onChange={event => this.changeNewId(event)}
                    />
                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        className={classes.newTextarea}
                        rows="8"
                        defaultValue='Summary'
                        variant="outlined"
                        onChange={event => this.changeNewSummary(event)}
                    />
                    <div className={classes.adAddItemContainer}>
                        <div className={classes.adModalButtonContainer}>
                            <Button
                                variant="contained"
                                color="secondary"
                                onClick={this.cancelAdding}
                            >
                                Cancel
                            </Button>
                            <Button
                                onClick={this.saveBlock}
                                variant="contained"
                                color="primary"
                            >
                                Save new block
                            </Button>
                        </div>
                    </div>
                </div> :
                <div>
                    <div className={classes.adAddItemContainer}>
                        <Button
                            onClick={this.addBlock}
                            variant="contained"
                        >
                            Add a block
                        </Button>
                    </div>
                </div>
        };

        const renderBreadcrumbs = () => {
            return <div className={classes.adNav}>

                <div>
                    <Breadcrumbs aria-label="breadcrumb">
                        <Typography color="textPrimary">Blocks</Typography>
                    </Breadcrumbs>
                </div>

                <AdminNavLinks />

            </div>
        };

        return (
            <div>
                <ConfirmDeleteModal
                    deleteModalIsOpen={deleteModalIsOpen}
                    handleOpenCloseModal={this.handleOpenCloseModal}
                    cancelDeletion={this.cancelDeletion}
                    deleteChosenItem={this.deleteChosenItem}
                >
                </ConfirmDeleteModal>

                { renderBreadcrumbs() }

                <div className={classes.adFilterContainer}>
                    <div className={classes.adNavItem}>Order by:</div>

                    <div className={classes.adNavItem}>name</div>
                    {
                        isAscBlockNameOrder ?
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setNameOrder}
                            >
                                <ArrowDownward/>
                            </div> :
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setNameOrder}
                            >
                                <ArrowUpward/>
                            </div>
                    }

                    <div className={classes.adNavItem}>id</div>
                    {
                        isAscBlockIdOrder ?
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setIdOrder}
                            >
                                <ArrowDownward/>
                            </div> :
                            <div
                                className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                onClick={this.setIdOrder}
                            >
                                <ArrowUpward/>
                            </div>
                    }
                </div>

                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell className={classes.adTableHeader}>Block Name</TableCell>
                                <TableCell align="center" className={classes.adTableHeader}>Order number</TableCell>
                                <TableCell align="right" className={classes.adTableHeader}>Delete</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        { blocks && blocks.map(block => {
                            const { name, id, _id } = block;
                            return (
                                <TableRow
                                    key={_id}
                                    className={classes.adTableRow}
                                >
                                    <TableCell
                                        component="th"
                                        scope="row"
                                        onClick={() => this.openSingleBlock(block)}
                                    >
                                        {name}
                                    </TableCell>
                                    <TableCell
                                        align="center"
                                        className={classes.adTableOrderNumber}
                                        onClick={() => this.openSingleBlock(block)}
                                    >
                                        {id}
                                    </TableCell>
                                    <TableCell
                                        align="right"
                                        className={classes.adDeleteButton}
                                        onClick={() => this.handleOpenCloseModal(block)}
                                    >
                                        <IconButton
                                            aria-label="delete"
                                            color="secondary"
                                        >
                                            <DeleteIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                        </TableBody>
                    </Table>
                </TableContainer>

                { renderAddBlocks() }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    blocks: state.blockData.blocks,
    isAscBlockNameOrder: state.blockData.isAscBlockNameOrder,
    isAscBlockIdOrder: state.blockData.isAscBlockIdOrder,
    steps: state.stepData.steps,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchBlocks: () => dispatch(fetchBlocks()),
        fetchSteps: () => dispatch(fetchSteps()),
        setBlockToEdit: (block) => dispatch(setBlockToEdit(block)),
        setStepsToEdit: (steps) => dispatch(setStepsToEdit(steps)),
        putBlock: (id, name, summary) => dispatch(putBlock(id, name, summary)),
        deleteBlock: (objId) => dispatch(deleteBlock(objId)),
        orderBlocksNameAsc:() => dispatch(orderBlocksNameAsc()),
        orderBlocksNameDesc:() => dispatch(orderBlocksNameDesc()),
        orderBlocksIdAsc:() => dispatch(orderBlocksIdAsc()),
        orderBlocksIdDesc:() => dispatch(orderBlocksIdDesc()),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(BlocksList)))
