import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { updateQuestion, fetchQuestions }  from "../../actions/questionsActions";
import styles from '../../styles.js';
import Breadcrumbs from "@material-ui/core/Breadcrumbs/Breadcrumbs";
import Typography from "@material-ui/core/Typography/Typography";
import AdminNavLinks from "../../components/AdminNavLinks";
import Link from "@material-ui/core/Link/Link";
import IconButton from "@material-ui/core/IconButton/IconButton";
import DoneIcon from '@material-ui/icons/Done';
import EditIcon from "@material-ui/icons/Edit";
import TextField from "@material-ui/core/TextField/TextField";

class SingleQuestion extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
            isEdited: false,
            newText: '',
            newId: 0,
            prevText: '',
            prevId: 0,
            isAlertOpen: false,
        };

        this.handleNavClick = this.handleNavClick.bind(this);
        this.changeText = this.changeText.bind(this);
        this.changeId = this.changeId.bind(this);
        this.editQuestion = this.editQuestion.bind(this);
        this.saveQuestion = this.saveQuestion.bind(this);
    }

    async componentDidMount() {
        await this.props.fetchSteps();
    }

    handleNavClick(event, link) {
        event.preventDefault();
        this.props.history.push(link);
    }

    changeText(event) {
        event.preventDefault();
        this.setState({ newText: event.target.value });
    }

    changeId(event) {
        event.preventDefault();
        this.setState({ newId: event.target.value });
    }


    editQuestion() {
        const { isEdited } = this.state;

        !isEdited && this.setState({ isEdited: !isEdited });
    }

    async saveQuestion() {
        const { currentQuestion } = this.props;
        const { isEdited, newText, newId, prevText, prevId } = this.state;

        if (newText !== prevText || newId !== prevId){
            await this.props.updateQuestion(currentQuestion._id, { text: newText, id: newId });
            this.setState({ prevText: newText, prevId: newId });
        }

        isEdited && this.setState({ isEdited: !isEdited });

    }

    async componentDidMount() {
        await this.props.fetchQuestions();

        const { currentQuestion } = this.props;
        const { id, text } = currentQuestion;
        this.setState({
            newText: text,
            newId: id,
            prevText: text,
            prevId: id,
        });
    }

    render() {

        const { classes } = this.props;
        const {
            isEdited,
            newText,
            newId,
        } = this.state;

        const renderBreadcrumbs = () => {
            return <div className={classes.adNav}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/tests" onClick={event => this.handleNavClick(event, '/tests')}>
                        Tests
                    </Link>
                    <Link color="inherit" href="/test" onClick={event => this.handleNavClick(event, '/test')}>
                        Test
                    </Link>
                    <Typography color="textPrimary">Question</Typography>
                </Breadcrumbs>

                <AdminNavLinks />

            </div>
        };

        return (
            <div className={classes.adminContent}>

                { renderBreadcrumbs() }

                <div className={classes.adHeaderName}>Name:</div>
                {
                    !isEdited ?
                        <IconButton
                            className={classes.editButton}
                            aria-label="edit"
                            color="primary"
                            onClick={this.editQuestion}
                        >
                            <EditIcon />
                        </IconButton> :
                        <IconButton
                            className={classes.editButton}
                            aria-label="edit"
                            color="primary"
                            onClick={this.saveQuestion}
                        >
                            <DoneIcon />
                        </IconButton>
                }

                {
                    !isEdited ?
                        <div className={classes.adTextfieldName}>{ newText }</div> :
                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            rows="1"
                            defaultValue={newText}
                            variant="outlined"
                            onChange={event => this.changeText(event)}
                        />
                }

                <div className={classes.adHeader}>Order number:</div>
                {
                    !isEdited ?
                        <div className={classes.adTextfield}>{ newId ? newId : 'No id' }</div> :
                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            rows="1"
                            defaultValue={newId}
                            variant="outlined"
                            onChange={event => this.changeId(event)}
                        />
                }

            </div>
        );
    }
}

const mapStateToProps = state => ({
    currentQuestion: state.questionsData.currentQuestion,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchQuestions: () => dispatch(fetchQuestions()),
        updateQuestion: (_id, { text, id } ) => dispatch(updateQuestion(_id, { text, id })),
        // putQuestion: (id, text, testId) => dispatch(putQuestion(id, text, testId)),
        // deleteQuestion: (id, testId) => dispatch(deleteQuestion(id, testId)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(SingleQuestion)))
