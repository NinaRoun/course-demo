import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import BlocksList from './BlocksList.js';
import styles from '../../styles.js';
import CollapseButton from "../../components/CollapseButton";
import Button from '@material-ui/core/Button';
import { getCurrentStep, getCurrentBlock, pushLessonToFinished, pushBlockToFinished, setCurrentStep, setCurrentBlock } from "../../actions/userActions";

const minPageSize = 700;

class AdminPage extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
        };

    }

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {

        const { classes } = this.props;
        const { pageIsLarge } = this.state;

        return (
            <div className={classes.adminContent}>

                <BlocksList />

            </div>
        );
    }
}

const mapStateToProps = state => ({
    blocks: state.blockData.blocks,
    steps: state.stepData.steps,
    isLogged: state.validation.isLogged,
    error: state.validation.error,
    isLoading: state.validation.isLoading,
    userId: state.validation.userProfile.userId,
    currentStep: state.validation.userProfile.currentStep,
    currentBlock: state.validation.userProfile.currentBlock,
    chosenSteps: state.stepData.chosenSteps,
});

const mapDispatchToProps = dispatch => {
    return {
        //pushLessonToFinished: (id, userId) => dispatch(pushLessonToFinished(id, userId)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(AdminPage)))
