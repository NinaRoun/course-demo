import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { updateStep } from "../../actions/adminActions";
import { fetchSteps } from "../../actions/stepsActions";
import DoneIcon from '@material-ui/icons/Done';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from "@material-ui/core/IconButton/IconButton";
import TextField from '@material-ui/core/TextField';
import styles from '../../styles.js';
import Breadcrumbs from "@material-ui/core/Breadcrumbs/Breadcrumbs";
import Link from "@material-ui/core/Link/Link";
import Typography from "@material-ui/core/Typography/Typography";
import AdminNavLinks from "../../components/AdminNavLinks";

const minPageSize = 700;

class StepsList extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
            isEdited: false,
            newName: '',
            newId: 0,
            newSummary: '',
            prevName: '',
            prevId: 0,
            prevSummary: '',
        };

        this.changeName = this.changeName.bind(this);
        this.changeId = this.changeId.bind(this);
        this.changeSummary = this.changeSummary.bind(this);
        this.editStep = this.editStep.bind(this);
        this.saveStep = this.saveStep.bind(this);
        this.handleNavClick = this.handleNavClick.bind(this);
    }

    async componentDidMount() {
        await this.props.fetchSteps();
    }

    handleNavClick(event, link) {
        event.preventDefault();
        this.props.history.push(link);
    }

    changeName(event) {
        event.preventDefault();
        this.setState({ newName: event.target.value });
    }

    changeId(event) {
        event.preventDefault();
        this.setState({ newId: event.target.value });
    }

    changeSummary(event) {
        event.preventDefault();
        this.setState({ newSummary: event.target.value });
    }

    editStep() {
        const { isEdited } = this.state;

        !isEdited && this.setState({ isEdited: !isEdited });
    }

    async saveStep() {
        const { stepToEdit } = this.props;
        const { isEdited, newName, newId, newSummary, prevName, prevId, prevSummary } = this.state;

        if (newName !== prevName || newSummary !== prevSummary || newId !== prevId){
            this.setState({ prevName: newName, prevSummary: newSummary, prevId: newId });
            await this.props.updateStep(stepToEdit._id, { name: newName, id: newId, summary: newSummary });
        }

        isEdited && this.setState({ isEdited: !isEdited });

    }

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));

        const { stepToEdit } = this.props;
        const { id, name, summary } = stepToEdit;
        this.setState({
            newName: name,
            newId: id,
            newSummary: summary,
            prevName: name,
            prevId: id,
            prevSummary: summary,
        });
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {

        const { classes } = this.props;
        const { isEdited, newName, newId, newSummary } = this.state;

        const renderBreadcrumbs = () => {
            return <div className={classes.adNav}>

                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/admin" onClick={event => this.handleNavClick(event, '/admin')}>
                        Blocks
                    </Link>
                    <Link color="inherit" href="/block" onClick={event => this.handleNavClick(event, '/block')}>
                        Edit block
                    </Link>
                    <Typography color="textPrimary">Edit step</Typography>
                </Breadcrumbs>

                <AdminNavLinks />

            </div>
        };

        return (
            <div className={classes.adminContent}>

                { renderBreadcrumbs() }

                <div className={classes.adHeaderName}>Name:</div>
                {
                    !isEdited ?
                    <IconButton
                        className={classes.editButton}
                        aria-label="edit"
                        color="primary"
                        onClick={this.editStep}
                    >
                        <EditIcon />
                    </IconButton> :
                    <IconButton
                        className={classes.editButton}
                        aria-label="edit"
                        color="primary"
                        onClick={this.saveStep}
                    >
                         <DoneIcon />
                    </IconButton>
                }

                {
                    !isEdited ?
                <div className={classes.adTextfieldName}>{ newName }</div> :
                     <TextField
                         id="outlined-multiline-static"
                         multiline
                         rows="1"
                         defaultValue={newName}
                         variant="outlined"
                         onChange={event => this.changeName(event)}
                     />
                }

                <div className={classes.adHeader}>Order number:</div>
                {
                    !isEdited ?
                        <div className={classes.adTextfield}>{ newId ? newId : 'No id' }</div> :
                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            rows="1"
                            defaultValue={newId}
                            variant="outlined"
                            onChange={event => this.changeId(event)}
                        />
                }

                <div className={classes.adHeader}>Summary:</div>
                {
                    !isEdited ?
                <div className={classes.adTextfield}>{ newSummary || 'No summary' }</div> :
                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        rows="8"
                        defaultValue={newSummary}
                        variant="outlined"
                        onChange={event => this.changeSummary(event)}
                    />
                }

            </div>
        );
    }
}

const mapStateToProps = state => ({
    steps: state.stepData.steps,
    stepToEdit: state.admin.stepToEdit,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchSteps: () => dispatch(fetchSteps()),
        updateStep: (_id, { name, id, summary } ) => dispatch(updateStep(_id, { name, id, summary })),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(StepsList)))
