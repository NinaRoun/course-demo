import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import { fetchBlocks } from "../../actions/blocksActions";
import { updateBlock } from "../../actions/adminActions";
import { fetchSteps } from "../../actions/stepsActions";
import {
    setStepToEdit,
    deleteStep,
    putStep,
    orderStepsNameAsc,
    orderStepsNameDesc,
    orderStepsIdAsc,
    orderStepsIdDesc,
} from "../../actions/adminActions";
import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from '@material-ui/icons/Done';
import EditIcon from '@material-ui/icons/Edit';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Paper from '@material-ui/core/Paper';
import IconButton from "@material-ui/core/IconButton/IconButton";
import TextField from '@material-ui/core/TextField';
import styles from '../../styles.js';
import Button from '@material-ui/core/Button';
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import TableContainer from "@material-ui/core/TableContainer/TableContainer";
import Breadcrumbs from "@material-ui/core/Breadcrumbs/Breadcrumbs";
import Link from "@material-ui/core/Link/Link";
import Typography from "@material-ui/core/Typography/Typography";
import AdminNavLinks from '../../components/AdminNavLinks.js';
import ConfirmDeleteModal from "../../components/ConfirmDeleteModal";

const minPageSize = 700;

class BlocksList extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
            isEdited: false,
            isNewStepAdded: false,
            newName: '',
            newId: 0,
            newSummary: '',
            addNewName: '',
            addNewId: '',
            addNewSummary: '',
            prevName: '',
            prevId: 0,
            prevSummary: '',
            deleteModalIsOpen: false,
            stepToDelete: '',
            isAlertOpen: false,
            notNumberAlertOpen: false,
        };

        this.changeName = this.changeName.bind(this);
        this.changeSummary = this.changeSummary.bind(this);
        this.changeNewName = this.changeNewName.bind(this);
        this.changeNewId = this.changeNewId.bind(this);
        this.changeNewSummary = this.changeNewSummary.bind(this);
        this.editBlock = this.editBlock.bind(this);
        this.editStep = this.editStep.bind(this);
        this.addStep = this.addStep.bind(this);
        this.saveBlock = this.saveBlock.bind(this);
        this.saveStep = this.saveStep.bind(this);
        this.deleteChosenItem = this.deleteChosenItem.bind(this);
        this.handleOpenCloseModal = this.handleOpenCloseModal.bind(this);
        this.cancelDeletion = this.cancelDeletion.bind(this);
        this.cancelAdding = this.cancelAdding.bind(this);
        this.handleNavClick = this.handleNavClick.bind(this);
        this.setIdOrder = this.setIdOrder.bind(this);
        this.setNameOrder = this.setNameOrder.bind(this);
    }

    async componentDidMount() {
        await this.props.fetchBlocks();
        await this.props.fetchSteps();
    }

    async setNameOrder() {
        const { isAscStepNameOrder } = this.props;

        isAscStepNameOrder ? await this.props.orderStepsNameDesc() : await this.props.orderStepsNameAsc();
    }

    async setIdOrder() {
        const { isAscStepIdOrder } = this.props;

        isAscStepIdOrder ? await this.props.orderStepsIdDesc() : await this.props.orderStepsIdAsc();
    }

    handleNavClick(event, link) {
        event.preventDefault();
        this.props.history.push(link);
    }

    changeName(event) {
        event.preventDefault();
        this.setState({ newName: event.target.value });
    }

    changeId(event) {
        event.preventDefault();
        this.setState({ newId: event.target.value });
    }

    changeSummary(event) {
        event.preventDefault();
        this.setState({ newSummary: event.target.value });
    }

    changeNewName(event) {
        event.preventDefault();
        this.setState({ addNewName: event.target.value });
    }

    changeNewId(event) {
        event.preventDefault();
        this.setState({ addNewId: event.target.value });
    }

    changeNewSummary(event) {
        event.preventDefault();
        this.setState({ addNewSummary: event.target.value });
    }

    editBlock() {
        const { isEdited } = this.state;

        !isEdited && this.setState({ isEdited: !isEdited });
    }

    addStep() {
        const { isNewStepAdded } = this.state;

        !isNewStepAdded && this.setState({ isNewStepAdded: !isNewStepAdded });
    }

    async deleteChosenItem() {
        const { stepToDelete } = this.state;
        const { blockToEdit } = this.props;

        await this.props.deleteStep(stepToDelete, blockToEdit._id);
        this.setState({ deleteModalIsOpen: false, stepToDelete: '' });
    }

    handleOpenCloseModal(step) {
        const { deleteModalIsOpen } = this.state;

        if (step._id) this.setState({ stepToDelete: step._id });
        this.setState({ deleteModalIsOpen: !deleteModalIsOpen });
    }

    cancelDeletion() {
        this.setState({ deleteModalIsOpen: false, stepToDelete: '' });
    }

    cancelAdding() {
        this.setState({
            isNewStepAdded: false,
            isAlertOpen: false,
            notNumberAlertOpen: false,
        });
    }

    async editStep(step) {
        await this.props.setStepToEdit(step);
        this.props.history.push('/step');
    }

    async saveBlock() {
        const { blockToEdit } = this.props;
        const { isEdited, newName, newId, newSummary, prevName, prevId, prevSummary } = this.state;

        if (newName !== prevName || newSummary !== prevSummary || newId !== prevId){
            this.setState({ prevName: newName, prevId: newId, prevSummary: newSummary });
            await this.props.updateBlock(blockToEdit._id, { name: newName, id: newId, summary: newSummary });
        }

        isEdited && this.setState({ isEdited: !isEdited });

    }

    async saveStep() {
        const { blockToEdit } = this.props;
        const { isNewStepAdded, addNewName, addNewId, addNewSummary } = this.state;

        if(isNaN(Number(addNewId))) this.setState({ notNumberAlertOpen: true });
        else if (addNewName && addNewId){
            await this.props.putStep(addNewId, addNewName, addNewSummary, blockToEdit._id);
            this.setState({
                addNewName: '',
                addNewId: '',
                addNewSummary: '',
                isAlertOpen: false,
                isNewStepAdded: !isNewStepAdded,
                notNumberAlertOpen: false,
            });
        } else {
            this.setState({ isAlertOpen: true });
        }
    }


    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));

        const { blockToEdit } = this.props;
        const { name, id, summary } = blockToEdit;
        this.setState({
            newName: name,
            newId: id,
            newSummary: summary,
            prevName: name,
            prevId: id,
            prevSummary: summary
        });
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {

        const { classes, stepsToEdit, isAscStepNameOrder, isAscStepIdOrder } = this.props;
        const { isEdited, newName, newId, newSummary, deleteModalIsOpen } = this.state;

        const renderSteps = () => {

            return (
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell className={classes.adTableHeader}>Step Name</TableCell>
                                <TableCell align="center" className={classes.adTableHeader}>Order number</TableCell>
                                <TableCell align="right" className={classes.adTableHeader}>Delete</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {stepsToEdit ? stepsToEdit.map(step => {
                                const { name, id } = step;
                                return (
                                    <TableRow
                                        key={name}
                                        className={classes.adTableRow}
                                    >
                                        <TableCell
                                            component="th"
                                            scope="row"
                                            onClick={() => this.editStep(step)}
                                        >
                                            {name}
                                        </TableCell>
                                        <TableCell
                                            align="center"
                                            className={classes.adTableOrderNumber}
                                            onClick={() => this.editStep(step)}
                                        >
                                            {id}
                                        </TableCell>
                                        <TableCell
                                            align="right"
                                            className={classes.adDeleteButton}
                                            onClick={() => this.handleOpenCloseModal(step)}
                                        >
                                            <IconButton
                                                aria-label="delete"
                                                color="secondary"
                                            >
                                                <DeleteIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                )
                            }) : null }
                        </TableBody>
                    </Table>
                </TableContainer>
            );
        };

        const renderAddSteps = () => {
            const { isNewStepAdded, isAlertOpen, notNumberAlertOpen } = this.state;

            return(
                isNewStepAdded ?
                    <div className={classes.adCreateItemContainer}>
                        <h4>Creating a new step</h4>

                        <div className={isAlertOpen ? classes.warningIsOpen : classes.warningIsClosed}>
                            Name and id should be filled
                        </div>

                        <div className={notNumberAlertOpen ? classes.warningIsOpen : classes.warningIsClosed}>
                            Id should be a plain number
                        </div>

                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            className={classes.newTextareaName}
                            rows="1"
                            defaultValue='Name'
                            variant="outlined"
                            onChange={event => this.changeNewName(event)}
                        />
                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            className={classes.newTextarea}
                            rows="1"
                            defaultValue='Id'
                            variant="outlined"
                            onChange={event => this.changeNewId(event)}
                        />
                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            className={classes.newTextarea}
                            rows="8"
                            defaultValue='Summary'
                            variant="outlined"
                            onChange={event => this.changeNewSummary(event)}
                        />
                        <div className={classes.adAddItemContainer}>
                            <div className={classes.adModalButtonContainer}>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    onClick={this.cancelAdding}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    onClick={this.saveStep}
                                    variant="contained"
                                    color="primary"
                                >
                                    Save new step
                                </Button>
                            </div>
                        </div>
                    </div> :
                    <div className={classes.adAddItemContainer}>
                        <Button
                            onClick={this.addStep}
                            variant="contained"
                        >
                            Add a step
                        </Button>
                    </div>
            )
        };

        const renderBreadcrumbs = () => {
            return <div className={classes.adNav}>

                <Breadcrumbs aria-label="breadcrumb">
                    <Link color="inherit" href="/admin" onClick={event => this.handleNavClick(event, '/admin')}>
                        Blocks
                    </Link>
                    <Typography color="textPrimary">Edit block</Typography>
                </Breadcrumbs>

                <AdminNavLinks />

            </div>
        };

        return (
            <div className={classes.adminContent}>

                <ConfirmDeleteModal
                    deleteModalIsOpen={deleteModalIsOpen}
                    handleOpenCloseModal={this.handleOpenCloseModal}
                    cancelDeletion={this.cancelDeletion}
                    deleteChosenItem={this.deleteChosenItem}
                >
                </ConfirmDeleteModal>

                { renderBreadcrumbs() }

                <div className={classes.adHeaderName}>Name:</div>
                {
                    !isEdited ?
                    <IconButton
                        className={classes.editButton}
                        aria-label="edit"
                        color="primary"
                        onClick={this.editBlock}
                    >
                        <EditIcon />
                    </IconButton> :
                    <IconButton
                        className={classes.editButton}
                        aria-label="edit"
                        color="primary"
                        onClick={this.saveBlock}
                    >
                         <DoneIcon />
                    </IconButton>
                }
                {
                    !isEdited ?
                <div className={classes.adTextfieldName}>{ newName }</div> :
                     <TextField
                         id="outlined-multiline-static"
                         multiline
                         rows="1"
                         defaultValue={newName}
                         variant="outlined"
                         onChange={event => this.changeName(event)}
                     />
                }

                <div className={classes.adHeader}>Order number:</div>
                {
                    !isEdited ?
                        <div className={classes.adTextfield}>{ newId ? newId : 'No id' }</div> :
                        <TextField
                            id="outlined-multiline-static"
                            multiline
                            rows="1"
                            defaultValue={newId}
                            variant="outlined"
                            onChange={event => this.changeId(event)}
                        />
                }

                <div className={classes.adHeader}>Summary:</div>
                {
                    !isEdited ?
                <div className={classes.adTextfield}>{ newSummary || 'No summary' }</div> :
                    <TextField
                        id="outlined-multiline-static"
                        multiline
                        rows="8"
                        defaultValue={newSummary}
                        variant="outlined"
                        onChange={event => this.changeSummary(event)}
                    />
                }

                {
                    stepsToEdit.length ?
                        <div className={classes.adFilterContainer}>
                            <div className={classes.adNavItem}>Order by:</div>

                            <div className={classes.adNavItem}>name</div>
                            {
                                isAscStepNameOrder ?
                                    <div
                                        className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                        onClick={this.setNameOrder}
                                    >
                                        <ArrowDownward/>
                                    </div> :
                                    <div
                                        className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                        onClick={this.setNameOrder}
                                    >
                                        <ArrowUpward/>
                                    </div>
                            }

                            <div className={classes.adNavItem}>id</div>
                            {
                                isAscStepIdOrder ?
                                    <div
                                        className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                        onClick={this.setIdOrder}
                                    >
                                        <ArrowDownward/>
                                    </div> :
                                    <div
                                        className={[classes.adNavItem, classes.adNavLink].join(' ')}
                                        onClick={this.setIdOrder}
                                    >
                                        <ArrowUpward/>
                                    </div>
                            }
                        </div> :
                        <div className={classes.adHeader}>No steps are added</div>
                }

                { renderSteps() }

                { !isEdited && renderAddSteps() }

            </div>
        );
    }
}

const mapStateToProps = state => ({
    blocks: state.blockData.blocks,
    steps: state.stepData.steps,
    blockToEdit: state.admin.blockToEdit,
    stepsToEdit: state.admin.stepsToEdit,
    isAscStepNameOrder: state.admin.isAscStepNameOrder,
    isAscStepIdOrder: state.admin.isAscStepIdOrder,
});

const mapDispatchToProps = dispatch => {
    return {
        fetchBlocks: () => dispatch(fetchBlocks()),
        fetchSteps: () => dispatch(fetchSteps()),
        updateBlock: (_id, { name, id, summary} ) => dispatch(updateBlock(_id, { name, id, summary})),
        setStepToEdit: (step) => dispatch(setStepToEdit(step)),
        deleteStep: (objId, currentBlockId) => dispatch(deleteStep(objId, currentBlockId)),
        putStep: (id, name, summary, currentBlockId) => dispatch(putStep(id, name, summary, currentBlockId)),
        orderStepsNameAsc:() => dispatch(orderStepsNameAsc()),
        orderStepsNameDesc:() => dispatch(orderStepsNameDesc()),
        orderStepsIdAsc:() => dispatch(orderStepsIdAsc()),
        orderStepsIdDesc:() => dispatch(orderStepsIdDesc()),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(BlocksList)))
