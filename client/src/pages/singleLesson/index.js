import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import ReactPlayer from 'react-player'
import connect from "react-redux/es/connect/connect";
import UserCard from '../../components/UserCard.js';
import styles from '../../styles.js';
import CollapseButton from "../../components/CollapseButton";
import Button from '@material-ui/core/Button';
import { getCurrentStep, getCurrentBlock, pushLessonToFinished, pushBlockToFinished, setCurrentStep, setCurrentBlock } from "../../actions/userActions";

const minPageSize = 700;

class LearningPage extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            pageIsLarge: null,
        };

        this.finishCurrentStep = this.finishCurrentStep.bind(this);
    }

    updateDimensions() {
        const w = window;
        const d = document;
        const documentElement = d.documentElement;
        const body = d.getElementsByTagName('body')[0];
        const width = w.innerWidth || documentElement.clientWidth || body.clientWidth;
        const largePage = width >= minPageSize;
        this.setState({ pageIsLarge: largePage });
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        // should retrieve content of currentStep, track that a video is watched to the end to set next step as current
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    async finishCurrentStep() {
        const { currentStep, currentBlock, chosenSteps, userId, blocks, steps } = this.props;
        await this.props.getCurrentBlock(userId);
        await this.props.pushLessonToFinished(currentStep._id, userId);

        // console.log('333333 currentBlock ', currentBlock);
        // console.log('333333 currentStep ', currentStep);

        let nextStep;

        chosenSteps.forEach((step, index) => {
            if (step._id === currentStep._id) nextStep = chosenSteps[index + 1];
        });

        if (nextStep !== undefined){
            //console.log('next step ', nextStep.name);
            await this.props.setCurrentStep(nextStep, userId)
        }
        else {
            //console.log('block is finished ', currentBlock.name);
            await this.props.pushBlockToFinished(currentBlock._id, userId);
            //find next block
            let nextBlock, nextStep;

            blocks.forEach((block, index) => {
                if (block._id === currentBlock._id) nextBlock = blocks[index + 1];
            });

            //console.log('33333 nextBlock ', nextBlock.name);
            if (nextBlock) {
                //console.log('333333 next block exists');
                await this.props.setCurrentBlock(nextBlock, userId);
                nextStep = steps.find(step => step._id === nextBlock.steps[0]);
                //console.log('333333 nextStep ', nextStep.name);
                nextStep && await this.props.setCurrentStep(nextStep, userId);
            }
        }
    }

    render() {

        const { classes } = this.props;
        const { pageIsLarge } = this.state;

        return (
            <div className={classes.pageContent}>

                { !pageIsLarge && <CollapseButton></CollapseButton> }

                <div className={classes.profileContent}>

                    { pageIsLarge && <div className={classes.userCardGrid}><UserCard /></div> }

                    <div className={classes.contentGrid}>
                        <h2 className={classes.pageTitle}>Lesson</h2>
                        <div className={classes.pageText}>
                            <ReactPlayer url='https://www.youtube.com/watch?v=ysz5S6PUM-U' controls />
                            <p>CSS-in-JS avoids these problems entirely by generating unique class names when styles are converted to CSS. This allows you to think about styles on a component level with out worrying about styles defined elsewhere.</p>
                            <p>In this course, you will learn how to express popular SCSS (Sass) language features using latest JavaScript features. We will convert simple examples from SCSS to CSS-in-JS. As a designer or (S)CSS developer, you should be able to follow without extensive JavaScript knowledge, understanding SCSS is required though. We will not be using any particular CSSinJS libraries. Instead, we will focus on a base knowledge you need later if you use any CSSinJS library.</p>
                            <Button
                                size="medium"
                                onClick={this.finishCurrentStep}
                            >
                                Mark as done
                            </Button>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    blocks: state.blockData.blocks,
    steps: state.stepData.steps,
    isLogged: state.validation.isLogged,
    error: state.validation.error,
    isLoading: state.validation.isLoading,
    userId: state.validation.userProfile.userId,
    currentStep: state.validation.userProfile.currentStep,
    currentBlock: state.validation.userProfile.currentBlock,
    chosenSteps: state.stepData.chosenSteps,
});

const mapDispatchToProps = dispatch => {
    return {
        pushLessonToFinished: (id, userId) => dispatch(pushLessonToFinished(id, userId)),
        pushBlockToFinished: (id, userId) => dispatch(pushBlockToFinished(id, userId)),
        setCurrentStep: (step, userId) => dispatch(setCurrentStep(step, userId)),
        setCurrentBlock: (block, userId) => dispatch(setCurrentBlock(block, userId)),
        getCurrentStep: (userId) => dispatch(setCurrentBlock(userId)),
        getCurrentBlock: (userId) => dispatch(getCurrentBlock(userId)),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(LearningPage)))
