import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { NAVBAR_OPTIONS } from '../constants.js';
import { logout } from '../actions/userActions.js';
import styles from '../styles.js';

class UserCard extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            anchorEl: null,
            mobileOpen: false,
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    async handleClick(path) {
        if (path === '/') {
            await this.props.logout();
        }
        this.props.history.push(path);
    };

    handleClose() {
        this.setState({ anchorEl: null })
    };

    render() {

        const { classes, userName, history, currentCourses, currentBlocks } = this.props;

        const avatarText = userName ? userName.toUpperCase()[0] + userName.toUpperCase().substring(userName.indexOf(' '))[1] : 'NA';

        return (
            <div className={classes.userCardContainer}>

                <Card className={classes.userCard} variant="outlined">

                    <CardContent className={classes.userCardHeader}>

                        <Avatar className={classes.userAvatar}>{ avatarText }</Avatar>
                        <h3 className={classes.userCardName}>{ userName }</h3>

                    </CardContent>

                    <CardActions className={classes.userCardActions}>

                        <List component="nav" className={classes.userCardNavbar}>

                            <Divider />
                            {NAVBAR_OPTIONS.map(option => {
                                const { name, icon, path } = option;

                                return <ListItem button key={name}>
                                    <ListItemIcon>{icon}</ListItemIcon>
                                    <ListItemText primary={name}
                                                  className={classes.userCardNavbarItem}
                                                  onClick={() => this.handleClick(path)}
                                    />
                                    <Divider />
                                </ListItem>
                                }
                            )}

                            <Divider />

                        </List>
                    </CardActions>
                </Card>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    //blocks: state.blockData.blocks,
    isLogged: state.validation.isLogged,
    error: state.validation.error,
    isLoading: state.validation.isLoading,
    userName: state.validation.userProfile.userName,
    //currentCourse: state.userProfile.currentCourse,
    currentBlocks: state.validation.userProfile.currentBlocks,
});

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(logout()),
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(injectStyles(styles)(UserCard)))
