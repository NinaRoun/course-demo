import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import 'react-dropdown/style.css';
import styles from '../styles.js';

class CollapseDropdown extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            dropdownIsOpen: false,
        };

        this.selectOption = this.selectOption.bind(this);
    }

    selectOption() {
        const {
            multiedit,
            data,
        } = this.props;

        this.setState(
            { dropdownIsOpen: false },
            () => {
                this.props.cb(value, multiedit, data);
                this.setState({ dropdownIsOpen: false });
            }
        );

        // switch (page) {
        //     case 'Instructions':
        //         this.props.history.push('/instructions');
        //         break;
        //     case 'Learning':
        //         this.props.history.push('/learning');
        //         break;
        // }
    }

    render() {



        return (

            <DropdownBase
                {...this.props}
                closeDropdownBase={closeDropdownBase}
                dropdownHeight={dropdownHeight}
                customStyles={customStyles}
            >
                {optionsToRender}
            </DropdownBase>

        );
    }
}

const mapStateToProps = state => ({
});

// const mapDispatchToProps = dispatch => {
//     return {
//     }
// };

export default withRouter(connect(
    mapStateToProps,
    // mapDispatchToProps
)(injectStyles(styles)(CollapseDropdown)))
