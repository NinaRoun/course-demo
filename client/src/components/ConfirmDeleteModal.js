import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import 'react-dropdown/style.css';
import styles from '../styles.js';
import Fade from "@material-ui/core/Fade/Fade";
import Button from "@material-ui/core/Button/Button";
import Modal from "@material-ui/core/Modal/Modal";

class ConfirmDeleteModal extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
        };

    }

    render() {

        const { classes, deleteModalIsOpen } = this.props;

        return (

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={deleteModalIsOpen}
                onClose={this.handleOpenCloseModal}
                closeAfterTransition
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={deleteModalIsOpen}>
                    <div className={classes.adModalFade}>
                        <h3 id="transition-modal-title">Are you sure you want to delete this step?</h3>
                        <div className={classes.adModalButtonContainer}>
                            <Button
                                variant="contained"
                                color="secondary"
                                onClick={this.props.cancelDeletion}
                            >
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={this.props.deleteChosenItem}
                            >
                                Confirm
                            </Button>
                        </div>
                    </div>
                </Fade>
            </Modal>

        );
    }
}

const mapStateToProps = state => ({
});

export default withRouter(connect(
    mapStateToProps,
)(injectStyles(styles)(ConfirmDeleteModal)))
