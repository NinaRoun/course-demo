import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import CollapseDropdown from './CollapseDropdown';
import 'react-dropdown/style.css';
import styles from '../styles.js';

class CollapseButton extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            dropdownIsOpen: false,
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const { dropdownIsOpen } = this.state;
        this.setState({ dropdownIsOpen: !dropdownIsOpen });
    };

    render() {

        const { classes } = this.props;
        const { dropdownIsOpen } = this.state;

        return (

            <div>
                <Toolbar className={classes.collapseButtonContainer}>
                    <IconButton
                        className={classes.collapseButton}
                        onClick={this.handleClick}
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography className={classes.collapseButtonHeader} variant="h5">
                        Menu
                    </Typography>

                </Toolbar>

                { dropdownIsOpen && <CollapseDropdown></CollapseDropdown> }

            </div>

        );
    }
}

const mapStateToProps = state => ({
});

export default withRouter(connect(
    mapStateToProps,
)(injectStyles(styles)(CollapseButton)))
