import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { NAVBAR_OPTIONS } from '../constants.js';
import 'react-dropdown/style.css';
import styles from '../styles.js';

class CollapseDropdown extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            dropdownIsOpen: false,
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(path) {
        this.props.history.push(path);
    }

    render() {

        const { classes } = this.props;

        return (

            <div className={classes.collapseDropdown}>

                <Divider />

                {NAVBAR_OPTIONS.map(option => {
                        const { name, icon, path } = option;

                        return <ListItem className={classes.collapseNavbarItem}
                                         button
                                         key={name}
                                         onClick={() => this.handleClick(path)}
                        >
                            <div className={classes.collapseNabarItemContent}>
                                <ListItemIcon>{icon}</ListItemIcon>
                                <ListItemText
                                    className={classes.collapseNavbarItemText}
                                    primary={name}
                                />
                            </div>
                        </ListItem>
                    }
                )}
            </div>

        );
    }
}

const mapStateToProps = state => ({
});

// const mapDispatchToProps = dispatch => {
//     return {
//     }
// };

export default withRouter(connect(
    mapStateToProps,
    // mapDispatchToProps
)(injectStyles(styles)(CollapseDropdown)))
