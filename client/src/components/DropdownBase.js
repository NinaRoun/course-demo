import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import 'react-dropdown/style.css';
import styles from '../styles.js';

class DropdownBase extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            open: false,
            containerCoords: {},
            optionsCoords: {},
            arrowCoords: {},
            renderToTop: false,
            horizontalDirection: '',
        };

        this.keyCodes = { ESC: 27 };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.listener = this.listener.bind(this);
        this.basicKeyListener = this.basicKeyListener.bind(this);
        this.calculateAbsoluteCoords = this.calculateAbsoluteCoords.bind(this);
    }

    componentDidUpdate (prevProps) {
        const { closeDropdownBase } = this.props;

        if (
            closeDropdownBase !== prevProps.closeDropdownBase && closeDropdownBase
        ) {
            this.close();
        }
    }

    open (event) {
        // works only for dropdownTextarea, since we have working links in btn there
        if (event.target.tagName === 'A') {
            return;
        }

        const { base } = this;
        const {
            data,
            onOpenCb,
            readCb,
            blocked,
        } = this.props;

        if (blocked) {
            return;
        }

        const coords = this.calculateAbsoluteCoords(base);

        this.setState({
            open: true,
            ...coords,
        }, function () {
            document.addEventListener('click', this.listener);
            // subscribe all dropdowns to basicKeyListener actions,
            // such as pressing ESC
            document.addEventListener('keydown', this.basicKeyListener);

            onOpenCb && onOpenCb(this.state);
            lodashIsFunction(readCb) && readCb(data.id);
        });
    }

    close (event) {
        const { unblock } = this.props;

        if (typeof unblock === 'function') {
            unblock();
        }

        this.setState({ open: false }, function () {
            document.removeEventListener('click', this.listener);
            document.removeEventListener('keydown', this.basicKeyListener);
        });
    }

    listener (event) {
        const { body } = this;
        const { overlayCb, shouldStayOpen } = this.props;
        const { target } = event;

        // if we clicked inside dropdown body
        if (body && body.contains(target)) {
            // if callback shouldStayOpen was passed from child component and
            // returns true
            // should be used as exception
        } else if (shouldStayOpen && shouldStayOpen(target)) {
            event.preventDefault();
        } else {
            if (overlayCb) {
                overlayCb(this.state);
            }
            this.close();
        }
    }

    basicKeyListener (event) {
        switch (event.keyCode) {
            case this.keyCodes.ESC:
                event.preventDefault();
                event.stopPropagation();
                this.close();
                break;
        }
    }

    calculateAbsoluteCoords (base) {
        let {
            dropdownWidth,
            expandDirection,
            fixedHeight,
        } = this.props;
        const baseCoords = base.getBoundingClientRect();
        dropdownWidth = dropdownWidth || baseCoords.width;

        const calculateWidth = (commonStyles) => {
            const {
                styles,
                arrowStyles,
                optionsStyles,
            } = commonStyles;

            styles.width = dropdownWidth;
            return { styles, arrowStyles, optionsStyles };
        };

        const calculateVerticalCoords = (commonStyles) => {
            const {
                styles,
                arrowStyles,
                optionsStyles,
            } = commonStyles;

            const { dropdownHeight } = this.props;
            const body = base.offsetParent;
            const bodyCoords = body.getBoundingClientRect();

            const distanceToBottom = bodyCoords.bottom - baseCoords.bottom;
            const distanceToTop = baseCoords.top - bodyCoords.top;

            // if cell is positioned too low and
            // opened dropdown will cause vertical gaps
            if (
                baseCoords.top + baseCoords.height + dropdownHeight + 20
                > window.innerHeight - 20
            ) {
                if (distanceToBottom <= distanceToTop) {
                    this.setState({ renderToTop: true });

                    styles.top = 'auto';
                    styles.bottom = '110%';

                    if (expandDirection === 'center') {
                        arrowStyles.transform = 'translate(-50%) rotate(180deg)';
                    }
                } else {
                    this.setState({ renderToTop: false });
                }
                optionsStyles.maxHeight = Math
                    .max(distanceToTop, distanceToBottom) - 45;
            }
            return { styles, arrowStyles, optionsStyles };
        };

        const checkHorizontalViewportShift = (
            commonStyles,
            offset,
            expandDirection
        ) => {
            const { styles } = commonStyles;
            const windowWidth = window.innerWidth;
            const leftPos = baseCoords.width - styles.width / 2;
            const rightDropdownCorner = baseCoords.left + leftPos + styles.width;
            const leftDropdownCorner = baseCoords.left + leftPos - 10;

            if (rightDropdownCorner > windowWidth) {
                return 'left';
            } else if (leftDropdownCorner < 10) {
                return 'right';
            }
            return expandDirection || 'center';
        };

        const calculateHorizontalCoords = (commonStyles) => {
            const offset = 30;
            const {
                styles,
                arrowStyles,
                optionsStyles,
            } = commonStyles;

            expandDirection = expandDirection
                || checkHorizontalViewportShift(commonStyles, offset, expandDirection);

            switch (expandDirection) {
                case 'right':
                    this.setState({ horizontalDirection: 'left' });
                    styles.right = -styles.width + offset;
                    arrowStyles.right = styles.width - offset;
                    break;
                case 'left':
                    this.setState({ horizontalDirection: 'right' });
                    styles.left = -styles.width + offset;
                    arrowStyles.left = styles.width - offset;
                    break;
                case 'byParentWidth':
                    styles.maxWidth = baseCoords.width;
                    if (styles.width < styles.maxWidth) {
                        styles.right = 0;
                        arrowStyles.left = styles.width - offset;
                    } else {
                        arrowStyles.left = '50%';
                        arrowStyles.transform = 'translate(-50%)';
                    }
                    break;
                case 'center':
                default:
                    styles.right = -(dropdownWidth / 2) + 10;
                    arrowStyles.left = '50%';
                    arrowStyles.transform = 'translate(-50%)';
                    break;
            }
            return { styles, arrowStyles, optionsStyles };
        };

        let commonStyles = {
            styles: {},
            optionsStyles: {},
            arrowStyles: {},
        };

        commonStyles = { ...calculateWidth(commonStyles) };
        commonStyles = { ...calculateHorizontalCoords(commonStyles) };

        if (!fixedHeight) {
            commonStyles = { ...calculateVerticalCoords(commonStyles) };
        } else if (fixedHeight && typeof fixedHeight === 'number') {
            // we can set fixedHeight of dropdown as number
            commonStyles.optionsStyles.maxHeight = fixedHeight;
        }

        return {
            containerCoords: commonStyles.styles,
            optionsCoords: commonStyles.optionsStyles,
            arrowCoords: commonStyles.arrowStyles,
        };
    }

    // without this scrolling dropdown body can trigger parent scrolling,
    // e.g. listing body etc

    dontPropagate (event) {
        event.stopPropagation();
    }

    render() {

        const { open } = this.state;
        const {
            className,
            classes,
            customStyles,
            disableSelectionEffect,
        } = this.props;
        const {
            root,
            // button styles
            button,
            buttonNoChevron,
            buttonText,
            buttonTextA,
            // expanded dropdown styles
            body,
            optionsContainer,
            containerPointerPath,
            // styles applied with former .-open class
            optionsContainerOpen,
            optionsOpen,
            // styles applied if component is rendered in Listing
            selectionEffect,
        } = classes;

        const renderBtn = () => {
            const {
                btnText,
                btnPlaceholder,
                btnComponent,
                btnClassName,
                disableChevronIcon,
                tooltipContent,
            } = this.props;

            let content = null;
            const propsObj = {
                className: classNames(
                    'dropdown-base__btn-text',
                    buttonText,
                    buttonTextA,
                    btnClassName
                ),
                'data-test': 'dropdown-base__btn-text',
            };

            // dropdown btn can switch chevron icon (i.e. disable if needed)
            // disabled icon also will change some paddings inside btn
            let icon;
            if (!disableChevronIcon) {
                const iconStyle = {
                    ...dropdownBaseInlineStyles.buttonIcon,
                    ...((!!open && dropdownBaseInlineStyles.buttonIconOpen) || {}),
                };
                icon = (
                    <Icon
                        type="chevronListing"
                        style={iconStyle}
                    />
                );
            }

            // we can pass inside btn specific component, like in tagEditor dropdown
            if (btnComponent) {
                let btnContent = Array.isArray(btnComponent)
                    ? btnComponent
                    : [ btnComponent ];

                if (!btnContent.length && btnPlaceholder) {
                    btnContent = btnPlaceholder;
                }
                content = (
                    <div
                        {...propsObj}
                        style={customStyles.dropdownBaseButtonText || {}}
                    >
                        {btnContent}
                    </div>
                );
                // or we can pass inside just simple text
            } else {
                let text = btnText;
                // we can pass inside dropdown btnPlaceholder
                // prop will be used if dropdownBtn doesn't have text to show and
                // has placeholder passed as prop
                if (!btnText && btnPlaceholder) {
                    text = btnPlaceholder;
                }

                content = (
                    <div {...propsObj}>
                        {ReactHtmlParser(text)}
                    </div>
                );
            }

            // wrap content in tooltip if needed
            // tooltip text is computed on the higher level,
            // using function passed from listingStructure
            if (tooltipContent) {
                content = <Tooltip visible={content}>{tooltipContent}</Tooltip>;
            }

            const buttonStyles = {
                ...(customStyles.dropdownBaseButton || {}),
                ...((!!disableChevronIcon && customStyles.dropdownBaseButtonNoChevron) || {}),
            };

            return (
                <div
                    data-test="main-button"
                    className={classNames(
                        'dropdown-base__btn',
                        button,
                        { 'dropdown-base__btn--no-chevron': disableChevronIcon },
                        { [buttonNoChevron]: disableChevronIcon }
                    )}
                    role="button"
                    tabIndex={0}
                    onClick={this.open}
                    style={buttonStyles}
                >
                    {content}
                    {icon}
                </div>
            );
        };

        const renderDropdownBody = () => {
            const {
                containerCoords,
                optionsCoords,
                arrowCoords,
                renderToTop,
            } = this.state;
            const { children } = this.props;
            const containerStyles = {
                ...containerCoords,
                ...(customStyles.dropdownBaseOptionsContainer || {}),
            };
            const iconStyle = {
                ...dropdownBaseInlineStyles.containerPointer,
                ...((!!renderToTop && dropdownBaseInlineStyles.containerPointerArrowDown) || {}),
                ...arrowCoords,
            };

            return (
                <div
                    className={classNames(
                        'dropdown-base__body',
                        body
                    )}
                    ref={(body) => {
                        this.body = body;
                    }}
                >
                    <div
                        data-test="internal-container"
                        className={classNames(
                            'dropdown-base__options-container',
                            optionsContainer,
                            { [optionsContainerOpen]: open }
                        )}
                        style={containerStyles}
                    >
                        <ul
                            className={classNames({ [optionsOpen]: open })}
                            style={{
                                ...optionsCoords,
                                ...customStyles.dropdownBaseOptions,
                            }}
                            onScroll={this.dontPropagate}
                            onClick={this.dontPropagate}
                            onDoubleClick={this.dontPropagate}
                        >
                            {children}
                        </ul>
                        <Icon
                            type="arrow"
                            className={containerPointerPath}
                            style={iconStyle}
                        />
                    </div>
                </div>
            );
        };

        const renderStylingDiv = () => <div className={selectionEffect} />;

        const stylingDiv = open && !disableSelectionEffect ? renderStylingDiv() : false;
        const dropdownBody = open ? renderDropdownBody() : false;
        const dropdownBtn = renderBtn();

        return (

            <div
                className={classNames(
                    'dropdown-base',
                    className,
                    root,
                    { '-open': open }
                )}
                ref={(base) => {
                    this.base = base;
                }}
                style={customStyles.dropdownBaseRoot || {}}
            >
                {stylingDiv}
                {dropdownBtn}
                {dropdownBody}
            </div>

        );
    }
}

const mapStateToProps = state => ({
});

export default withRouter(connect(
    mapStateToProps,
    // mapDispatchToProps
)(injectStyles(styles)(DropdownBase)))
