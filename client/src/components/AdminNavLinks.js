import React, { Component } from 'react';
import injectStyles from 'react-jss';
import { withRouter } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import 'react-dropdown/style.css';
import styles from '../styles.js';

class AdminNavLinks extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
        };

    }

    render() {

        const { classes } = this.props;

        return (

            <div className={classes.adNavLinks}>
                <span
                    className={[classes.adNavItem, classes.adNavLink].join(' ')}
                    onClick={() => this.props.history.push('/tests')}
                >
                    tests
                </span>
                <span className={classes.adNavItem}>
                  /
                </span>
                <span
                   className={[classes.adNavItem, classes.adNavLink].join(' ')}
                   onClick={() => this.props.history.push('/admin')}
                >
                    blocks
                </span>
                <span className={classes.adNavItem}>
                  /
                </span>
                <span
                   className={[classes.adNavItem, classes.adNavLink].join(' ')}
                   onClick={() => this.props.history.push('/users')}
                >
                   users
                </span>
                <span className={classes.adNavItem}>
                   /
                </span>
                <span
                   className={[classes.adNavItem, classes.adNavLink].join(' ')}
                   onClick={() => this.props.history.push('/')}
                >
                   log out
                </span>
            </div>

        );
    }
}

const mapStateToProps = state => ({
});

export default withRouter(connect(
    mapStateToProps,
)(injectStyles(styles)(AdminNavLinks)))
