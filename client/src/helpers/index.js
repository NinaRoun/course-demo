export function sortNumber(a, b) {
    return a - b;
}

export function sortDescNumber(a, b) {
    return b - a;
}

export function formatDate(timestamp) {
    const x = new Date(timestamp);
    const dd = x.getDate();
    const mm = x.getMonth() + 1;
    const yy = x.getFullYear();
    return yy +"/" + mm+"/" + dd;
}

export function sortByAscName(a, b) {
    return a.localeCompare(b);
}

export function sortByDescName(a, b) {
    return b.localeCompare(a);
}
