import * as types from '../actions/types';
import { initialState } from './index.js';
import {
    sortNumber,
    sortDescNumber,
    sortByAscName,
    sortByDescName
} from '../helpers/index.js';

export const courseData = (state = initialState.courseData, action) => {
    switch (action.type) {
        case types.FETCH_COURSES_REQUEST:
            return {
                ...state,
                isLoading: true,
                courseError: ""
            };
        case types.FETCH_COURSES_FAILURE:
            return {
                ...state,
                isLoading: false,
                courseError: action.payload,
            };
        case types.FETCH_COURSES_SUCCESS:
            return {
                ...state,
                courses: [...action.payload],
                isLoading: false,
                courseError: "",
            };
        case types.DELETE_COURSE_REQUEST:
            return {
                ...state,
                isLoading: true,
                courseError: "",
            };
        case types.DELETE_COURSE_FAILURE:
            return {
                ...state,
                isLoading: false,
                courseError: action.payload,
            };
        case types.DELETE_COURSE_SUCCESS:
            return {
                ...state,
                courses: [...action.payload],
                isLoading: false,
                courseError: "",
            };
        case types.PUT_COURSE_REQUEST:
            return {
                ...state,
                isLoading: true,
                courseError: "",
            };
        case types.PUT_COURSE_FAILURE:
            return {
                ...state,
                isLoading: false,
                courseError: action.payload,
            };
        case types.PUT_COURSE_SUCCESS:
            return {
                ...state,
                courses: [...action.payload],
                isLoading: false,
                courseError: "",
            };
        case types.UPDATE_COURSE_REQUEST:
            return {
                ...state,
                isLoading: true,
                courseError: "",
            };
        case types.UPDATE_COURSE_FAILURE:
            return {
                ...state,
                isLoading: false,
                courseError: action.payload,
            };
        case types.UPDATE_COURSE_SUCCESS:
            return {
                ...state,
                courses: [...action.payload],
                isLoading: false,
                courseError: "",
            };
        default:
            return state
    }
};

export const testData = (state = initialState.testData, action) => {
    switch (action.type) {
        case types.ORDER_TESTS_NAME_ASC:
            const orderedAscTests = [...state.tests].sort((a, b) => sortByAscName(a.name, b.name));
            return {
                ...state,
                tests: orderedAscTests,
                isAscTestNameOrder: true,
            };
        case types.ORDER_TESTS_NAME_DESC:
            const orderedDescTests = [...state.tests].sort((a, b) => sortByDescName(a.name, b.name));
            return {
                ...state,
                tests: orderedDescTests,
                isAscTestNameOrder: false,
            };
        case types.ORDER_TESTS_ID_ASC:
            const orderedAscTestIds = [...state.tests].sort((a, b) => sortNumber(a.id, b.id));
            return {
                ...state,
                tests: orderedAscTestIds,
                isAscTestIdOrder: true,
            };
        case types.ORDER_TESTS_ID_DESC:
            const orderedDescTestsIds = [...state.tests].sort((a, b) => sortDescNumber(a.id, b.id));
            return {
                ...state,
                tests: orderedDescTestsIds,
                isAscTestIdOrder: false,
            };
        case types.FETCH_TESTS_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: ""
            };
        case types.FETCH_TESTS_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        case types.FETCH_TESTS_SUCCESS:
            return {
                ...state,
                tests: [...action.payload],
                isLoading: false,
                error: "",
            };
        case types.DELETE_TEST_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: "",
            };
        case types.DELETE_TEST_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        case types.DELETE_TEST_SUCCESS:
            return {
                ...state,
                tests: [...action.payload],
                isLoading: false,
                error: "",
            };
        case types.PUT_TEST_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: "",
            };
        case types.PUT_TEST_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        case types.PUT_TEST_SUCCESS:
            return {
                ...state,
                tests: [...action.payload],
                isLoading: false,
                error: "",
            };
        case types.SET_CURRENT_TEST:
            return {
                ...state,
                currentTest: { ...action.payload },
            };
        case types.UPDATE_TEST_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: "",
            };
        case types.UPDATE_TEST_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        case types.UPDATE_TEST_SUCCESS:
            const updatedTests = state.tests.map(test => {
                const { _id, id, name, summary } = action.payload;
                if (test._id === _id) {
                    return { ...test, id, name, summary };
                }

                return test;
            });

            return {
                ...state,
                tests: [...updatedTests],
                currentTest: { ...action.payload },
                isLoading: false,
                error: "",
            };
        default:
            return state
    }
};

export const questionsData = (state = initialState.questionsData, action) => {
    switch (action.type) {
        case types.FETCH_QUESTIONS_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: ""
            };
        case types.FETCH_QUESTIONS_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        case types.FETCH_QUESTIONS_SUCCESS:
            //console.log('action.payload ' ,action.payload);
            return {
                ...state,
                questions: [...action.payload],
                isLoading: false,
                error: "",
            };
        case types.PUT_QUESTION_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: "",
            };
        case types.PUT_QUESTION_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        case types.PUT_QUESTION_SUCCESS:
            return {
                ...state,
                questions: [ ...action.payload.allQuestions ],
                currentQuestions: [ ...state.currentQuestions, action.payload.questionUpdate ],
                isLoading: false,
                error: false,
            };
        case types.DELETE_QUESTION_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: "",
            };
        case types.DELETE_QUESTION_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        case types.DELETE_QUESTION_SUCCESS:
             const filteredCurrentQuestions = [...state.currentQuestions].filter(question => question._id !== action.payload);
            const filteredQuestions = [...state.questions].filter(question => question._id !== action.payload);
            return {
                ...state,
                currentQuestions: filteredCurrentQuestions.sort((a, b) => sortNumber(a.id, b.id)),
                questions: filteredQuestions,
                isLoading: false,
                error: "",
                isAscQuestionNameOrder: false,
                isAscQuestionIdOrder: true,
            };
        case types.UPDATE_QUESTION_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: "",
            };
        case types.UPDATE_QUESTION_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        case types.UPDATE_QUESTION_SUCCESS:
            const updatedQuestions = state.currentQuestions.map(question => {
                const { _id, id, text } = action.payload;
                if (question._id === _id) {
                    return { ...question, id, text };
                }

                return question;
            });

            return {
                ...state,
                currentQuestions: [ ...updatedQuestions ],
                currentQuestion: { ...action.payload },
                isLoading: false,
                error: "",
            };
        case types.SET_CURRENT_QUESTIONS:
            return {
                ...state,
                currentQuestions: [ ...action.payload ],
            };
        case types.SET_CURRENT_QUESTION:
            return {
                ...state,
                currentQuestion: action.payload,
            };
        default:
            return state
    }
};

export const blockData = (state = initialState.blockData, action) => {
    switch (action.type) {
        case types.SET_ID_FOR_BLOCKS:
            return {
                ...state,
                idForBlocks: action.payload,
            };
        case types.SET_CURRENT_BLOCK_IDS:
            return {
                ...state,
                idsOfCurrentBlocks: [...action.payload],
            };
        case types.SET_CORRESPONDING_BLOCKS:
            return {
                ...state,
                correspondingBlocks: [...action.payload],
            };
        case types.FETCH_BLOCKS_REQUEST:
            return {
                ...state,
                isLoading: true,
                blocksError: ""
            };
        case types.FETCH_BLOCKS_FAILURE:
            return {
                ...state,
                isLoading: false,
                blocksError: action.payload,
            };
        case types.FETCH_BLOCKS_SUCCESS:
            const sortedBlocks = action.payload.sort((a, b) => sortNumber(a.id, b.id));
            return {
                ...state,
                blocks: [...sortedBlocks],
                isLoading: false,
                blocksError: "",
                isAscBlockNameOrder: false,
                isAscBlockIdOrder: true,
            };
        case types.ORDER_BLOCKS_NAME_ASC:
            const orderedAscBlocks = [...state.blocks].sort((a, b) => sortByAscName(a.name, b.name));
            return {
                ...state,
                blocks: orderedAscBlocks,
                isAscBlockNameOrder: true,
            };
        case types.ORDER_BLOCKS_NAME_DESC:
            const orderedDescBlocks = [...state.blocks].sort((a, b) => sortByDescName(a.name, b.name));
            return {
                ...state,
                blocks: orderedDescBlocks,
                isAscBlockNameOrder: false,
            };
        case types.ORDER_BLOCKS_ID_ASC:
            const orderedAscBlockIds = [...state.blocks].sort((a, b) => sortNumber(a.id, b.id));
            return {
                ...state,
                blocks: orderedAscBlockIds,
                isAscBlockIdOrder: true,
            };
        case types.ORDER_BLOCKS_ID_DESC:
            const orderedDescBlocksIds = [...state.blocks].sort((a, b) => sortDescNumber(a.id, b.id));
            return {
                ...state,
                blocks: orderedDescBlocksIds,
                isAscBlockIdOrder: false,
            };
        default:
            return state
    }
};

export const stepData = (state = initialState.stepData, action) => {
    switch (action.type) {
        case types.SET_CHOSEN_STEPS:
            return {
                ...state,
                chosenSteps: action.payload,
            };
        case types.SET_ID_FOR_STEPS:
            return {
                ...state,
                idForSteps: action.payload,
            };
        case types.SET_CURRENT_STEPS_IDS:
            return {
                ...state,
                idsOfCurrentSteps: [...action.payload],
            };
        case types.SET_CORRESPONDING_STEPS:
            return {
                ...state,
                correspondingSteps: [...action.payload],
            };
        case types.FETCH_STEPS_REQUEST:
            return {
                ...state,
                isLoading: true,
                stepsError: ""
            };
        case types.FETCH_STEPS_FAILURE:
            return {
                ...state,
                isLoading: false,
                stepsError: action.payload,
            };
        case types.FETCH_STEPS_SUCCESS:
            const sortedSteps = action.payload.sort((a, b) => sortNumber(a.id, b.id));
            return {
                ...state,
                steps: [...sortedSteps],
                isLoading: false,
                stepsError: "",
            };
        default:
            return state
    }
};

export const courseModal = (state = initialState.courseModal, action) => {
    switch (action.type) {
        case types.OPEN_COURSE_MODAL:
            return {
                ...state,
                modalIsOpen: true,
            };
        case types.EDIT_NAME:
            return {
                ...state,
                editNameIsOpen: !state.editNameIsOpen,
            };
        case types.EDIT_SUMMARY:
            return {
                ...state,
                editSummaryIsOpen: !state.editSummaryIsOpen,
            };
        case types.SET_MODAL_INITIAL_STATE:
            return {
                ...state,
                modalIsOpen: false,
                editNameIsOpen: false,
                editSummaryIsOpen: false,
            };
        default:
            return state
    }
};

export const registration = (state = initialState.registration, action) => {
    switch (action.type) {
        case types.FETCH_REGISTER_REQUEST:
            return {
                ...state,
                isRegistered: false,
                isLoading: true,
                error: ""
            };
        case types.FETCH_REGISTER_SUCCESS:
            return {
                ...state,
                isRegistered: true,
                isLoading: false,
                error: "",
            };
        case types.FETCH_REGISTER_FAILURE:
            return {
                ...state,
                isRegistered: false,
                isLoading: false,
                errors: [...action.payload],
            };
        default:
            return state
    }
};

export const validation = (state = initialState.validation, action) => {
    switch (action.type) {
        case types.FETCH_LOGIN_REQUEST:
            return {
                ...state,
                isLogged: false,
                isLoading: true,
                errors: [],
                userProfile: {},
            };
        case types.FETCH_LOGIN_SUCCESS:
            const { _id: userId, name: userName, email: userEmail } = action.payload;
            const isAdmin = userName === 'admin';
            return {
                ...state,
                isLogged: true,
                isLoading: false,
                errors: [],
                userProfile: { ...state.userProfile, userId, userName, userEmail, isAdmin }
    };
        case types.FETCH_LOGIN_FAILURE:
            return {
                ...state,
                isLogged: false,
                isLoading: false,
                errors: [...action.payload],
                userProfile: {},
            };
        case types.LOG_OUT:
            return {
                ...state,
                isLogged: false,
                isLoading: false,
                error: [],
                userId: 0,
                userProfile: { ...state.userProfile,
                    userId: '',
                    userName: '',
                    userEmail: '',
                    isAdmin: false,
                    currentBlocks: [],
                    currentBlock: {},
                    currentSteps: [],
                    currentStep: {},
                    finishedStep: {},
                    activeStep: 0,
                    finishedSteps: [],
                    finishedBlocks: [],
                }
            };
        case types.OPEN_IDS_OF_CHOSEN_BLOCKS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    chosenBlocks: [...action.payload],
                    maxActiveStep: action.payload.maxActiveStep,
                    activeStep: action.payload.maxActiveStep,
                }
            };
        case types.SET_PREVIOUS_STEPS_TO_SHOW:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    previousStepsToShow: [...action.payload.stepsToShow],
                    currentSteps: [],
                }
            };
        case types.SET_CURRENT_STEPS_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    previousStepsToShow: [],
                    currentSteps: [],
                    maxActiveStep: 0,
                    activeStep: 0,
                    profileError: "",
                    stepsAreLoading: true,
                }
            };
        case types.SET_CURRENT_STEPS_SUCCESS:
            console.log(action.payload);
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    currentSteps: [...action.payload.currentSteps],
                    maxActiveStep: action.payload.maxActiveStep,
                    activeStep: 0,
                    stepsAreLoading: false,
                }
            };
        case types.SET_CURRENT_STEPS_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    profileError: action.payload,
                    stepsAreLoading: false,
                }
            };
        case types.GET_CURRENT_STEPS_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    currentSteps: [],
                    maxActiveStep: 0,
                    activeStep: 0,
                    profileError: "",
                    stepsAreLoading: true,
                }
            };
        case types.GET_CURRENT_STEPS_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    currentSteps: [...action.payload.currentSteps],
                    maxActiveStep: action.payload.maxActiveStep,
                    activeStep: action.payload.activeStep,
                    profileError: "",
                    stepsAreLoading: false,
                }
            };
        case types.GET_CURRENT_STEPS_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    profileError: action.payload,
                    stepsAreLoading: false,
                }
            };
        case types.SET_CURRENT_STEP_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    profileError: "",
                    stepsAreLoading: true,
                    currentStep: {},
                }
            };
        case types.SET_CURRENT_STEP_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    profileError: action.payload,
                    stepsAreLoading: false,
                }
            };
        case types.SET_CURRENT_STEP_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    currentStep: action.payload,
                    finishedStep: {},
                }
            };
        case types.SET_FINISHED_STEP:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    finishedStep: action.payload,
                    currentStep: {},
                }
            };
        case types.PUSH_LESSON_TO_FINISHED_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    finishedSteps: [],
                    profileError: "",
                    stepsAreLoading: true,
                }
            };
        case types.PUSH_LESSON_TO_FINISHED_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    profileError: action.payload,
                    stepsAreLoading: false,
                }
            };
        case types.PUSH_LESSON_TO_FINISHED_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    finishedSteps: [...action.payload.finishedSteps], //CHECK
                    profileError: "",
                    stepsAreLoading: false,
                }
            };
        case types.GET_FINISHED_BLOCKS_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    profileError: "",
                    blocksAreLoading: true,
                }
            };
        case types.GET_FINISHED_BLOCKS_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    profileError: "",
                    blocksAreLoading: false,
                    finishedBlocks: [...action.payload],
                }
            };
        case types.GET_FINISHED_BLOCKS_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    blocksAreLoading: false,
                    profileError: action.payload,
                }
            };
        case types.GET_FINISHED_STEPS_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    profileError: "",
                    stepsAreLoading: true,
                }
            };
        case types.GET_FINISHED_STEPS_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    profileError: "",
                    stepsAreLoading: false,
                    finishedSteps: [...action.payload],
                }
            };
        case types.GET_FINISHED_STEPS_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    stepsAreLoading: false,
                    profileError: action.payload,
                }
            };
        case types.GET_CHOSEN_COURSE_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    isChosenCourseLoading: true,
                    profileError: "",
                }
            };
        case types.GET_CHOSEN_COURSE_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    isChosenCourseLoading: false,
                    profileError: action.payload,
                }
            };
        case types.GET_CHOSEN_COURSE_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    currentCourse: {...action.payload},
                    currentBlocksIds: [...action.payload.blocks],
                    isChosenCourseLoading: false,
                    profileError: "",
                }
            };
        case types.SET_CHOSEN_COURSE_EMPTY:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    currentCourse: {},
                    isChosenCourseLoading: false,
                    profileError: "",
                }
            };
        case types.SET_CHOSEN_COURSE_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    isChosenCourseLoading: true,
                    profileError: "",
                }
            };
        case types.SET_CHOSEN_COURSE_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    isChosenCourseLoading: false,
                    profileError: action.payload,
                }
            };
        case types.SET_CHOSEN_COURSE_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    currentCourse: {...action.payload},
                    currentBlocksIds: [...action.payload.blocks],
                    isChosenCourseLoading: false,
                    profileError: "",
                }
            };
        case types.SET_CURRENT_BLOCKS_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    blocksAreLoading: true,
                    profileError: "",
                }
            };
        case types.SET_CURRENT_BLOCKS_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    blocksAreLoading: false,
                    profileError: action.payload,
                }
            };
        case types.SET_CURRENT_BLOCKS_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    blocksAreLoading: false,
                    currentBlocks: [...action.payload],
                }
            };
        case types.GET_CURRENT_STEP_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    stepsAreLoading: true,
                    profileError: "",
                    currentStep: {},
                }
            };
        case types.GET_CURRENT_STEP_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    stepsAreLoading: false,
                    profileError: action.payload,
                }
            };
        case types.GET_CURRENT_STEP_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    stepsAreLoading: false,
                    currentStep: action.payload.currentStep,
                    maxActiveStep: action.payload.maxActiveStep,
                    activeStep: action.payload.activeStep,
                }
            };
        case types.SET_CURRENT_BLOCK_REQUEST:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    blockIsLoading: true,
                    profileError: "",
                }
            };
        case types.SET_CURRENT_BLOCK_FAILURE:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    blockIsLoading: false,
                    profileError: action.payload,
                }
            };
        case types.SET_CURRENT_BLOCK_SUCCESS:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    blockIsLoading: false,
                    currentBlock: {...action.payload},
                }
            };
        case types.RESET_ACTIVE_STEP:
            return {
                ...state,
                userProfile: {
                    ...state.userProfile,
                    maxActiveStep: 0,
                    activeStep: 0,
                }
            };
        default:
            return state
    }
};

export const admin = (state = initialState.admin, action) => {
    switch (action.type) {
        case types.SET_CURRENT_USER:
            return {
                ...state,
                currentUser: action.payload,
            };
        case types.FETCH_USERS_REQUEST:
            return {
                ...state,
                adminLoading: true,
                adminError: ""
            };
        case types.FETCH_USERS_FAILURE:
            return {
                ...state,
                adminLoading: false,
                adminError: action.payload,
            };
        case types.FETCH_USERS_SUCCESS:
            return {
                ...state,
                users: [...action.payload],
                adminLoading: false,
                adminError: "",
                isAscUserRegistrationOrder: false,
                isAscUserNameOrder: true,
            };
        case types.FETCH_ASC_USERS_SUCCESS:
            return {
                ...state,
                users: [...action.payload],
                adminLoading: false,
                adminError: "",
                isAscUserRegistrationOrder: true,
            };
        case types.ORDER_USERS_ASC:
            const orderedAscUsers = [...state.users].sort((a, b) => sortByAscName(a.name, b.name));
            return {
                ...state,
                users: orderedAscUsers,
                isAscUserNameOrder: true,
            };
        case types.ORDER_USERS_DESC:
            const orderedDescUsers = [...state.users].sort((a, b) => sortByDescName(a.name, b.name));
            return {
                ...state,
                users: orderedDescUsers,
                isAscUserNameOrder: false,
            };
        case types.SET_BLOCK_TO_EDIT:
            return {
                ...state,
                blockToEdit: action.payload,
                adminLoading: false,
            };
        case types.PUT_BLOCK_REQUEST:
            return {
                ...state,
                adminLoading: true,
                adminError: "",
            };
        case types.PUT_BLOCK_FAILURE:
            return {
                ...state,
                adminLoading: false,
                adminError: action.payload,
            };
        case types.PUT_BLOCK_SUCCESS:
            return {
                ...state,
                lastAddedBlock: action.payload,
                adminLoading: false,
                adminError: "",
            };
        case types.UPDATE_BLOCK_REQUEST:
            return {
                ...state,
                adminLoading: true,
                adminError: "",
            };
        case types.UPDATE_BLOCK_FAILURE:
            return {
                ...state,
                adminLoading: false,
                adminError: action.payload,
            };
        case types.UPDATE_BLOCK_SUCCESS:
            return {
                ...state,
                blockToEdit: action.payload,
                adminLoading: false,
                adminError: "",
            };
        case types.DELETE_BLOCK_REQUEST:
            return {
                ...state,
                adminLoading: true,
                adminError: "",
            };
        case types.DELETE_BLOCK_FAILURE:
            return {
                ...state,
                adminLoading: false,
                adminError: action.payload,
            };
        case types.DELETE_BLOCK_SUCCESS:
            return {
                ...state,
                adminLoading: false,
                adminError: "",
            };
        case types.SET_STEPS_TO_EDIT:
            return {
                ...state,
                stepsToEdit: action.payload,
            };
        case types.ORDER_STEPS_NAME_ASC:
            const orderedStepsByAscName = [...state.stepsToEdit].sort((a, b) => sortByAscName(a.name, b.name));
            return {
                ...state,
                stepsToEdit: orderedStepsByAscName,
                isAscStepNameOrder: true,
            };
        case types.ORDER_STEPS_NAME_DESC:
            const orderedStepsByDescName = [...state.stepsToEdit].sort((a, b) => sortByDescName(a.name, b.name));
            return {
                ...state,
                stepsToEdit: orderedStepsByDescName,
                isAscStepNameOrder: false,
            };
        case types.ORDER_STEPS_ID_ASC:
            const orderedStepsByAscId = [...state.stepsToEdit].sort((a, b) => sortNumber(a.id, b.id));
            return {
                ...state,
                stepsToEdit: orderedStepsByAscId,
                isAscStepIdOrder: true,
            };
        case types.ORDER_STEPS_ID_DESC:
            const orderedStepsByDescId = [...state.stepsToEdit].sort((a, b) => sortDescNumber(a.id, b.id));
            return {
                ...state,
                stepsToEdit: orderedStepsByDescId,
                isAscStepIdOrder: false,
            };
        case types.SET_STEP_TO_EDIT:
            return {
                ...state,
                stepToEdit: action.payload,
            };
        case types.PUT_STEP_REQUEST:
            return {
                ...state,
                adminLoading: true,
                adminError: "",
            };
        case types.PUT_STEP_FAILURE:
            return {
                ...state,
                adminLoading: false,
                adminError: action.payload,
            };
        case types.PUT_STEP_SUCCESS:
            return {
                ...state,
                stepsToEdit: [...state.stepsToEdit, action.payload].sort((a, b) => sortNumber(a.id, b.id)),
                adminLoading: false,
                adminError: "",
                isAscStepNameOrder: false,
                isAscStepIdOrder: true,
            };
        case types.DELETE_STEP_REQUEST:
            return {
                ...state,
                adminLoading: true,
                adminError: "",
            };
        case types.DELETE_STEP_FAILURE:
            return {
                ...state,
                adminLoading: false,
                adminError: action.payload,
            };
        case types.DELETE_STEP_SUCCESS:
            const filteredSteps = [...state.stepsToEdit].filter(step => step._id !== action.payload);
            return {
                ...state,
                stepsToEdit: filteredSteps.sort((a, b) => sortNumber(a.id, b.id)),
                adminLoading: false,
                adminError: "",
                isAscStepNameOrder: false,
                isAscStepIdOrder: true,
            };
        case types.UPDATE_STEP_REQUEST:
            return {
                ...state,
                isLoading: true,
                stepsError: "",
            };
        case types.UPDATE_STEP_FAILURE:
            return {
                ...state,
                adminLoading: false,
                adminError: action.payload,
            };
        case types.UPDATE_STEP_SUCCESS:
            const updatedSteps = state.stepsToEdit.map(step => {
                const { _id, id, name, summary } = action.payload;
                if (step._id === _id) {
                    return {
                        _id,
                        id,
                        name,
                        summary,
                    }
                }

                return step;
            });

            return {
                ...state,
                stepToEdit: action.payload,
                stepsToEdit: updatedSteps,
                adminLoading: false,
                adminError: "",
            };
        default:
            return state
    }
};
