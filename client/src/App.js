import React, { Component } from 'react';
import { BrowserRouter, Route, NavLink } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import injectStyles from 'react-jss';
import AuthPage from './pages/authentication/';
import ProfilePage from './pages/profile/';
import LearningPage from './pages/learning/';
import LessonsPage from './pages/learning/LessonsPage';
import SingleLessonPage from './pages/singleLesson/';
import InstructionsPage from './pages/instructions/';
import StatisticsPage from './pages/statistics/';
import AdminPage from './pages/admin/';
import BlockPage from './pages/admin/SingleBlock';
import StepPage from './pages/admin/SingleStep';
import UsersTable from './pages/admin/UsersTable';
import SingleUser from './pages/admin/SingleUser';
import TestsList from './pages/admin/TestsList';
import SingleTest from './pages/admin/SingleTest';
import SingleQuestion from './pages/admin/SingleQuestion';
import styles from './styles.js';

const history = createBrowserHistory();

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {

        const { classes } = this.props;

        return (
            <div>

                <BrowserRouter history={history}>

                    <Route exact path="/" component={AuthPage}></Route>
                    <Route exact path="/profile" component={ProfilePage}></Route>
                    <Route exact path="/instructions" component={InstructionsPage}></Route>
                    <Route exact path="/learning" component={LearningPage}></Route>
                    <Route exact path="/statistics" component={StatisticsPage}></Route>
                    <Route exact path="/lessons" component={LessonsPage}></Route>
                    <Route exact path="/lesson" component={SingleLessonPage}></Route>
                    <Route exact path="/admin" component={AdminPage}></Route>
                    <Route exact path="/block" component={BlockPage}></Route>
                    <Route exact path="/step" component={StepPage}></Route>
                    <Route exact path="/users" component={UsersTable}></Route>
                    <Route exact path="/user" component={SingleUser}></Route>
                    <Route exact path="/tests" component={TestsList}></Route>
                    <Route exact path="/test" component={SingleTest}></Route>
                    <Route exact path="/question" component={SingleQuestion}></Route>

                </BrowserRouter>

            </div>
        );
    }

}

/* TODO:
* when log out, reset all state to initial
* investigate how to add space lines into textfields when add a summary
* log out from admin
* disable cursor: pointer over lessons which are locked
* admin should get a table of all users with info about their progress and results
* remove mark as read button from finished steps
* add loaders everywhere while isLoading equals true
* remove window resizing methods from admin components if not heeded (in the end)
* put same components as shared
* implement proptypes and default types (in the end)
* move dbURL and errorMsg from actions to constants
* put items.length after <TableBody> for rendering items
* make reordering on singleQuestion page
* make filtering for all pages
* */

export default injectStyles(styles)(App);
