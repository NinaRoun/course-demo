const themeColor = '#d2691e';
const mainFontColor = '#727272';
const secondFontColor = 'white';
const greyHover = '#cccccc';
const greyBackground = '#e6e6e6';
const finishedStateColor = '#2db300';

export default {
    '@global': {
        html: {
            height: "100%",
        },
        body: {
            backgroundColor: "white",
            color: mainFontColor,
            margin: 0,
            padding: 0,
            height: "100%",
            fontFamily: "-apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\",\n" +
                "    \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\",\n" +
                "    sans-serif",
        },
        '#root': {
            height: "100%",
            '& > div': {
                height: '100%',
            }
        },
        '#transition-modal-title': {
            margin: '30px auto 0 auto',
            width: '90%',
        },
        h1: {
            fontWeight: "bold",

            margin: 0,
        },
        h2: {
            textAlign: "center",
        },
        p: {
            fontSize: "14px",
            fontWeight: 100,
            lineHeight: "20px",
            letterSpacing: "0.5px",
            margin: "20px 0 30px",
        },
        span: {
            fontSize: "12px",
        },
        a: {
            color: "#333",
            fontSize: "14px",
            textDecoration: "none",
            margin: "15px 0",
        },
        button: {
            borderRadius: "20px",
            border: "1px solid #d2691e",
            backgroundColor: themeColor,
            color: "#FFFFFF",
            fontSize: "12px",
            fontWeight: "bold",
            padding: "12px 45px",
            letterSpacing: "1px",
            textTransform: "uppercase",
            transition: "transform 80ms ease-in",
            '&:active': {
                transform: "scale(0.95)",
            },
            '&:focus': {
                outline: "none",
            },
        },
        form: {
            backgroundColor: "#FFFFFF",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            padding: "0 50px",
            height: "100%",
            textAlign: "center",
        },
        input: {
            backgroundColor: "#e8f0fe",
            border: "none",
            padding: "12px 15px",
            margin: "8px 0",
            width: "100%",
        },
        '.MuiListItem-gutters': {
            paddingLeft: '10px',
            paddingRight: "40px",
        },
        '.MuiFab-root': {
            width: '40px',
            height: '40px',
            margin: '0 10px 0 10px',
        },
        '.MuiFormControl-root': {
            width: '100%',
        },
        '.MuiTypography-colorTextPrimary': {
            lineHeight: '30px',
            height: '30px',
        },
    },
    pageContent: {
        height: "100%",
    },
    collapseButtonContainer: {
        display: "flex",
        backgroundColor: themeColor,
        color: secondFontColor,
        // '@media (min-width: 700px)': {
        //     display: "none",
        // },
    },
    collapseButton: {
        border: "2px solid #727272",
        marginRight: "20px",
    },
    collapseButtonHeader: {
    },
    collapseDropdown: {
        position: "absolute",
        width: "100%",
        backgroundColor: "#d9d9d9",
        color: mainFontColor,
    },
    collapseNavbarItem: {
        '&:hover': {
            backgroundColor: greyHover,
        },
        '&:nth-child(even)': {
            backgroundColor: greyBackground,
            '&:hover': {
                backgroundColor: greyHover,
            },
        },
    },
    collapseNabarItemContent: {
        marginRight: "auto",
        marginLeft: "auto",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        //borderBottom: "1px solid grey",
    },
    collapseNavbarItemText: {
        '& > span': {
            fontSize: "1.5rem",
        },
    },
    authBody: {
        backgroundColor: "#f6f5f7",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        height: "100vh",
        margin: "-20px 0 50px",
    },
    authForm: {
        backgroundColor: "#fff",
        borderRadius: "10px",
        boxShadow: "0 14px 28px rgba(0,0,0,0.25), \n" +
            "\t\t\t0 10px 10px rgba(0,0,0,0.22)",
        position: "relative",
        overflow: "hidden",
        width: "768px",
        maxWidth: "100%",
        minHeight: "480px",
    },
    formContainer: {
        position: "absolute",
        top: 0,
        height: "100%",
        transition: "all 0.6s ease-in-out",
    },
    signInUpContainer: {
      left: 0 ,
      width: "50%",
      ZIndex: 2,
    },
    registrationError: {
      height: "2rem",
      display: "flex",
      justifyContent: "center",
      flexDirection: "column",
      color: themeColor,
      fontSize: "14px",
    },
    overlayContainer: {
        position: "absolute",
        top: 0,
        left: "50%",
        width: "50%",
        height: "100%",
        overflow: "hidden",
        transition: "transform 0.6s ease-in-out",
        ZIndex: 100,
    },
    overlay: {
      backgroundColor: themeColor,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      backgroundPosition: "0 0",
      color: "#FFFFFF",
      position: "relative",
      left: "-100%",
      height: "100%",
      width: "200%",
      transform: "translateX(0)",
      transition: "transform 0.6s ease-in-out",
    },
    overlayPanel: {
        position: "absolute",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        padding: "0 40px",
        textAlign: "center",
        top: 0,
        height: "100%",
        width: "50%",
        transform: "translateX(0)",
        transition: "transform 0.6s ease-in-out",
    },
    overlayPanelRight: {
        right: 0,
        transform: "translateX(0)",
    },
    profileContent: {
        display: "flex",
        width: "100%",
        height: "100%",
        //gridTemplateColumns: "1fr 3fr",
    },
    flexContainer: {
        display: "flex",
        height: "50px",
        justifyContent: "center",
        backgroundColor: "#af554f",
    },
    flexContainerItem: {
        width: "30%",
        fontSize: "30px",
        textAlign: "center",
        verticalAlign: "middle",
        paddingTop: "5px",
        '& a': {
            color: "white",
            textDecoration: "none",
        }
    },
    pageTitle: {
        marginLeft: "30px",
    },
    userCardGrid: {
        display: "flex",
        //height: "100%",
        // '@media (min-width: 700px)': {
        //     display: "flex",
        // },
    },
    contentGrid: {
        width: "100%",
    },
    userCardContainer: {
        '& $userCard': {
            color: secondFontColor,
            borderRadius: 0,
            backgroundColor: themeColor,
        },
    },
    userCard: {
        minWidth: 200,
        height: "100% !important",
        //position: "absolute",
        fontWeight: "bold",
        width: "20%",
    },
    userAvatar: {
        margin: "0 auto",
    },
    userCardName: {
        marginBottom: 0,
        textAlign: "center",
    },
    userCardHeader: {
        '& $userAvatar': {
            width: "70px",
            height: "70px",
        },
    },
    userCardMenu: {
        float: "right",
    },
    userCardActions: {
    },
    userCardNavbar: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        //backgroundColor: themeColor,
        borderRadius: "4px",
    },
    userCardNavbarItem: {
        width: "100%",
    },
    ghostButton: {
      backgroundColor: "transparent",
      borderColor: "#FFFFFF",
    },
    pageText: {
        margin: "10px",
        padding: "10px",
    },
    listItem: {
        padding: "10px",
        margin: "10px",
        '& > span': {
            fontSize: "16px",
        },
        '&:hover': {
            backgroundColor: "#bdbdbd",
            cursor: "pointer",
            borderRadius: "4px",
            //color: themeColor,
        },
    },
    table: {
        backgroundColor: greyBackground,
    },
    tableItem: {
        '&:hover': {
            backgroundColor: greyHover,
            cursor: "pointer",
        },
    },
    checkCurrentLesson: {
        color: themeColor,
    },
    checkFinishedLesson: {
        color: finishedStateColor,
    },
    blockListName: {
        display: 'inline',
        paddingLeft: '30px',
        width: '95%',
    },
    stepListName: {
        display: 'inline',
        paddingLeft: '30px',
        width: '95%',
    },
    blockListDeleteButton: {
        margin: '0 40px 0 40px',
        display: 'inline',
        width: '5%',
    },
    stepListDeleteButton: {
        //margin: '0 40px 0 40px',
        display: 'inline',
        width: '5%',
    },
    blockListButton: {
    },
    adminContent: {
        margin: '10px',
        padding: '30px 20px',
    },
    adHeaderName: {
        fontWeight: 'bold',
        padding: '5px 10px',
        margin: '10px 7px',
        display: 'inline',
        color: '#000000',
    },
    adTextfieldName: {
        padding: '5px 10px',
        margin: '10px 7px',
    },
    adHeader: {
        fontWeight: 'bold',
        padding: '5px 10px',
        margin: '10px 7px',
        color: '#000000',
    },
    adUserHeader: {
        padding: '5px 10px',
        margin: '10px 7px',
        color: mainFontColor,
    },
    adTextfield: {
        padding: '5px 10px',
        margin: '10px 7px 30px 7px',
    },
    editButton: {
        display: 'inline',
        float: 'right',
    },
    adStepList: {
        margin: '30px 0',
    },
    adModalFade: {
        backgroundColor: greyBackground,
        position: 'absolute',
        borderRadius: '4px',
        width: '50%',
        height: '20%',
        top: '25%',
        left: '25%',
        textAlign: 'center',
    },
    adModalButtonContainer: {
        display: 'flex',
        margin: '30px',
        justifyContent: 'space-evenly',
    },
    adAddItemContainer: {
        margin: '30px 0',
    },
    newTextareaName: {
        margin: '15px 0',
    },
    newTextarea: {
        margin: '0 0 15px 0',
    },
    adItemList: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    adDeleteButton: {
        width: '50px',
    },
    adTableOrderNumber: {
        width: '40%',
    },
    adTableHeader: {
        fontWeight: 'bold',
        backgroundColor: greyHover,
    },
    adTableRow: {
        cursor: 'pointer',
        '&:hover': {
            backgroundColor: greyBackground,
        },
    },
    adNav: {
        padding: '10px 0 10px 18px',
        marginBottom: '20px',
        backgroundColor: themeColor,
        borderRadius: '4px',
        display: 'flex',
        justifyContent:'space-between',
    },
    adNavItem: {
        padding: '0 10px',
        fontSize:'inherit',
        color: '#000000',
    },
    adNavLinks: {
        height: '30px',
        lineHeight: '30px',
    },
    adNavLink: {
        cursor: 'pointer',
        '&:hover': {
            color: '#3f51b5',
        },
    },
    adFilterContainer: {
        padding: '10px 0 10px 18px',
        marginBottom: '20px',
        borderRadius: '4px',
        display: 'flex',
        justifyContent:'center',
    },
    warningIsOpen: {
        display: 'block',
        color: 'red',
        margin: '10px 0',
        border: '1px solid #FF2D3D',
        borderRadius: '8px',
        padding: '10px 12px',
    },
    warningIsClosed: {
        display: 'none',
    },
    adCreateItemContainer: {
        margin: '40px 0 0 0',
    },
    paginationContainer: {
        cursor: 'pointer',
        float: 'right',
        display: 'inlineBlock',
        padding: '25px',
            '& span': {
                padding: '8px 16px',
            },
    },
    activePage: {
        backgroundColor: greyHover,
        borderRadius: '4px',
    },
    adUserInfoItem: {
        display: 'flex',
    },
    adSelect: {
        marginBottom: '15px',
    },
    adSelectItem: {
        margin: '12px 0',
    },
}
