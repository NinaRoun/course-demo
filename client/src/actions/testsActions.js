import 'cross-fetch/polyfill';
import axios from "axios";
import * as types from './types';

const dbURL = "http://localhost:9000/server/";
const errorMsg = "An error has occurred while sending the request!";

function fetchTestsRequest() {
    return {
        type: types.FETCH_TESTS_REQUEST
    }
}

function fetchTestsSuccess(body) {
    return {
        type: types.FETCH_TESTS_SUCCESS,
        payload: body
    }
}

function fetchTestsFailure() {
    return {
        type: types.FETCH_TESTS_FAILURE,
        payload: errorMsg
    }
}

export function fetchTests() {
    return dispatch => {
        dispatch(fetchTestsRequest());
        return axios.get(`${dbURL}getTests`)
            .then((res) => {
                if(res.data.success) {
                    dispatch(fetchTestsSuccess(res.data.data));
                } else {
                    dispatch(fetchTestsFailure())
                }
        });
    }
}

function fetchDeleteTestRequest() {
    return {
        type: types.DELETE_COURSE_REQUEST
    }
}

function fetchDeleteTestSuccess(body) {
    return {
        type: types.DELETE_COURSE_SUCCESS,
        payload: body
    }
}

function fetchDeleteTestFailure() {
    return {
        type: types.DELETE_COURSE_FAILURE,
        payload: errorMsg
    }
}

export function deleteTest(id, blockId) {
    return dispatch => {
        dispatch(fetchDeleteTestRequest());
        return axios.delete(`${dbURL}deleteTest`, {
            data: {
                id,
                blockId,
            }
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchDeleteTestSuccess(res.data.data));
            } else {
                dispatch(fetchDeleteTestFailure());
            }
        });
    }
}

function fetchPutTestRequest() {
    return {
        type: types.PUT_TEST_REQUEST
    }
}

function fetchPutTestSuccess(body) {
    return {
        type: types.PUT_TEST_SUCCESS,
        payload: body
    }
}

function fetchPutTestFailure() {
    return {
        type: types.PUT_TEST_FAILURE,
        payload: errorMsg
    }
}

export function putTest(id, name, summary, blockId) {
    return dispatch => {
        dispatch(fetchPutTestRequest());
        return axios.post(`${dbURL}putTest`, {
                id,
                name,
                summary,
                blockId,
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchPutTestSuccess(res.data.allTests));
            } else {
                dispatch(fetchPutTestFailure());
            }
        });
    }
}


