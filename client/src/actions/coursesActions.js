import 'cross-fetch/polyfill';
import axios from "axios";
import * as types from './types';

const dbURL = "http://localhost:9000/server/";
const errorMsg = "An error has occurred while sending the request!";

function fetchCoursesRequest() {
    return {
        type: types.FETCH_COURSES_REQUEST
    }
}

function fetchCoursesSuccess(body) {
    return {
        type: types.FETCH_COURSES_SUCCESS,
        payload: body
    }
}

function fetchCoursesFailure() {
    return {
        type: types.FETCH_COURSES_FAILURE,
        payload: errorMsg
    }
}

export function fetchCourses() {
    return dispatch => {
        dispatch(fetchCoursesRequest());
        return axios.get(`${dbURL}getCourses`)
            .then((res) => {
                if(res.data.success) {
                    dispatch(fetchCoursesSuccess(res.data.data));
                } else {
                    dispatch(fetchCoursesFailure())
                }
        });
    }
}

function fetchDeleteCourseRequest() {
    return {
        type: types.DELETE_COURSE_REQUEST
    }
}

function fetchDeleteCourseSuccess(body) {
    return {
        type: types.DELETE_COURSE_SUCCESS,
        payload: body
    }
}

function fetchDeleteCourseFailure() {
    return {
        type: types.DELETE_COURSE_FAILURE,
        payload: errorMsg
    }
}

export function deleteCourse(id) {
    return dispatch => {
        console.log('id ', id);
        dispatch(fetchDeleteCourseRequest());
        return axios.delete(`${dbURL}deleteCourses`, {
            data: {
                id
            }
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchDeleteCourseSuccess(res.data.data));
            } else {
                dispatch(fetchDeleteCourseFailure());
            }
        });
    }
}

function fetchPutCourseRequest() {
    return {
        type: types.DELETE_COURSE_REQUEST
    }
}

function fetchPutCourseSuccess(body) {
    return {
        type: types.DELETE_COURSE_SUCCESS,
        payload: body
    }
}

function fetchPutCourseFailure() {
    return {
        type: types.DELETE_COURSE_FAILURE,
        payload: errorMsg
    }
}

export function putCourse(id, name, summary) {
    return dispatch => {
        console.log('name ', name);
        dispatch(fetchPutCourseRequest());
        return axios.post(`${dbURL}putCourses`, {
                id,
                name,
                summary
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchPutCourseSuccess(res.data.data));
            } else {
                dispatch(fetchPutCourseFailure());
            }
        });
    }
}

function fetchUpdateCourseRequest() {
    return {
        type: types.UPDATE_COURSE_REQUEST
    }
}

function fetchUpdateCourseSuccess(body) {
    return {
        type: types.UPDATE_COURSE_SUCCESS,
        payload: body
    }
}

function fetchUpdateCourseFailure() {
    return {
        type: types.UPDATE_COURSE_FAILURE,
        payload: errorMsg
    }
}

export function updateCourse(id, update) {
    return dispatch => {
        dispatch(fetchUpdateCourseRequest());
        return axios.post(`${dbURL}updateCourses`, {
            id,
            update
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchUpdateCourseSuccess(res.data.data));
            } else {
                dispatch(fetchUpdateCourseFailure());
            }
        });
    }
}


