import axios from "axios";
import * as types from './types';

const dbURL = "http://localhost:9000/server/";
const errorMsg = "An error has occurred while sending the request!";

function fetchUpdateTestRequest() {
    return {
        type: types.UPDATE_TEST_REQUEST
    }
}

function fetchUpdateTestSuccess(body) {
    return {
        type: types.UPDATE_TEST_SUCCESS,
        payload: body
    }
}

function fetchUpdateTestFailure() {
    return {
        type: types.UPDATE_TEST_FAILURE,
        payload: errorMsg
    }
}

export function updateTest(_id, update) {
    return dispatch => {
        dispatch(fetchUpdateTestRequest());
        return axios.post(`${dbURL}updateTest`, {
            _id,
            update
        }).then((res) => {
            const updatedTest = res.data.data.find(test => test._id === _id);
            if(res.data.success) {
                dispatch(fetchUpdateTestSuccess(updatedTest));
            } else {
                dispatch(fetchUpdateTestFailure());
            }
        });
    }
}

function fetchUpdateBlockRequest() {
    return {
        type: types.UPDATE_BLOCK_REQUEST
    }
}

function fetchUpdateBlockSuccess(body) {
    return {
        type: types.UPDATE_BLOCK_SUCCESS,
        payload: body
    }
}

function fetchUpdateBlockFailure() {
    return {
        type: types.UPDATE_BLOCK_FAILURE,
        payload: errorMsg
    }
}

export function updateBlock(_id, update) {
    return dispatch => {
        dispatch(fetchUpdateBlockRequest());
        return axios.post(`${dbURL}updateBlocks`, {
            _id,
            update
        }).then((res) => {
            if(res.data.success) {
                const updatedBlock = res.data.data.find(step => step._id === _id);
                dispatch(fetchUpdateBlockSuccess(updatedBlock));
            } else {
                dispatch(fetchUpdateBlockFailure());
            }
        });
    }
}

function fetchPutBlockRequest() {
    return {
        type: types.PUT_BLOCK_REQUEST
    }
}

function fetchPutBlockSuccess(body) {
    return {
        type: types.PUT_BLOCK_SUCCESS,
        payload: body
    }
}

function fetchPutBlockFailure() {
    return {
        type: types.PUT_BLOCK_FAILURE,
        payload: errorMsg
    }
}

export function putBlock(id, name, summary) {
    return dispatch => {
        dispatch(fetchPutBlockRequest());
        return axios.post(`${dbURL}putBlocks`, {
            id,
            name,
            summary,
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchPutBlockSuccess(res.data.allBlocks));
            } else {
                dispatch(fetchPutBlockFailure());
            }
        });
    }
}

export function setBlockToEdit(body) {
    return {
        type: types.SET_BLOCK_TO_EDIT,
        payload: body
    }
}

export function setStepsToEdit(body) {
    return {
        type: types.SET_STEPS_TO_EDIT,
        payload: body
    }
}

export function setStepToEdit(body) {
    return {
        type: types.SET_STEP_TO_EDIT,
        payload: body
    }
}

function fetchDeleteStepRequest() {
    return {
        type: types.DELETE_STEP_REQUEST
    }
}

function fetchDeleteStepSuccess(body) {
    return {
        type: types.DELETE_STEP_SUCCESS,
        payload: body
    }
}

function fetchDeleteStepFailure() {
    return {
        type: types.DELETE_STEP_FAILURE,
        payload: errorMsg
    }
}

export function deleteStep(objId, currentBlockId) {
    console.log(objId, currentBlockId);
    return dispatch => {
        dispatch(fetchDeleteStepRequest());
        return axios.delete(`${dbURL}deleteSteps`, {
            data: {
                objId,
                currentBlockId
            }
        }).then((res) => {
            console.log(res);
            if(res.data.success) {
                dispatch(fetchDeleteStepSuccess(objId));
            } else {
                dispatch(fetchDeleteStepFailure());
            }
        });
    }
}

function fetchPutStepRequest() {
    return {
        type: types.PUT_STEP_REQUEST
    }
}

function fetchPutStepSuccess(body) {
    return {
        type: types.PUT_STEP_SUCCESS,
        payload: body
    }
}

function fetchPutStepFailure() {
    return {
        type: types.PUT_STEP_FAILURE,
        payload: errorMsg
    }
}

export function putStep(id, name, summary, currentBlockId) {
    return dispatch => {
        dispatch(fetchPutStepRequest());
        return axios.post(`${dbURL}putSteps`, {
            id,
            name,
            summary,
            currentBlockId
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchPutStepSuccess(res.data.stepUpdate));
            } else {
                dispatch(fetchPutStepFailure());
            }
        });
    }
}

function fetchUpdateStepRequest() {
    return {
        type: types.UPDATE_STEP_REQUEST
    }
}

function fetchUpdateStepSuccess(body) {
    return {
        type: types.UPDATE_STEP_SUCCESS,
        payload: body
    }
}

function fetchUpdateStepFailure() {
    return {
        type: types.UPDATE_STEP_FAILURE,
        payload: errorMsg
    }
}

export function updateStep(_id, update) {
    return dispatch => {
        dispatch(fetchUpdateStepRequest());
        return axios.post(`${dbURL}updateSteps`, {
            _id,
            update
        }).then((res) => {
            if(res.data.success) {
                const updatedStep = res.data.data.find(step => step._id === _id);
                dispatch(fetchUpdateStepSuccess(updatedStep));
            } else {
                dispatch(fetchUpdateStepFailure());
            }
        });
    }
}

function fetchDeleteBlockRequest() {
    return {
        type: types.DELETE_BLOCK_REQUEST
    }
}

function fetchDeleteBlockSuccess(body) {
    return {
        type: types.DELETE_BLOCK_SUCCESS,
        payload: body
    }
}

function fetchDeleteBlockFailure() {
    return {
        type: types.DELETE_BLOCK_FAILURE,
        payload: errorMsg
    }
}

export function deleteBlock(objId) {
    return dispatch => {
        dispatch(fetchDeleteBlockRequest());
        return axios.delete(`${dbURL}deleteBlocks`, {
            data: {
                objId,
            }
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchDeleteBlockSuccess(res.data.data));
            } else {
                dispatch(fetchDeleteBlockFailure());
            }
        });
    }
}

export function orderBlocksNameAsc() {
    return {
        type: types.ORDER_BLOCKS_NAME_ASC,
    }
}

export function orderBlocksNameDesc() {
    return {
        type: types.ORDER_BLOCKS_NAME_DESC,
    }
}

export function orderBlocksIdAsc() {
    return {
        type: types.ORDER_BLOCKS_ID_ASC,
    }
}

export function orderBlocksIdDesc() {
    return {
        type: types.ORDER_BLOCKS_ID_DESC,
    }
}

export function orderStepsNameAsc() {
    return {
        type: types.ORDER_STEPS_NAME_ASC,
    }
}

export function orderStepsNameDesc() {
    return {
        type: types.ORDER_STEPS_NAME_DESC,
    }
}

export function orderStepsIdAsc() {
    return {
        type: types.ORDER_STEPS_ID_ASC,
    }
}

export function orderStepsIdDesc() {
    return {
        type: types.ORDER_STEPS_ID_DESC,
    }
}

export function orderTestsNameAsc() {
    return {
        type: types.ORDER_TESTS_NAME_ASC,
    }
}

export function orderTestsNameDesc() {
    return {
        type: types.ORDER_TESTS_NAME_DESC,
    }
}

export function orderTestsIdAsc() {
    return {
        type: types.ORDER_TESTS_ID_ASC,
    }
}

export function orderTestsIdDesc() {
    return {
        type: types.ORDER_TESTS_ID_DESC,
    }
}

export function setCurrentUser(user) {
    return {
        type: types.SET_CURRENT_USER,
        payload: user,
    }
}

export function setCurrentTest(test) {
    return {
        type: types.SET_CURRENT_TEST,
        payload: test,
    }
}
