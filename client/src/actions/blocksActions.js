import 'cross-fetch/polyfill';
import axios from "axios";
import * as types from './types';

const dbURL = "http://localhost:9000/server/";
const errorMsg = "An error has occurred while sending the request!";

function fetchBlocksRequest() {
    return {
        type: types.FETCH_BLOCKS_REQUEST
    }
}

function fetchBlocksSuccess(body) {
    return {
        type: types.FETCH_BLOCKS_SUCCESS,
        payload: body
    }
}

function fetchBlocksFailure() {
    return {
        type: types.FETCH_BLOCKS_FAILURE,
        payload: errorMsg
    }
}

export function fetchBlocks() {
    return dispatch => {
        dispatch(fetchBlocksRequest());
        return axios.get(`${dbURL}getBlocks`)
            .then((res) => {
                if(res.data.success) {
                    dispatch(fetchBlocksSuccess(res.data.data));
                } else {
                    dispatch(fetchBlocksFailure())
                }
            });
    }
}


