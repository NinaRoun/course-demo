import 'cross-fetch/polyfill';
import * as types from './types';
import axios from "axios";

const dbURL = "http://localhost:9000/server/";
//const errorMsg = "Check your username / password and try again, please!";

function fetchRegisterRequest() {
    return {
        type: types.FETCH_REGISTER_REQUEST
    }
}

function fetchRegisterSuccess(body) {
    return {
        type: types.FETCH_REGISTER_SUCCESS,
        payload: body
    }
}

function fetchRegisterFailure(errors) {
    return {
        type: types.FETCH_REGISTER_FAILURE,
        payload: errors
    }
}

function checkFieldsBeforeDaspatch(credentials) {
    const { userName, userEmail, firstPassword, secondPassword } = credentials;
    let errors = [];

    // Check required fields
    if(!userName || !userEmail || !firstPassword || !secondPassword) {
        errors.push({ msg: "Please fill in all fields" })
    }

    // Check passwords match
    if(firstPassword !== secondPassword) {
        errors.push({ msg: "Passwords do not match" })
    }

    // Check passwords match
    if(firstPassword.length < 7) {
        errors.push({ msg: "Passwords should be at least 7 characters " })
    }

    return errors;
}

export function registerNewUser(credentials) {
    const errors = checkFieldsBeforeDaspatch(credentials);
    return dispatch => {
        if(errors.length) {
            console.log('errors: ', errors);
            return dispatch(fetchRegisterFailure(errors));
        }
        console.log('No errors occurred');
        dispatch(fetchRegisterRequest());
        return axios.post(`${dbURL}registerUser`, {
            method: 'POST',
            body: credentials,
            headers: {
                'content-type': 'application/json'
            },
        })
            .then((res) => {
                console.log('res.data ', res.data);
                if(res.data.success) {
                    dispatch(fetchRegisterSuccess(res.data.data));
                } else {
                    console.log(res.data);
                    errors.push({ msg: res.data.msg });
                    dispatch(fetchRegisterFailure(errors))
                }
            });
    }
}

