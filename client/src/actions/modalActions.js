import * as types from './types';

const minPageSize = 700;

export function openModal() {
    return {
        type: types.OPEN_COURSE_MODAL
    }
}

export function editName() {
    return {
        type: types.EDIT_NAME
    }
}

export function editSummary() {
    return {
        type: types.EDIT_SUMMARY
    }
}

export function setModalInitialState() {
    return {
        type: types.SET_MODAL_INITIAL_STATE
    }
}
