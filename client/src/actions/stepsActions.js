import 'cross-fetch/polyfill';
import fetch from 'cross-fetch';
import * as types from './types';
import axios from "axios";
import { setIdsOfCurrentBlocks } from "./blocksActions";

const dbURL = "http://localhost:9000/server/";
const errorMsg = "An error has occurred while sending the request!";

function fetchStepsRequest() {
    return {
        type: types.FETCH_STEPS_REQUEST
    }
}

function fetchStepsSuccess(body) {
    return {
        type: types.FETCH_STEPS_SUCCESS,
        payload: body
    }
}

function fetchStepsFailure() {
    return {
        type: types.FETCH_STEPS_FAILURE,
        payload: errorMsg
    }
}

export function fetchSteps() {
    return dispatch => {
        dispatch(fetchStepsRequest());
        return fetch(`${dbURL}getSteps`)
            .then(res => {
                return res.json()})
            .then(body => {
                return dispatch(fetchStepsSuccess(body.data))
            })
            .catch(ex => dispatch(fetchStepsFailure(ex)))
    }
}

export function setCorrespondingSteps(steps) {
    return {
        type: types.SET_CORRESPONDING_STEPS,
        payload: steps,
    }
}

export function setIdForSteps(id) {
    return {
        type: types.SET_ID_FOR_STEPS,
        payload: id,
    }
}

export function setIdsOfCurrentSteps(ids) {
    return {
        type: types.SET_CURRENT_STEPS_IDS,
        payload: ids,
    }
}

export function setChosenSteps(steps) {
    return {
        type: types.SET_CHOSEN_STEPS,
        payload: steps,
    }
}
