import axios from "axios";
import * as types from './types';
import fetch from "cross-fetch";

const dbURL = "http://localhost:9000/server/";
const errorMsg = "An error has occurred while sending the request!";

function setCurrentStepsRequest() {
    return {
        type: types.SET_CURRENT_STEPS_REQUEST
    }
}

function setCurrentStepsSuccess(body) {
    console.log(body);
    const data = {
        currentSteps: body.currentSteps,
        maxActiveStep: body.maxActiveStep,
        activeStep: body.activeStep
    };
    return {
        type: types.SET_CURRENT_STEPS_SUCCESS,
        payload: data
    }
}

function setCurrentStepsFailure() {
    return {
        type: types.SET_CURRENT_STEPS_FAILURE,
        payload: errorMsg
    }
}

export function setCurrentSteps(currentSteps, maxActiveStep, userId) {
    return dispatch => {
        dispatch(setCurrentStepsRequest());
        return axios.post(`${dbURL}setCurrentSteps`, {
            data: {
                currentSteps,
                maxActiveStep,
                userId
            }
        })
            .then((res) => {
                console.log('set res ', res.data);
                if(res.data.success) {
                    dispatch(setCurrentStepsSuccess(res.data));
                } else {
                    dispatch(setCurrentStepsFailure())
                }
            });
    }
}

export function setPreviousStepsToShow(stepsToShow, maxActiveStep) {
    const data = {
        stepsToShow,
        maxActiveStep
    };
    return {
        type: types.SET_PREVIOUS_STEPS_TO_SHOW,
        payload: data
    }
}

function setCurrentStepRequest() {
    return {
        type: types.SET_CURRENT_STEP_REQUEST
    }
}

function setCurrentStepSuccess(body) {
    return {
        type: types.SET_CURRENT_STEP_SUCCESS,
        payload: body
    }
}

function setCurrentStepFailure() {
    return {
        type: types.SET_CURRENT_STEP_FAILURE,
        payload: errorMsg
    }
}

export function setCurrentStep(step, userId) {
    console.log('setCurrentStep action start ', step, userId);
    return dispatch => {
        dispatch(setCurrentStepRequest());
        return axios.post(`${dbURL}setCurrentStep`, {
            data: {
                step,
                userId
            }
        })
            .then((res) => {
                console.log('setCurrentStep res ', res.data);
                if(res.data.success) {
                    dispatch(setCurrentStepSuccess(res.data.currentStep));
                } else {
                    dispatch(setCurrentStepFailure())
                }
            });
    }
}

function getCurrentStepRequest() {
    return {
        type: types.GET_CURRENT_STEP_REQUEST
    }
}

function getCurrentStepSuccess(body) {
    const data = {
        currentStep: body.currentStep,
        maxActiveStep: body.maxActiveStep,
        activeStep: body.activeStep, //+1
    };
    return {
        type: types.GET_CURRENT_STEP_SUCCESS,
        payload: data
    }
}

function getCurrentStepFailure() {
    return {
        type: types.GET_CURRENT_STEP_FAILURE,
        payload: errorMsg
    }
}

export function getCurrentStep(userId) {
    console.log('in getCurrentStep');
    return dispatch => {
        dispatch(getCurrentStepRequest());
        return axios.post(`${dbURL}getCurrentStep`, {
            data: {
                userId
            }
        })
            .then((res) => {
                console.log('get res ', res.data);
                if(res.data.success) {
                    dispatch(getCurrentStepSuccess(res.data));
                } else {
                    dispatch(getCurrentStepFailure())
                }
            });
    }
}

export function setFinishedStep(step) {
    return {
        type: types.SET_FINISHED_STEP,
        payload: step
    }
}

function pushLessonToFinishedRequest() {
    return {
        type: types.PUSH_LESSON_TO_FINISHED_REQUEST
    }
}

function pushLessonToFinishedSuccess(body) {
    const data = {
        finishedSteps: body.finishedSteps,
        activeStep: body.activeStep
    };
    return {
        type: types.PUSH_LESSON_TO_FINISHED_SUCCESS,
        payload: data
    }
}

function pushLessonToFinishedFailure() {
    return {
        type: types.PUSH_LESSON_TO_FINISHED_FAILURE,
        payload: errorMsg
    }
}

export function pushLessonToFinished(id, userId) {
    return dispatch => {
        dispatch(pushLessonToFinishedRequest());
        return axios.post(`${dbURL}pushLessonToFinished`, {
            data: {
                id,
                userId
            }
        })
            .then((res) => {
                console.log('set res ', res.data);
                if(res.data.success) {
                    dispatch(pushLessonToFinishedSuccess(res.data));
                } else {
                    dispatch(pushLessonToFinishedFailure())
                }
            });
    }
}

function pushBlockToFinishedRequest() {
    return {
        type: types.GET_FINISHED_BLOCKS_REQUEST,
    }
}

function pushBlockToFinishedSuccess(id) {
    return {
        type: types.GET_FINISHED_BLOCKS_SUCCESS,
        payload: id
    }
}

function pushBlockToFinishedFailure() {
    return {
        type: types.GET_FINISHED_BLOCKS_FAILURE,
        payload: errorMsg
    }
}

export function pushBlockToFinished(block_id, userId) {
    return dispatch => {
        dispatch(pushBlockToFinishedRequest());
        return axios.post(`${dbURL}pushBlockToFinished`, {
            data: {
                userId,
                block_id
            }
        })
            .then((res) => {
                console.log('pushBlockToFinished action res ', res);
                if(res.data.success) {
                    dispatch(pushBlockToFinishedSuccess(res.data.block_id));
                } else {
                    dispatch(pushBlockToFinishedFailure())
                }
            });
    }
}

function fetchGetChosenCourseRequest() {
    return {
        type: types.GET_CHOSEN_COURSE_REQUEST
    }
}

function fetchGetChosenCourseSuccess(body) {
    return {
        type: types.GET_CHOSEN_COURSE_SUCCESS,
        payload: body
    }
}

function setCurrentCourseEmpty() {
    return {
        type: types.SET_CHOSEN_COURSE_EMPTY,
    }
}

function fetchGetChosenCourseFailure() {
    return {
        type: types.GET_CHOSEN_COURSE_FAILURE,
        payload: errorMsg
    }
}

export function getCurrentCourse(userId) {
    return dispatch => {
        dispatch(fetchGetChosenCourseRequest());
        return axios.post(`${dbURL}getCurrentCourse`, {
            data: {
                userId
            }
        })
            .then((res) => {
                if(res.data.success) {
                    if(res.data.course) {
                        dispatch(fetchGetChosenCourseSuccess(res.data.course));
                    } else {
                        dispatch(setCurrentCourseEmpty());
                    }
                } else {
                    dispatch(fetchGetChosenCourseFailure())
                }
            });
    }
}

function fetchChosenCourseRequest() {
    return {
        type: types.SET_CHOSEN_COURSE_REQUEST
    }
}

function fetchChosenCourseSuccess(body) {
    return {
        type: types.SET_CHOSEN_COURSE_SUCCESS,
        payload: body
    }
}

function fetchChosenCourseFailure() {
    return {
        type: types.SET_CHOSEN_COURSE_FAILURE,
        payload: errorMsg
    }
}

export function fetchSetChosenCourse(_id, userId, blocks) {
    return dispatch => {
        dispatch(fetchChosenCourseRequest());
        return axios.post(`${dbURL}chooseCourse`, {
            data: {
                _id,
                userId
            }
        })
            .then((res) => {
                if(res.data.success) {
                    dispatch(fetchChosenCourseSuccess(res.data.course));
                    const currentBlockToSet = blocks.filter(block => res.data.course.blocks[0] == block._id);
                    dispatch(setCurrentBlock(currentBlockToSet[0], userId));
                } else {
                    dispatch(fetchChosenCourseFailure())
                }
            });
    }
}

function fetchSetCurrentBlockRequest() {
    return {
        type: types.SET_CURRENT_BLOCK_REQUEST
    }
}

function fetchSetCurrentBlockSuccess(body) {
    return {
        type: types.SET_CURRENT_BLOCK_SUCCESS,
        payload: body
    }
}

function fetchSetCurrentBlockFailure() {
    return {
        type: types.SET_CURRENT_BLOCK_FAILURE,
        payload: errorMsg
    }
}

export function setCurrentBlock(block, userId) {
    return dispatch => {
        dispatch(fetchSetCurrentBlockRequest());
        return axios.post(`${dbURL}setCurrentBlock`, {
            data: {
                block,
                userId
            }
        })
            .then((res) => {
                if(res.data.success) {
                    dispatch(fetchSetCurrentBlockSuccess(res.data.block));
                } else {
                    dispatch(fetchSetCurrentBlockFailure())
                }
            });
    }
}

function getCurrentBlockRequest() {
    return {
        type: types.SET_CURRENT_BLOCK_REQUEST
    }
}

function getCurrentBlockSuccess(body) {
    return {
        type: types.SET_CURRENT_BLOCK_SUCCESS,
        payload: body
    }
}

function getCurrentBlockFailure() {
    return {
        type: types.SET_CURRENT_BLOCK_FAILURE,
        payload: errorMsg
    }
}

export function getCurrentBlock(userId) {
    return dispatch => {
        dispatch(getCurrentBlockRequest());
        return axios.post(`${dbURL}getCurrentBlock`, {
            data: {
                userId
            }
        })
            .then((res) => {
                console.log('getCurrentBlock action res ', res);
                if(res.data.success) {
                    dispatch(getCurrentBlockSuccess(res.data.currentBlock));
                } else {
                    dispatch(getCurrentBlockFailure())
                }
            });
    }
}

function getFinishedBlocksRequest() {
    return {
        type: types.GET_FINISHED_BLOCKS_REQUEST
    }
}

function getFinishedBlocksSuccess(body) {
    return {
        type: types.GET_FINISHED_BLOCKS_SUCCESS,
        payload: body
    }
}

function getFinishedBlocksFailure() {
    return {
        type: types.GET_FINISHED_BLOCKS_FAILURE,
        payload: errorMsg
    }
}

export function getFinishedBlocks(userId) {
    return dispatch => {
        dispatch(getFinishedBlocksRequest());
        return axios.post(`${dbURL}getFinishedBlocks`, {
            data: {
                userId
            }
        })
            .then((res) => {
                console.log(' getFinishedBlocks action res ', res);
                if(res.data.success) {
                    dispatch(getFinishedBlocksSuccess(res.data.finishedBlocks));
                } else {
                    dispatch(getFinishedBlocksFailure())
                }
            });
    }
}

function getFinishedStepsRequest() {
    return {
        type: types.GET_FINISHED_STEPS_REQUEST
    }
}

function getFinishedStepsSuccess(body) {
    return {
        type: types.GET_FINISHED_STEPS_SUCCESS,
        payload: body
    }
}

function getFinishedStepsFailure() {
    return {
        type: types.GET_FINISHED_STEPS_FAILURE,
        payload: errorMsg
    }
}

export function getFinishedSteps(userId) {
    return dispatch => {
        dispatch(getFinishedStepsRequest());
        return axios.post(`${dbURL}getFinishedSteps`, {
            data: {
                userId
            }
        })
            .then((res) => {
                console.log(' getFinishedSteps action res ', res);
                if(res.data.success) {
                    dispatch(getFinishedStepsSuccess(res.data.finishedSteps));
                } else {
                    dispatch(getFinishedStepsFailure())
                }
            });
    }
}

function getCurrentBlocksRequest() {
    return {
        type: types.SET_CURRENT_BLOCKS_REQUEST
    }
}

function getCurrentBlocksSuccess(body) {
    return {
        type: types.SET_CURRENT_BLOCKS_SUCCESS,
        payload: body
    }
}

function getCurrentBlocksFailure() {
    return {
        type: types.SET_CURRENT_BLOCKS_FAILURE,
        payload: errorMsg
    }
}

export function getCurrentBlocks(userId) {
    return dispatch => {
        dispatch(getCurrentBlocksRequest());
        return axios.post(`${dbURL}getCurrentBlocks`, {
            data: {
                userId
            }
        })
            .then((res) => {
                if(res.data.success) {
                    dispatch(getCurrentBlocksSuccess(res.data.currentBlocks));
                } else {
                    dispatch(getCurrentBlocksFailure())
                }
            });
    }
}

function getCurrentStepsRequest() {
    return {
        type: types.GET_CURRENT_STEPS_REQUEST
    }
}

function getCurrentStepsSuccess(body) {
    const data = {
        currentSteps: body.currentSteps,
        maxActiveStep: body.maxActiveStep,
        activeStep: body.activeStep
    };
    return {
        type: types.GET_CURRENT_STEPS_SUCCESS,
        payload: data
    }
}

function getCurrentStepsFailure() {
    return {
        type: types.GET_CURRENT_STEPS_FAILURE,
        payload: errorMsg
    }
}

export function getCurrentSteps(userId) {
    return dispatch => {
        dispatch(getCurrentStepsRequest());
        return axios.post(`${dbURL}getCurrentSteps`, {
            data: {
                userId
            }
        })
            .then((res) => {
                console.log('get res ', res.data);
                if(res.data.success) {
                    dispatch(getCurrentStepsSuccess(res.data));
                } else {
                    dispatch(getCurrentStepsFailure())
                }
            });
    }
}

function fetchSetCurrentBlocksRequest() {
    return {
        type: types.SET_CURRENT_BLOCKS_REQUEST
    }
}

function fetchSetCurrentBlocksSuccess(body) {
    return {
        type: types.SET_CURRENT_BLOCKS_SUCCESS,
        payload: body
    }
}

function fetchSetCurrentBlocksFailure() {
    return {
        type: types.SET_CURRENT_BLOCKS_FAILURE,
        payload: errorMsg
    }
}

export function setCurrentBlocks(blocks, userId) {
    return dispatch => {
        dispatch(fetchSetCurrentBlocksRequest());
        return axios.post(`${dbURL}setCurrentBlocks`, {
            data: {
                blocks,
                userId
            }
        })
            .then((res) => {
                if (res.data.success) {
                    dispatch(fetchSetCurrentBlocksSuccess(res.data.blocks));
                } else {
                    dispatch(fetchSetCurrentBlocksFailure())
                }
            });
    }
}

export function resetActiveStep() {
    return {
        type: types.RESET_ACTIVE_STEP,
    }
}

export function logout() {
    return {
        type: types.LOG_OUT,
    }
}

function fetchUsersRequest() {
    return {
        type: types.FETCH_USERS_REQUEST
    }
}

function fetchUsersSuccess(body) {
    return {
        type: types.FETCH_USERS_SUCCESS,
        payload: body
    }
}

function fetchAscUsersSuccess(body) {
    return {
        type: types.FETCH_ASC_USERS_SUCCESS,
        payload: body
    }
}

function fetchUsersFailure() {
    return {
        type: types.FETCH_USERS_FAILURE,
        payload: errorMsg
    }
}

export function fetchUsers() {
    return dispatch => {
        dispatch(fetchUsersRequest());
        return fetch(`${dbURL}getDescUsers`)
            .then(res => {
                return res.json()})
            .then(body => {
                return dispatch(fetchUsersSuccess(body.data))
            })
            .catch(ex => dispatch(fetchUsersFailure(ex)))
    }
}

export function fetchAscUsers() {
    return dispatch => {
        dispatch(fetchUsersRequest());
        return fetch(`${dbURL}getAscUsers`)
            .then(res => {
                return res.json()})
            .then(body => {
                return dispatch(fetchAscUsersSuccess(body.data))
            })
            .catch(ex => dispatch(fetchUsersFailure(ex)))
    }
}

export function orderUsersAsc() {
    return {
        type: types.ORDER_USERS_ASC,
    }
}

export function orderUsersDesc() {
    return {
        type: types.ORDER_USERS_DESC,
    }
}
