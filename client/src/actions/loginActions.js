import 'cross-fetch/polyfill';
import * as types from './types';
import axios from "axios";

const dbURL = "http://localhost:9000/server/";
//const errorMsg = "Check your username / password and try again, please!";

function checkFieldsBeforeDispatch(credentials) {
    let errors = [];
    const { email, password } = credentials;

    // Check required fields
    if(!email || !password) {
        errors.push({ msg: "Please fill in all fields" })
    }

    return errors;
}

function fetchLoginRequest() {
    //console.log('in fetchLoginRequest');
    return {
        type: types.FETCH_LOGIN_REQUEST
    }
}

function fetchLoginSuccess(body) {
    //console.log('in fetchNewsSuccess ');
    return {
        type: types.FETCH_LOGIN_SUCCESS,
        payload: body
    }
}

function fetchLoginFailure(errors) {
    //console.log('error ', errors);
    return {
        type: types.FETCH_LOGIN_FAILURE,
        payload: errors
    }
}

export function loginUser(credentials) {
    return dispatch => {
        const errors = checkFieldsBeforeDispatch(credentials);
        if(errors.length) {
            //console.log('errors: ', errors);
            return dispatch(fetchLoginFailure(errors));
        }
        //console.log('No errors occurred');
        dispatch(fetchLoginRequest());
        return axios.post(`${dbURL}loginUser`, {
            method: 'POST',
            body: credentials,
            headers: {
                'content-type': 'application/json'
            },
        })
            .then((res) => {
                //console.log('res.data ', res.data);
                if(res.data.user) {
                    dispatch(fetchLoginSuccess(res.data.user));
                } else {
                    //console.log(res.data.message);
                    errors.push({ msg: res.data.message });
                    dispatch(fetchLoginFailure(errors));
                }
            });
    }
}

