import 'cross-fetch/polyfill';
import axios from "axios";
import * as types from './types';

const dbURL = "http://localhost:9000/server/";
const errorMsg = "An error has occurred while sending the request!";

function fetchQuestionsRequest() {
    return {
        type: types.FETCH_QUESTIONS_REQUEST
    }
}

function fetchQuestionsSuccess(body) {
    return {
        type: types.FETCH_QUESTIONS_SUCCESS,
        payload: body
    }
}

function fetchQuestionsFailure() {
    return {
        type: types.FETCH_QUESTIONS_FAILURE,
        payload: errorMsg
    }
}

export function fetchQuestions() {
    return dispatch => {
        dispatch(fetchQuestionsRequest());
        return axios.get(`${dbURL}getQuestions`)
            .then((res) => {
                if(res.data.success) {
                    dispatch(fetchQuestionsSuccess(res.data.data));
                } else {
                    dispatch(fetchQuestionsFailure())
                }
        });
    }
}

function fetchDeleteQuestionRequest() {
    return {
        type: types.DELETE_QUESTION_REQUEST
    }
}

function fetchDeleteQuestionSuccess(body) {
    return {
        type: types.DELETE_QUESTION_SUCCESS,
        payload: body
    }
}

function fetchDeleteQuestionFailure() {
    return {
        type: types.DELETE_QUESTION_FAILURE,
        payload: errorMsg
    }
}

export function deleteQuestion(id, currentTestId) {
    return dispatch => {
        dispatch(fetchDeleteQuestionRequest());
        return axios.delete(`${dbURL}deleteQuestion`, {
            data: {
                id,
                currentTestId,
            }
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchDeleteQuestionSuccess(res.data.removedElId));
            } else {
                dispatch(fetchDeleteQuestionFailure());
            }
        });
    }
}

function fetchPutQuestionsRequest() {
    return {
        type: types.PUT_QUESTION_REQUEST
    }
}

function fetchPutQuestionsSuccess(body) {
    return {
        type: types.PUT_QUESTION_SUCCESS,
        payload: body
    }
}

function fetchPutQuestionsFailure() {
    return {
        type: types.PUT_QUESTION_FAILURE,
        payload: errorMsg
    }
}

export function putQuestion(id, text, testId) {
    return dispatch => {
        dispatch(fetchPutQuestionsRequest());
        return axios.post(`${dbURL}putQuestion`, {
                id,
                text,
                testId,
        }).then((res) => {
            if(res.data.success) {
                dispatch(fetchPutQuestionsSuccess(res.data));
            } else {
                dispatch(fetchPutQuestionsFailure());
            }
        });
    }
}

function fetchUpdateQuestionRequest() {
    return {
        type: types.UPDATE_QUESTION_REQUEST
    }
}

function fetchUpdateQuestionSuccess(body) {
    return {
        type: types.UPDATE_QUESTION_SUCCESS,
        payload: body
    }
}

function fetchUpdateQuestionFailure() {
    return {
        type: types.UPDATE_QUESTION_FAILURE,
        payload: errorMsg
    }
}

export function updateQuestion(_id, update) {
    return dispatch => {
        dispatch(fetchUpdateQuestionRequest());
        return axios.post(`${dbURL}updateQuestion`, {
            _id,
            update
        }).then((res) => {
            const updatedQuestion = res.data.data.find(question => question._id === _id);
            if(res.data.success) {
                dispatch(fetchUpdateQuestionSuccess(updatedQuestion));
            } else {
                dispatch(fetchUpdateQuestionFailure());
            }
        });
    }
}

export function setCurrentQuestions(questions) {
    return {
        type: types.SET_CURRENT_QUESTIONS,
        payload: questions,
    }
}

export function setCurrentQuestion(question) {
    return {
        type: types.SET_CURRENT_QUESTION,
        payload: question,
    }
}


