const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CoursesSchema = new Schema(
    {
        name: {type: String, required: true},
        id: {type: Number},
        summary: {type: String},
        urlName: {type: String},
        isFinished: {type: Boolean},
        isActivated: {type: Boolean},
        grade: {type: String},
        blocks: [{ type: Schema.Types.ObjectId, ref: 'Blocks' }]
    },
    { timestamps: true }
);

module.exports = mongoose.model("Courses", CoursesSchema);
