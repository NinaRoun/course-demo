const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BlocksSchema = new Schema(
    {
        name: {type: String, required: true},
        id: {type: Number},
        summary: {type: String},
        urlName: {type: String},
        isFinished: {type: Boolean},
        isActivated: {type: Boolean},
        grade: {type: String},
        steps: [{ type: Schema.Types.ObjectId, ref: 'Steps' }],
        tests: [{ type: Schema.Types.ObjectId, ref: 'Tests' }],
    },
    { timestamps: true }
);

module.exports = mongoose.model("Blocks", BlocksSchema);
