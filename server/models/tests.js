const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TestsSchema = new Schema(
    {
        name: {type: String, required: true},
        id: {type: Number},
        summary: {type: String},
        urlName: {type: String},
        questions: [{ type: Schema.Types.ObjectId, ref: 'Questions' }],
    },
    { timestamps: true }
);

module.exports = mongoose.model("Tests", TestsSchema);
