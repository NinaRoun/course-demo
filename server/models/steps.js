const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const StepsSchema = new Schema(
    {
        name: {type: String, required: true},
        id: {type: Number},
        summary: {type: String},
        urlName: {type: String},
        isFinished: {type: Boolean},
        isActivated: {type: Boolean},
        grade: {type: String},
    },
    { timestamps: true }
);

module.exports = mongoose.model("Steps", StepsSchema);
