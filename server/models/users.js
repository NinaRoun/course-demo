const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema(
    {
        name: {type: String, required: true},
        email: {type: String, required: true},
        password: {type: String, required: true},
        currentCourse: {type: Object},
        currentBlock: {type: Object},
        currentStep: {type: Object},
        activeStep: {type: Number},
        currentBlocks: {type: Array},
        currentSteps: {type: Array},
        finishedSteps: {type: Array},
        finishedBlocks: {type: Array},
        finishedCourse: {type: Array},
        maxActiveStep: {type: Number},
        tests: {type: Array}, // {testId: _id, isFirstAttempt: bool, answers: [{id: 1, answer: fff, isCorrect: bool}, {}, ...]}
    },
    { timestamps: true }
);

module.exports = mongoose.model("Users", UserSchema);
