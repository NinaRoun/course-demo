const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const QuestionsSchema = new Schema(
    {
        text: {type: String, required: true},
        id: {type: Number},
        answers: {type: Array},
        correctAnswerId: 0,
    },
    { timestamps: true }
);

module.exports = mongoose.model("Questions", QuestionsSchema);
