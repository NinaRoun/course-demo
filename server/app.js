const mongoose = require('mongoose');
const express = require('express');
const ObjectId = require('mongodb').ObjectID;
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
const session = require('express-session');
const LocalStrategy = require('passport-local').Strategy;
const createError = require('http-errors');
const path = require('path');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require("cors");
const router = express.Router();
//const Passport = require('./passport');
const Courses = require('./models/courses');
const Blocks = require('./models/blocks');
const Steps = require('./models/steps');
const Users = require('./models/users');
const Tests = require('./models/tests');
const Questions = require('./models/questions');
const config = require('./etc/config.json');
const app = express();

//const dbRoute = `mongodb+srv://admin:289774pas@demo-skvv7.mongodb.net/blog-mern?retryWrites=true`;
const dbRoute = `${config.apiPrefix}${config.user}:${config.password}@demo-skvv7.mongodb.net/${config.dbName}?retryWrites=true`;

mongoose.connect(
    dbRoute,
    { useNewUrlParser: true }
);

let db = mongoose.connection;

db.once("open", () => console.log("connected to the database"));
db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));
app.use(
    session({
        secret: 'secret',
        resave: true,
        saveUninitialized: true
    })
);
app.use(passport.initialize());
app.use(passport.session());

passport.use(
    new LocalStrategy({ userNameField: 'email' }, (email, password, done) => {
        console.log(' in new LocalStrategy ');
        //Match User
        Users.findOne({email}).then(user => {
            console.log('in Users.findOne line 54, user = ', user);
            //if(!user) return res.json({msg: "Email is already registered"});
            if(!user) return done(null, false, { message: "Incorrect username"});
            //Match Password
            bcrypt.compare(password, user.password, (err, isMatch) => {
                console.log('in LocalStrategy bcrypt');
                if(err) throw err;
                if(isMatch) return done(null, user);
                return done(null, false, { msg: "Incorrect Password"});
            })

        }).catch(err => console.log('error occurred in line 42', err))
    })
);

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    Users.findById(id, (err, user) => {
        done(err, user);
    });
});

router.get("/getCourses", (req, res) => {
    Courses.find((err, data) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data });
    });
});

router.post("/putCourses", (req, res) => {
    let data = new Courses();

    const { id, name, summary } = req.body;

    if ((!id && id !== 0) || !name) {
        return res.json({
            success: false,
            error: "INVALID INPUTS"
        });
    }
    data.name = name;
    data.id = id;
    data.summary = summary;
    data.save(err => {
        if (err) return res.json({ success: false, error: err });

        Courses.find((err, data) => {
            if (err) return res.json({ success: false, error: err, message: 'error getting courses' });
            return res.json({
                success: true,
                data,
            });
        });
    });
});

router.post("/updateCourses", (req, res) => {
    const { id, update } = req.body;
    //updateData(id, update)
    Courses.findByIdAndUpdate(id, update, err => {
        if (err) return res.json({ success: false, error: err });

        Courses.find((err, data) => {
            if (err) return res.json({ success: false, error: err, message: 'error getting courses' });
            return res.json({
                success: true,
                data,
            });
        });
    });
});

router.delete("/deleteCourses", (req, res) => {
    console.log('from server, ', req);
    const { id } = req.body;
    Courses.findByIdAndRemove(id, err => {
        if (err) return res.send(err);
        //return res.json({ success: true });
        Courses.find((err, data) => {
            if (err) return res.json({ success: false, error: err, message: 'error getting courses' });
            return res.json({
                success: true,
                data,
            });
        });
    });
});

router.get("/getBlocks", (req, res) => {
    Blocks.find((err, data) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data });
    });
});

router.post("/putBlocks", (req, res) => {
    let data = new Blocks();

    const { id, name, summary } = req.body;

    if ((!id && id !== 0) || !name) {
        return res.json({
            success: false,
            error: "INVALID INPUTS"
        });
    }
    data.name = name;
    data.id = id;
    data.summary = summary;
    data.save((err, savedEl) => {
        if (err) return res.json({ success: false, error: err });

        const addedBlock = data;

        Blocks.find((err, data) => {
            if (err) return res.json({ success: false, error: err, message: 'error getting blocks' });
            return res.json({
                success: true,
                blockUpdate: addedBlock,
                updatedId: savedEl._id,
                allBlocks: data,
            });
        });
    });
});

router.delete("/deleteBlocks", (req, res) => {
    const { objId } = req.body;
    Blocks.findByIdAndRemove(objId, err => {
        //console.log('first err, ', err);
        if (err) return res.send(err);
        Blocks.find((err, data) => {
            //console.log('second err, ', err);
            if (err) return res.json({ success: false, error: err, message: 'error getting blocks after removing' });
            return res.json({
                success: true,
                data,
            });
        });
    });
});

router.post("/updateBlocks", (req, res) => {
    const { _id, update } = req.body;
    Blocks.findByIdAndUpdate(_id, update, err => {
        if (err) return res.json({ success: false, error: err });

        Blocks.find((err, data) => {
            if (err) return res.json({ success: false, error: err, message: 'error getting blocks' });
            return res.json({
                success: true,
                data
            });
        });
    });
});

// STEPS

router.get("/getSteps", (req, res) => {
    Steps.find((err, data) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data });
    });
});

router.post("/putSteps", (req, res) => {
    let data = new Steps();

    const { id, name, summary, currentBlockId } = req.body;

    if ((!id && id !== 0) || !name) {
        return res.json({
            success: false,
            error: "INVALID INPUTS"
        });
    }
    data.name = name;
    data.id = id;
    data.summary = summary;
    data.save((err, savedEl) => {
        if (err) return res.json({ success: false, error: err });
        Blocks.findByIdAndUpdate(currentBlockId, { $push: { steps: savedEl._id } }, (err, savedBlock) => {
            if (err) return res.send(err);

            const addedStep = data;

            Steps.find((err, data) => {
                if (err) return res.json({ success: false, error: err, message: 'error while getting steps' });
                return res.json({
                    success: true,
                    stepUpdate: addedStep,
                    updatedId: savedEl._id,
                    allSteps: data,
                });
            });

        });
    });
});

router.delete("/deleteSteps", (req, res) => {
    const { objId, currentBlockId } = req.body;
    const ObjectID = new mongoose.Types.ObjectId(objId);
    Blocks.findByIdAndUpdate(currentBlockId, { $pull: { steps: ObjectID } }, { new: true }, (err) => {
        if (err) return res.send(err);

        Steps.findByIdAndRemove(objId, err => {
            if (err) return res.send(err);

            Steps.find((err, data) => {
                if (err) return res.json({ success: false, error: err, message: 'error getting steps after removing' });
                return res.json({
                    success: true,
                    data,
                });
            });
        });

    });
});

router.post("/updateSteps", (req, res) => {
    const { _id, update } = req.body;
    Steps.findByIdAndUpdate(_id, update, err => {
        if (err) return res.json({ success: false, error: err });

        Steps.find((err, data) => {
            if (err) return res.json({ success: false, error: err, message: 'error getting steps' });
            return res.json({
                success: true,
                data
            });
        });
    });
});

// TESTS

router.get("/getTests", (req, res) => {
    Tests.find((err, data) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data });
    });
});

router.post("/putTest", (req, res) => {
    let data = new Tests();

    const { id, name, summary, blockId } = req.body;

    if ((!id && id !== 0) || !name) {
        return res.json({
            success: false,
            error: "INVALID INPUTS"
        });
    }
    data.name = name;
    data.id = id;
    data.summary = summary;
    data.save((err, savedEl) => {
        if (err) return res.json({ success: false, error: err });
        Blocks.findByIdAndUpdate(blockId, { $push: { tests: savedEl._id } }, (err) => {
            if (err) return res.send(err);

            const addedTest = data;

            Tests.find((err, data) => {
                if (err) return res.json({ success: false, error: err, message: 'error while putting tests' });
                return res.json({
                    success: true,
                    testUpdate: addedTest,
                    updatedId: savedEl._id,
                    allTests: data,
                });
            });

        });
    });
});

router.delete("/deleteTest", (req, res) => {
    const { id, blockId } = req.body;
    const ObjectID = new mongoose.Types.ObjectId(id);
    Blocks.findByIdAndUpdate(blockId, { $pull: { tests: ObjectID } }, { new: true }, (err) => {
        if (err) return res.send(err);

        Tests.findByIdAndRemove(id, err => {
            if (err) return res.send(err);

            Tests.find((err, data) => {
                if (err) return res.json({ success: false, error: err, message: 'error getting tests after removing' });
                return res.json({
                    success: true,
                    data,
                });
            });
        });

    });
});

router.post("/updateTest", (req, res) => {
    const { _id, update } = req.body;
    Tests.findByIdAndUpdate(_id, update, err => {
        if (err) return res.json({ success: false, error: err });

        Tests.find((err, data) => {
            if (err) return res.json({ success: false, error: err, message: 'error getting tests' });
            return res.json({
                success: true,
                data
            });
        });
    });
});

// QUESTIONS

router.get("/getQuestions", (req, res) => {
    Questions.find((err, data) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data });
    });
});

router.post("/putQuestion", (req, res) => {
    let data = new Questions();

    const { id, text, testId } = req.body;

    if ((!id && id !== 0) || !text) {
        return res.json({
            success: false,
            error: "INVALID INPUTS"
        });
    }

    data.text = text;
    data.id = id;
    data.save((err, savedEl) => {
        if (err) return res.json({ success: false, error: err });
        Tests.findByIdAndUpdate(testId, { $push: { questions: savedEl._id } }, (err) => {
            if (err) return res.send(err);

            const addedQuestion = data;

            Questions.find((err, data) => {
                if (err) return res.json({ success: false, error: err, message: 'error while putting questions' });
                return res.json({
                    success: true,
                    questionUpdate: addedQuestion,
                    updatedId: savedEl._id,
                    allQuestions: data,
                });
            });

        });
    });
});

router.delete("/deleteQuestion", (req, res) => {
    const { id, currentTestId } = req.body;
    const ObjectID = new mongoose.Types.ObjectId(id);
    Tests.findByIdAndUpdate(currentTestId, { $pull: { questions: ObjectID } }, { new: true }, (err) => {
        if (err) return res.send(err);

        Questions.findByIdAndRemove(id, err => {
            if (err) return res.send(err);

            return res.json({
                success: true,
                removedElId: id,
            });
        });

    });
});

router.post("/updateQuestion", (req, res) => {
    const { _id, update } = req.body;
    Questions.findByIdAndUpdate(_id, update, err => {
        if (err) return res.json({ success: false, error: err });

        Questions.find((err, data) => {
            if (err) return res.json({ success: false, error: err, message: 'error getting questions' });
            return res.json({
                success: true,
                data
            });
        });
    });
});

// USERS

router.post("/registerUser", (req, res) => {
    let data = new Users();
    const { userName, userEmail, firstPassword } = req.body.body;

    Users.findOne({email: userEmail}).then(user => {
        if (user) return res.json({ msg: "Email is already registered" });

        data.name = userName;
        data.email = userEmail;
        data.password = firstPassword;

        //Hash password
        bcrypt.genSalt(10, (err, salt) => bcrypt.hash(firstPassword, salt, (err, hash) => {
            if (err) return res.json({ msg: "En error occurred, please, try later!" });
            data.password = hash;
            data.save(err => {
                if (err) return res.json({ msg: err.errors.message });
                Users.findOne({email: userEmail}).then((user, err) => {
                    if (err) return res.json({ msg: "En error occurred, please, try later!" });
                    return res.json({
                        success: true,
                        data: user,
                    });
                });
            })
        }))
    });
});

router.post('/loginUser', (req, res, next) => {
    const { email, password } = req.body.body;

    Users.findOne({email}).then(user => {
        if(!user) return res.json({ message: "Incorrect username" });
        bcrypt.compare(password, user.password, (err, isMatch) => {
            if(isMatch) return res.json({ user });
            return res.json({ message: "Incorrect password" });
        })

    })
});

router.get("/getDescUsers", (req, res) => {
    Users.find().sort({ createdAt: -1 }).then((data, err) => {
        if (err) return res.json({ success: false, error: err });
        else {
            return res.json({ success: true, data });
        }
    });
});

router.get("/getAscUsers", (req, res) => {
    Users.find().sort({ createdAt: 1 }).then((data, err) => {
        if (err) return res.json({ success: false, error: err });
        else {
            return res.json({ success: true, data });
        }
    });
});

router.post("/chooseCourse", (req, res) => {
    //console.log("! ", req.body);
    const { _id, userId } = req.body.data;
    console.log('came id = ', _id, ' username = ' + userId);
    const ObjectID = new mongoose.Types.ObjectId(userId);
    Courses.findById(_id).then((course, err) => {
        console.log("found ", course);
        if (err) return res.json({ msg: "En error occurred, please, try later!" });

        Users.findByIdAndUpdate(ObjectID, { $set: { currentCourse: course } }, {new: true}, (err, parentEl) => {
            console.log('parentEl ', parentEl);
            if (err) return res.send({ success: false, error: err });
            return res.json({
                success: true,
                course,
            });
        });

    });
});

router.post("/getCurrentCourse", (req, res) => {
    //console.log("! ", req.body);
    const { userId } = req.body.data;
    console.log('came username = ' + userId);
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findById(ObjectID, (err, foundEl) => {
        console.log('parentEl ', foundEl);
        if (err) return res.send({ success: false, error: err });
        return res.json({
            success: true,
            course: foundEl.currentCourse
        });
    });
});

router.post("/setCurrentBlocks", (req, res) => {
    const { blocks, userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findByIdAndUpdate(ObjectID, { $set: { currentBlocks: blocks } }, {new: true}, (err) => {
        if (err) return res.send({ success: false, error: err });
        return res.json({
            success: true,
            blocks,
        });
    });

});

router.post("/setCurrentBlock", (req, res) => {
    const { block, userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findByIdAndUpdate(ObjectID, { $set: { currentBlock: block } }, {new: true}, (err) => {
        if (err) return res.send({ success: false, error: err });
        return res.json({
            success: true,
            block,
        });
    });

});

router.post("/getCurrentBlock", (req, res) => {
    const { userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findById(ObjectID, (err, foundEl) => {
        if (err) return res.send({ success: false, error: err });
        const currentBlock = foundEl ? foundEl.currentBlock : {};
        return res.json({
            success: true,
            currentBlock
        });
    });

});

// router.post("/getUpdatedBlock", (req, res) => {
//     const { id } = req.body.data;
//     const ObjectID = new mongoose.Types.ObjectId(id);
//
//     Blocks.findById(ObjectID, (err, foundEl) => {
//         if (err) return res.send({ success: false, error: err });
//         return res.json({
//             success: true,
//             block: foundEl,
//         });
//     });
//
// });

router.post("/getCurrentBlocks", (req, res) => {
    const { userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findById(ObjectID, (err, foundEl) => {
        if (err) return res.send({ success: false, error: err });
        return res.json({
            success: true,
            currentBlocks: foundEl.currentBlocks
        });
    });

});

router.post("/getFinishedBlocks", (req, res) => {
    const { userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findById(ObjectID, (err, foundEl) => {
        console.log('foundEl ', foundEl);
        if (err) return res.send({ success: false, error: err });
        return res.json({
            success: true,
            finishedBlocks: foundEl.finishedBlocks
        });
    });

});

router.post("/getFinishedSteps", (req, res) => {
    const { userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findById(ObjectID, (err, foundEl) => {
        console.log('foundEl ', foundEl);
        if (err) return res.send({ success: false, error: err });
        return res.json({
            success: true,
            finishedSteps: foundEl.finishedSteps
        });
    });

});

router.post("/pushBlockToFinished", (req, res) => {
    const { block_id, userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findByIdAndUpdate(ObjectID, { $push: { finishedBlocks: block_id } }, {new: true}, (err) => {
        if (err) return res.send({ success: false, error: err });
        return res.json({
            success: true,
            block_id,
        });
    });

});

router.post("/getCurrentSteps", (req, res) => {
    const { userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findById(ObjectID, (err, foundEl) => {
        if (err) return res.send({ success: false, error: err });
        return res.json({
            success: true,
            currentSteps: foundEl.currentSteps,
            maxActiveStep: foundEl.maxActiveStep,
            activeStep: foundEl.activeStep,
        });
    });

});

router.post("/setCurrentSteps", (req, res) => {
    const { currentSteps, maxActiveStep, userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findByIdAndUpdate(ObjectID, { $set: { currentSteps: currentSteps, maxActiveStep: maxActiveStep } }, {new: true}, (err) => {
        if (err) return res.send({ success: false, error: err });
        Users.findById(ObjectID, (err, foundEl) => {
            if (err) return res.send({ success: false, error: err });
            return res.json({
                success: true,
                currentSteps: foundEl.currentSteps,
                maxActiveStep: foundEl.maxActiveStep,
                activeStep: foundEl.activeStep,
            });
        });
    });
});

router.post("/pushLessonToFinished", (req, res) => {
    const { id, userId } = req.body.data;
    const userObjectID = new mongoose.Types.ObjectId(userId);
    const stepObjectID = new mongoose.Types.ObjectId(id);

    Users.findByIdAndUpdate(userObjectID, { $push: { finishedSteps: stepObjectID } }, {new: true}, (err) => {
        if (err) return res.send({ success: false, error: err });
        Users.findById(userObjectID, (err, foundEl) => {
            console.log('pushLessonToFinished ', foundEl);
            if (err) return res.send({ success: false, error: err });
            return res.json({
                success: true,
                finishedSteps: foundEl.finishedSteps,
                currentStep: foundEl.currentStep,
            });
        });
    });
});

router.post("/setCurrentStep", (req, res) => {
    const { step, userId } = req.body.data;
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findByIdAndUpdate(ObjectID, { $set: { currentStep: step } }, {new: true}, (err) => {
        if (err) return res.send({ success: false, error: err });
        Users.findById(ObjectID, (err, foundEl) => {
            console.log('setCurrentStep ', foundEl);
            if (err) return res.send({ success: false, error: err });
            return res.json({
                success: true,
                currentStep: foundEl.currentStep,
            });
        });
    });
});

router.post("/getCurrentStep", (req, res) => {
    const { userId } = req.body.data;
    console.log('in getStep, ', userId);
    const ObjectID = new mongoose.Types.ObjectId(userId);

    Users.findById(ObjectID, (err, foundEl) => {
        if (err) return res.send({ success: false, error: err });
        return res.json({
            success: true,
            currentStep: foundEl.currentStep,
            maxActiveStep: foundEl.maxActiveStep,
            activeStep: foundEl.activeStep,
        });
    });

});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/server", router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
